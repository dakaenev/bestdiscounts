-- phpMyAdmin SQL Dump
-- version 2.11.7
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Време на генериране: 
-- Версия на сървъра: 5.0.51
-- Версия на PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `bestdiscountshere_com`
--

-- --------------------------------------------------------

--
-- Структура на таблица `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL auto_increment,
  `meta_title` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL,
  `name` varchar(255) NOT NULL,
  `published` datetime NOT NULL,
  `modified` datetime default NULL,
  `status` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дъмп (схема) на данните в таблицата `categories`
--

INSERT INTO `categories` (`id`, `meta_title`, `meta_description`, `meta_keywords`, `name`, `published`, `modified`, `status`) VALUES
(1, 'Manual Inserted', 'manual, inserted, test', 'manual, inserted, test', 'Manual Inserted', '2013-06-12 10:14:35', '2013-06-12 10:14:35', 1),
(2, 'First added', 'First Added Test', 'First ,Added ,Test', 'First Added Test', '2013-06-12 11:32:40', '2013-06-15 01:02:41', 1),
(3, 'Test', 'tesads', 'asdaf', 'New Category', '2013-06-14 13:41:23', '2013-06-15 01:07:39', 1);

-- --------------------------------------------------------

--
-- Структура на таблица `crons`
--

CREATE TABLE IF NOT EXISTS `crons` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `cron_time` varchar(255) NOT NULL,
  `cron_method` varchar(255) NOT NULL,
  `published` datetime NOT NULL,
  `last_run_time` datetime default NULL,
  `status` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дъмп (схема) на данните в таблицата `crons`
--

INSERT INTO `crons` (`id`, `name`, `cron_time`, `cron_method`, `published`, `last_run_time`, `status`) VALUES
(1, 'Send emails', '*/5 * * * *', 'send_emails', '2011-06-30 20:33:58', '2013-04-04 07:55:03', 1);

-- --------------------------------------------------------

--
-- Структура на таблица `emails`
--

CREATE TABLE IF NOT EXISTS `emails` (
  `id` int(11) NOT NULL auto_increment,
  `from_name` varchar(255) NOT NULL,
  `from_email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `mail_content` text NOT NULL,
  `published` datetime NOT NULL,
  `status` tinyint(4) NOT NULL default '1',
  `priority` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дъмп (схема) на данните в таблицата `emails`
--

INSERT INTO `emails` (`id`, `from_name`, `from_email`, `subject`, `mail_content`, `published`, `status`, `priority`) VALUES
(1, 'OMEGA TREND SUPPORT', 'support@bestdiscountshere.com', 'Welcome to Best Discounts Here', 'Dear sdad asd,<br />\n<br />\nThank you for choosing Omega Trend EA. <br />\n<br />\nYou can access the download section of our website from the link below:<br />\n<a href="http://www.omega-trend.com/users/login">http://www.omega-trend.com/users/login</a><br />\n<br />\nYour Order ID: <div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"><br />\n<br />\n<h4>A PHP Error was encountered</h4><br />\n<br />\n<p>Severity: Notice</p><br />\n<p>Message:  Undefined index:  order_id</p><br />\n<p>Filename: templates_c/b3b41d2d963a71f75dcd6477f8e212875e4a8706.file.add_user.htm.php</p><br />\n<p>Line Number: 35</p><br />\n<br />\n</div><div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"><br />\n<br />\n<h4>A PHP Error was encountered</h4><br />\n<br />\n<p>Severity: Notice</p><br />\n<p>Message:  Trying to get property of non-object</p><br />\n<p>Filename: templates_c/b3b41d2d963a71f75dcd6477f8e212875e4a8706.file.add_user.htm.php</p><br />\n<p>Line Number: 35</p><br />\n<br />\n</div><br />\n<br />\nPlease use your Email and Order ID to access it. <br />\nAfter this, you will be able to download Omega Trend EA, install it and activate it.<br />\nComplete information about the full process can be found in our User Guide, which is also accessible in the download section.<br />\n<br />\nIf you have any questions, just contact us!<br />\n<br />\n<br />\nSincerely,<br />\nBest Discounts Here<br />\nwww.bestdiscountshere.com<br />\n<br />\n<br />\n', '2013-06-12 11:35:13', 1, 0);

-- --------------------------------------------------------

--
-- Структура на таблица `emails_attachments`
--

CREATE TABLE IF NOT EXISTS `emails_attachments` (
  `id` int(11) NOT NULL auto_increment,
  `email_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `delete_after` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дъмп (схема) на данните в таблицата `emails_attachments`
--


-- --------------------------------------------------------

--
-- Структура на таблица `emails_predefined_data`
--

CREATE TABLE IF NOT EXISTS `emails_predefined_data` (
  `id` int(11) NOT NULL auto_increment,
  `value` text NOT NULL,
  `type` varchar(255) NOT NULL COMMENT 'subject, from, name, message',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дъмп (схема) на данните в таблицата `emails_predefined_data`
--


-- --------------------------------------------------------

--
-- Структура на таблица `emails_recipients`
--

CREATE TABLE IF NOT EXISTS `emails_recipients` (
  `id` int(11) NOT NULL auto_increment,
  `email_id` int(11) NOT NULL,
  `recipient_name` varchar(255) NOT NULL,
  `recipient_email` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1 = normal, 2 = bcc',
  `status` tinyint(4) NOT NULL default '0',
  `tried` tinyint(4) NOT NULL default '0',
  `sent_date` datetime default NULL,
  `unsubscribe` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дъмп (схема) на данните в таблицата `emails_recipients`
--

INSERT INTO `emails_recipients` (`id`, `email_id`, `recipient_name`, `recipient_email`, `type`, `status`, `tried`, `sent_date`, `unsubscribe`) VALUES
(1, 1, 'sdad asd', 'dsad@gmail.com', 1, 0, 0, NULL, '');

-- --------------------------------------------------------

--
-- Структура на таблица `emails_statistics_events`
--

CREATE TABLE IF NOT EXISTS `emails_statistics_events` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `constant` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дъмп (схема) на данните в таблицата `emails_statistics_events`
--


-- --------------------------------------------------------

--
-- Структура на таблица `emails_statistics_logs`
--

CREATE TABLE IF NOT EXISTS `emails_statistics_logs` (
  `id` int(11) NOT NULL auto_increment,
  `email_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `email` varchar(255) default NULL,
  `published` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дъмп (схема) на данните в таблицата `emails_statistics_logs`
--


-- --------------------------------------------------------

--
-- Структура на таблица `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` bigint(11) NOT NULL auto_increment,
  `file_name` varchar(255) NOT NULL,
  `file_type` varchar(255) NOT NULL default '',
  `file_path` text NOT NULL,
  `full_path` text NOT NULL,
  `raw_name` varchar(255) NOT NULL,
  `orig_name` varchar(255) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `file_ext` varchar(255) NOT NULL,
  `file_size` varchar(255) NOT NULL,
  `is_image` tinyint(4) NOT NULL default '0',
  `image_width` int(11) NOT NULL,
  `image_height` int(11) NOT NULL,
  `image_type` varchar(255) NOT NULL,
  `image_size_str` varchar(255) NOT NULL,
  `published` datetime NOT NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Дъмп (схема) на данните в таблицата `files`
--

INSERT INTO `files` (`id`, `file_name`, `file_type`, `file_path`, `full_path`, `raw_name`, `orig_name`, `client_name`, `file_ext`, `file_size`, `is_image`, `image_width`, `image_height`, `image_type`, `image_size_str`, `published`, `modified`) VALUES
(18, '1.png', 'image/png', '/files/2013/10/08/', '/files/2013/10/08/1.png', '1', '1.png', '1.png', '.png', '626.32', 1, 442, 600, 'png', 'width="442" height="600"', '2013-10-08 20:11:02', NULL);

-- --------------------------------------------------------

--
-- Структура на таблица `payment_transactions`
--

CREATE TABLE IF NOT EXISTS `payment_transactions` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL default '0',
  `email` varchar(255) default NULL,
  `transaction_data` text NOT NULL,
  `published` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дъмп (схема) на данните в таблицата `payment_transactions`
--


-- --------------------------------------------------------

--
-- Структура на таблица `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` tinyint(4) NOT NULL auto_increment,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `published` datetime NOT NULL,
  `modified` datetime default NULL,
  `status` tinyint(4) NOT NULL default '1',
  `url` varchar(255) NOT NULL,
  `file_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дъмп (схема) на данните в таблицата `products`
--

INSERT INTO `products` (`id`, `meta_title`, `meta_description`, `meta_keywords`, `title`, `short_description`, `description`, `published`, `modified`, `status`, `url`, `file_id`) VALUES
(2, 'First products meta title', 'dfsd', 'dfsdfs', 'First product is this', '<p>\r\n	Short</p>\r\n', '<p>\r\n	Lorem ipsum</p>\r\n<div id="lipsum">\r\n	<p>\r\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eu tellus et urna aliquet tristique sed nec libero. Fusce feugiat turpis id nisi rhoncus, in scelerisque urna consectetur. Phasellus et ultrices felis. Aliquam elementum accumsan nisl, sit amet condimentum nisi laoreet id. Phasellus venenatis elit ac mauris egestas porttitor. Pellentesque mauris dolor, elementum eu elit ut, laoreet interdum tellus. Donec at pellentesque sapien, in suscipit leo. Nulla aliquam elit massa, in convallis nunc ultrices id. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur nisl urna, imperdiet sed aliquam eget, pharetra et felis. Mauris lorem felis, ultricies id metus eget, fermentum aliquet tellus. Morbi commodo at quam eget facilisis. Aenean lacinia feugiat hendrerit. Quisque porta scelerisque dui sed semper.</p>\r\n	<p>\r\n		Nunc sem tellus, tempus in nisl adipiscing, cursus ultricies lectus. Praesent tincidunt felis facilisis tincidunt suscipit. Praesent faucibus venenatis urna nec imperdiet. Nam ullamcorper mauris vel luctus hendrerit. Mauris vulputate leo sed lacus elementum, sollicitudin viverra justo luctus. Sed hendrerit volutpat lacus. Proin ut ipsum vitae quam blandit laoreet in ac tellus. Cras non fringilla purus.</p>\r\n	<p>\r\n		Aenean vitae ultrices quam. Nunc eleifend, velit sed pellentesque dictum, sem sapien dictum odio, in tempor metus tortor a augue. Cras et nulla vitae purus auctor venenatis. Phasellus et sollicitudin nisi. Cras pellentesque felis eu mauris euismod condimentum. Sed vel elementum ipsum, ac facilisis felis. Morbi lacus est, sodales egestas libero ac, rhoncus semper velit. Nam pellentesque aliquet nulla, vel sagittis nisl ullamcorper ac. Sed dictum velit velit, vitae cursus dolor ultrices eget. Nullam vitae tincidunt eros, at molestie ligula. In laoreet orci et pellentesque lacinia. Pellentesque eget placerat leo, eu sagittis mauris. Donec id sodales risus.</p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n', '2013-06-15 01:09:31', '2013-07-04 21:12:55', 1, 'first-product-is-this', 16),
(3, 'asd', 'dfs', 'sdf', 'Second Product', '<p>\r\n	This is part</p>\r\n', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eu tellus et urna aliquet tristique sed nec libero. Fusce feugiat turpis id nisi rhoncus, in scelerisque urna consectetur. Phasellus et ultrices felis. Aliquam elementum accumsan nisl, sit amet condimentum nisi laoreet id. Phasellus venenatis elit ac mauris egestas porttitor. Pellentesque mauris dolor, elementum eu elit ut, laoreet interdum tellus. Donec at pellentesque sapien, in suscipit leo. Nulla aliquam elit massa, in convallis nunc ultrices id. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur nisl urna, imperdiet sed aliquam eget, pharetra et felis. Mauris lorem felis, ultricies id metus eget, fermentum aliquet tellus. Morbi commodo at quam eget facilisis. Aenean lacinia feugiat hendrerit. Quisque porta scelerisque dui sed semper.', '2013-07-03 16:29:51', '2013-07-04 17:30:57', 1, 'second-product', 0),
(4, 'Third Prod', 'asdaf', 'asfasf', 'Third Product', '<p>\r\n	Texth</p>\r\n', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eu tellus et urna aliquet tristique sed nec libero. Fusce feugiat turpis id nisi rhoncus, in scelerisque urna consectetur. Phasellus et ultrices felis. Aliquam elementum accumsan nisl, sit amet condimentum nisi laoreet id. Phasellus venenatis elit ac mauris egestas porttitor. Pellentesque mauris dolor, elementum eu elit ut, laoreet interdum tellus. Donec at pellentesque sapien, in suscipit leo. Nulla aliquam elit massa, in convallis nunc ultrices id. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur nisl urna, imperdiet sed aliquam eget, pharetra et felis. Mauris lorem felis, ultricies id metus eget, fermentum aliquet tellus. Morbi commodo at quam eget facilisis. Aenean lacinia feugiat hendrerit. Quisque porta scelerisque dui sed semper.', '2013-07-04 11:44:27', '2013-07-04 17:32:50', 1, 'third-product', 0),
(5, 'This is products 4', 'This is products 4', 'This, is, products, 4, fourth', 'Fourth Product', '<p>\r\n	4 short description</p>\r\n', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eu tellus et urna aliquet tristique sed nec libero. Fusce feugiat turpis id nisi rhoncus, in scelerisque urna consectetur. Phasellus et ultrices felis. Aliquam elementum accumsan nisl, sit amet condimentum nisi laoreet id. Phasellus venenatis elit ac mauris egestas porttitor. Pellentesque mauris dolor, elementum eu elit ut, laoreet interdum tellus. Donec at pellentesque sapien, in suscipit leo. Nulla aliquam elit massa, in convallis nunc ultrices id. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur nisl urna, imperdiet sed aliquam eget, pharetra et felis. Mauris lorem felis, ultricies id metus eget, fermentum aliquet tellus. Morbi commodo at quam eget facilisis. Aenean lacinia feugiat hendrerit. Quisque porta scelerisque dui sed semper.', '2013-07-04 12:08:51', '2013-07-04 12:09:46', 1, 'fourth-product', 0),
(6, 'asdd', 'asdad', 'asdad', 'Zero product', '<p>\r\n	fsdfs</p>\r\n', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eu tellus et urna aliquet tristique sed nec libero. Fusce feugiat turpis id nisi rhoncus, in scelerisque urna consectetur. Phasellus et ultrices felis. Aliquam elementum accumsan nisl, sit amet condimentum nisi laoreet id. Phasellus venenatis elit ac mauris egestas porttitor. Pellentesque mauris dolor, elementum eu elit ut, laoreet interdum tellus. Donec at pellentesque sapien, in suscipit leo. Nulla aliquam elit massa, in convallis nunc ultrices id. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur nisl urna, imperdiet sed aliquam eget, pharetra et felis. Mauris lorem felis, ultricies id metus eget, fermentum aliquet tellus. Morbi commodo at quam eget facilisis. Aenean lacinia feugiat hendrerit. Quisque porta scelerisque dui sed semper.</p>\r\n', '2013-07-04 12:20:31', '2013-10-08 20:11:01', 1, 'zero-product', 18);

-- --------------------------------------------------------

--
-- Структура на таблица `products_categories`
--

CREATE TABLE IF NOT EXISTS `products_categories` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дъмп (схема) на данните в таблицата `products_categories`
--

INSERT INTO `products_categories` (`product_id`, `category_id`) VALUES
(5, 2),
(5, 3),
(3, 2),
(3, 3),
(2, 2),
(2, 3),
(6, 3);

-- --------------------------------------------------------

--
-- Структура на таблица `products_options`
--

CREATE TABLE IF NOT EXISTS `products_options` (
  `id` int(11) NOT NULL auto_increment,
  `product_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `buy_url` varchar(255) default NULL,
  `visit_url` varchar(255) default NULL,
  `price` decimal(10,2) NOT NULL,
  `old_price` decimal(10,2) NOT NULL,
  `discount` int(11) NOT NULL,
  `coupon` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дъмп (схема) на данните в таблицата `products_options`
--

INSERT INTO `products_options` (`id`, `product_id`, `title`, `description`, `buy_url`, `visit_url`, `price`, `old_price`, `discount`, `coupon`) VALUES
(1, 2, 'Option 1', 'sdfsf', 'sdfsf', 'sdfdsf', 23.00, 45.00, 30, 'dfsdfsd'),
(2, 2, 'Option 2', 'asd', 'asd', 'asd', 24.00, 45.00, 45, 'sdad'),
(3, 6, 'New_Option', 'fas', 'asda', 'asfs', 243.00, 360.00, 43, '66ghg'),
(4, 6, 'New_Added', 'new_add_descre', 'rtg', 'fgf', 34.00, 55.00, 12, 'fsdf454fd');

-- --------------------------------------------------------

--
-- Структура на таблица `subscribers`
--

CREATE TABLE IF NOT EXISTS `subscribers` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL default '0' COMMENT '0 - pending, 1 - subscribed, 2 - unsubscribed',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дъмп (схема) на данните в таблицата `subscribers`
--


-- --------------------------------------------------------

--
-- Структура на таблица `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL auto_increment,
  `first_name` varchar(100) NOT NULL default '',
  `last_name` varchar(100) NOT NULL default '',
  `username` varchar(100) NOT NULL default '',
  `password` varchar(32) NOT NULL default '',
  `temporary_password` varchar(255) default NULL,
  `email` varchar(255) NOT NULL default '',
  `additional_email` varchar(255) default NULL,
  `company` varchar(255) NOT NULL,
  `country` varchar(255) default NULL,
  `city` varchar(255) default NULL,
  `address` text,
  `post_code` varchar(255) default NULL,
  `phone` varchar(255) default NULL,
  `referrer` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `account_id` varchar(255) NOT NULL,
  `published` datetime NOT NULL,
  `modified` datetime default NULL,
  `user_type` tinyint(4) NOT NULL default '1' COMMENT '1 = admin; 2 = normal user;',
  `status` tinyint(4) NOT NULL default '1',
  `is_affiliate` tinyint(4) NOT NULL default '0',
  `newsletter` tinyint(4) NOT NULL default '1',
  `comment` text,
  `last_login` datetime default NULL,
  `login_info_sent` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дъмп (схема) на данните в таблицата `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `password`, `temporary_password`, `email`, `additional_email`, `company`, `country`, `city`, `address`, `post_code`, `phone`, `referrer`, `ip_address`, `account_id`, `published`, `modified`, `user_type`, `status`, `is_affiliate`, `newsletter`, `comment`, `last_login`, `login_info_sent`) VALUES
(1, 'test', 'test', '', '098f6bcd4621d373cade4e832627b4f6', NULL, 'test@test.com', NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', '0000-00-00 00:00:00', NULL, 1, 1, 0, 1, NULL, '2013-10-08 21:15:57', NULL),
(2, 'sdad', 'asd', '', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dsad@gmail.com', 'dsad@gmail.com', 'asdad', 'asda', 'asda', '', '3422', '34234', 'asdas', '', '', '2013-06-12 11:35:13', NULL, 2, 0, 0, 1, '', NULL, NULL);
