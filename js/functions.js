function showFlashMessage( title, message, message_options )
{	
	var sufix = Math.round( new Date().getTime() / 1000 );
	var message_id = "flash_message_" + sufix;
	
	var html_code = '<div id="' + message_id + '" title="' + title + '">';
	html_code += '<p>';
	
	if ( typeof( message_options ) == 'object' && message_options.type != '' )
	{
		html_code += '<span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 50px 0;"></span>';
	}
	html_code += message;
	html_code += '</p>';
	html_code += '</div>';
	
	var width = "auto";
	var height = "auto";
	var modal = true;
	var show_title = true;
	var show_button = true;
	var auto_hide = 0;
	if ( typeof( message_options ) == 'object' )
	{
		if ( typeof( message_options.width ) != "undefined" )
			width = parseInt( message_options.width );
		if ( typeof( message_options.height ) != "undefined" )
			height = parseInt( message_options.height );
		if ( typeof( message_options.modal ) != "undefined" )
			modal = message_options.modal;
		if ( typeof( message_options.show_title ) != "undefined" )
			show_title = message_options.show_title;
		if ( typeof( message_options.show_button ) != "undefined" )
			show_button = message_options.show_button;
		if ( typeof( message_options.auto_hide ) != "undefined" )
			auto_hide = parseInt( message_options.auto_hide );
	}
	
	var buttons = {
				"  ok  ": function() {
					$( this ).dialog( 'close' );
				},
				"  cancel  ": function() {
					$( this ).dialog( 'close' );
				}
			};
	if ( !show_button )
		buttons = null;
	$( "body" ).append( html_code );
	$( "#" + message_id ).dialog( 
		{
			bgiframe: true,
			modal: modal,
			width: width,
			height: height,
			buttons: buttons
		}
	);
	
	if ( show_title == false )
		$( "div.ui-dialog-titlebar" ).remove();
	if ( auto_hide > 0 )
	{
		setTimeout( "hideFlashMessage( '" + message_id + "' )", auto_hide );
	}

}

function hideFlashMessage( message_id )
{
	$( "#" + message_id ).dialog( 'close' );
}

function validate_email(email)
{
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (filter.test(email)) 
	{
		return true;
	}
	return false;
}

function checkFieldValue(action, field, text)
{
	if (action == "focus")
	{
		if ($(field).val() == text)
		{
			$(field).val('');
		}
	}
	else
	{
		if ($(field).val() == "")
		{	
			$(field).val(text);
		}
	}
}

function showHidePassField(el)
{
	if ($(el).attr('type') == 'text')
	{
		if ($(el).val() == 'Password...')
		{
			$(el).hide();
			$('#login_pass_input').show().focus();
		}
	}
	else if ($(el).attr('type') == 'password')
	{
		if ($(el).val() == '')
		{
			$(el).hide();
			$('#login_text_input').show();
		}
	}
}

function count_down_redirect(seconds, redirect)
{
	if (seconds == 0)
	{
		$("div.count-down").html("0");
		window.location = redirect;
	}
	else
	{
		setTimeout("count_down_redirect(" + (seconds - 1) + ", '" + redirect + "')", 1000);
		$("div.count-down").html(seconds);
	}
}

function count_down_and_submit(seconds, form_id)
{
	if (seconds == 0)
	{
		$("div.count-down").html("0");
		$("form#" + form_id).submit();
	}
	else
	{
		setTimeout("count_down_and_submit(" + (seconds - 1) + ", '" + form_id + "')", 1000);
		$("div.count-down").html(seconds);
	}
}