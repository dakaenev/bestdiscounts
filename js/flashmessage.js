
function showFlashMessage( title, message, message_options )
{	
	
	var sufix = Math.round( new Date().getTime() / 1000 );
	var message_id = "flash_message_" + sufix;
	
	var width = "";
	var height = "";
	var modal = true;
	var show_title = true;
	var show_button = true;
	var auto_hide = 0;
	var type = "info";
	if ( typeof( message_options ) == 'object' )
	{
		if ( typeof( message_options.width ) != "undefined" )
			width = 'width:' + message_options.width + 'px;';
		if ( typeof( message_options.height ) != "undefined" )
			height = 'height:' + message_options.height + 'px;';
		if ( typeof( message_options.modal ) != "undefined" )
			modal = message_options.modal;
		if ( typeof( message_options.show_title ) != "undefined" )
			show_title = message_options.show_title;
		if ( typeof( message_options.show_button ) != "undefined" )
			show_button = message_options.show_button;
		if ( typeof( message_options.auto_hide ) != "undefined" )
			auto_hide = parseInt( message_options.auto_hide );
		if ( typeof( message_options.type ) != "undefined" )
			type = message_options.type;
	}
	
	var html_code = '';
	
	html_code += '<div id="' + message_id + '" class="modal hide fade" style="' + width + height + '">';
    
	if ( show_title )
	{
		html_code += '<div class="modal-header">';
			html_code += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html_code += '<h3>' + title + '</h3>';
		html_code += '</div>';
	}	
		html_code += '<div class="modal-body">';
			html_code += '<p>' + message + '</p>';
		html_code += '</div>';
	
	if ( show_button )	
	{
		html_code += '<div class="modal-footer">';
			html_code += '<button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Ok</button>';
			html_code += '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>';
		html_code += '</div>';
	}
	
    html_code += '</div>';
	
	$( "body" ).append( html_code );
	$( "#" + message_id ).modal({backdrop: modal});
	
	if ( auto_hide > 0 )
	{
		setTimeout( "hideFlashMessage( '" + message_id + "' )", auto_hide );
	}

}

function hideFlashMessage( message_id )
{
	$( "#" + message_id ).modal('hide');
}

