
function load_product_options(product_id)
{
    $("#load_result tbody").html('<tr><th colspan="3"><b class="center">Loading...</b></th></tr>');
    $.post("/admin/ajax/load_product_options", { product_id: product_id },
            function(data) {
                    $("#load_result tbody").html(data);
    });
}

function cancel_form_option()
{
    $('#add_edit_form').attr('style', 'display:none;'); 
    $('#add_edit_form_cancel').attr('style', 'display:none;'); 
    $('#add_edit_form_submit').attr('style', 'display:none;');
}

function prepare_edit_option(option_id)
{
    $.post("/admin/ajax/load_option", { option_id: option_id },
            function(data) {
                
                var object_data = $.parseJSON(data);
                //console.debug(object_data);
                $('input[name=id]').val(object_data[0].id);
                $('input[name=option_title]').val(object_data[0].title);
                $('input[name=descr]').val(object_data[0].description);
                $('input[name=url_b]').val(object_data[0].buy_url);
                $('input[name=url_v]').val(object_data[0].visit_url);
                $('input[name=price_now]').val(object_data[0].price);
                $('input[name=price_old]').val(object_data[0].old_price);
                $('input[name=discount]').val(object_data[0].discount);
                $('input[name=coupon]').val(object_data[0].coupon);
    });
    
    $('#response_div').addClass( "hide" );
    $('#add_edit_form').attr('style', 'display:inline;');
    $('#add_edit_form_cancel').attr('style', 'display:inline;');
    $('#add_edit_form_submit').attr('style', 'display:inline;');
    
    $('#action_title').text('Edit option');
}

function prepare_insert_option()
{
    $('input[name=id]'          ).val('');
    $('input[name=option_title]').val('');
    $('input[name=descr]'       ).val('');
    $('input[name=url_b]'       ).val('');
    $('input[name=url_v]'       ).val('');
    $('input[name=price_now]'   ).val('');
    $('input[name=price_old]'   ).val('');
    $('input[name=discount]'    ).val('');
    $('input[name=coupon]'      ).val('');

    $('#response_div').addClass( "hide" );
    $('#add_edit_form').attr('style', 'display:inline;');
    $('#add_edit_form_cancel').attr('style', 'display:inline;');
    $('#add_edit_form_submit').attr('style', 'display:inline;');
    
    $('#action_title').text('Insert option');
}

function insert_update_option()
{
    var data = $('#options_form').serialize();
    var product_id = $('input[name=product_id]').val();
    
    $.post("/admin/ajax/insert_update_option", { data: data },
            function(response) {
                    $('#response_div').removeClass( "hide" );
                    $('#response_div').text(response);
    });
    
    //cancel_form_option();
    load_product_options(product_id);
}

function delete_option(option_id)
{
	if (confirm("Are you sure that you want to remove this option?"))
	{
		$.post("/admin/ajax/delete_option", { option_id: option_id },
			function(data) {
				if (data == '1')
					$("tr#option_" + option_id).remove();
				else
					alert("There was an error! Please try again!");
		});
	}
}

function delete_user(user_id)
{
	if (confirm("Are you sure that you want to remove this user?"))
	{
		$.post("/admin/ajax/delete_user", { user_id: user_id },
			function(data) {
				if (data == '1')
					$("tr#row_user_" + user_id).remove();
				else
					alert("There was an error! Please try again!");
		});
	}
}

function delete_category(category_id)
{
	if (confirm("Are you sure that you want to remove this category?"))
	{
		$.post("/admin/ajax/delete_category", { category_id: category_id },
			function(data) {
				if (data == '1')
					$("tr#row_category_" + category_id).remove();
				else
					alert("There was an error! Please try again!");
		});
	}
}

function delete_product(product_id)
{
	if (confirm("Are you sure that you want to remove this product?"))
	{
		$.post("/admin/ajax/delete_product", { product_id: product_id },
			function(data) {
				if (data == '1')
					$("tr#row_product_" + product_id).remove();
				else
					alert("There was an error! Please try again!");
		});
	}
}

function changeUserStatus( user_id )
{
	$.post("/admin/ajax/change_user_status", {user_id: user_id},
		function(data){
			if (data != 'error')
			{
				if (data == "active")
				{
					$("span#user_status").html("Active").attr("class", "label label-success");
				}
				else
					$("span#user_status").html("Inactive").attr("class", "label label-important");
			}
			else
				alert("There was an error! Please try again!");
 
	   });
}

function changeCategoryStatus( category_id )
{
	$.post("/admin/ajax/change_category_status", {category_id: category_id},
		function(data){
			if (data != 'error')
			{
				if (data == "active")
				{
					$("span#category_status").html("Active").attr("class", "label label-success");
				}
				else
					$("span#category_status").html("Inactive").attr("class", "label label-important");
			}
			else
				alert("There was an error! Please try again!");
 
	   });
}

function changeProductStatus( product_id )
{
	$.post("/admin/ajax/change_product_status", {product_id: product_id},
		function(data){
			if (data != 'error')
			{
				if (data == "active")
				{
					$("span#product_status").html("Active").attr("class", "label label-success");
				}
				else
					$("span#product_status").html("Inactive").attr("class", "label label-important");
			}
			else
				alert("There was an error! Please try again!");
 
	   });
}

function updateUserAccountNumbers(product_id, user_id)
{	
	var max_live = $("input#max_live_" + product_id).val();
	var max_demo = $("input#max_demo_" + product_id).val();
	
	$.post("/admin/ajax/update_user_account_numbers", {user_id: user_id, product_id: product_id, max_live: max_live, max_demo: max_demo},
		function(data){
			if (data == 1)
				showFlashMessage("Information Box!", "Account's limit was updated successfully!", {modal: true, auto_hide: 3000});
			else
				showFlashMessage("Error!", "There was an error processing the data. Please try again later.", {modal: true, auto_hide: 3000}); 
	   });
}


function deleteUserProduct(user_id, product_id)
{	
	if (confirm("Are you sure that you want to remove this use?"))
	{
		$.post("/admin/ajax/delete_user_product", {user_id: user_id, product_id: product_id},
			function(data){
				if (data == 1)
				{
					$("tr#product_row_" + product_id).remove();
					showFlashMessage("Information Box!", "The product was removed successfully!", {modal: true, auto_hide: 3000});
				}
				else
					showFlashMessage("Error!", "There was an error removing the product. Please try again later.", {modal: true, auto_hide: 3000});
	 
		});
	}
}

function updateUserAcountNumber(account_id)
{	
	var account_number = $("input#account_number_" + account_id).val();
	
	//console.log(account_number);
	$.post("/admin/ajax/update_user_account_number", {account_id: account_id, account_number: account_number},
		function(data){
			if (data == 1)
				showFlashMessage("Information Box!", "The account number was updated successfully!", {modal: true, auto_hide: 3000});
			else
				showFlashMessage("Error!", "There was an error updating the account number. Please try again later.", {modal: true, auto_hide: 3000});
 
	   });
}

function deleteUserAccountNumber(account_id)
{	
	if (confirm("Are you sure that you want to remove this account number?"))
	{
		$.post("/admin/ajax/delete_user_account_number", {account_id: account_id},
			function(data){
				if (data == 1)
				{
					$("tr#account_number_row_" + account_id).remove();
					showFlashMessage("Information Box!", "The account number was removed successfully!", {modal: true, auto_hide: 3000});
				}
				else
					showFlashMessage("Error!", "There was an error removing the account number. Please try again later.", {modal: true, auto_hide: 3000});
 
	   });
	}
}

function change_current_robot_version(version_id)
{
	$.post("/admin/ajax/change_current_robot_version", { version_id: version_id });
}

function delete_robot_version(version_id)
{
	if (confirm("Are you sure that you want to remove this version?"))
	{
		$.post("/admin/ajax/delete_robot_version", { version_id: version_id },
			function(data) {
				if (data == '1')
					$("tr#row_version_" + version_id).remove();
				else
					alert("There was an error! Please try again!");
		});
	}
}

function delete_affiliate(affiliate_id)
{
	if (confirm("Are you sure that you want to remove this affiliate?"))
	{
		$.post("/admin/ajax/delete_affiliate", { affiliate_id: affiliate_id },
			function(data) {
				if (data == '1')
					$("tr#row_affiliate_" + affiliate_id).remove();
				else
					alert("There was an error! Please try again!");
		});
	}
}

function delete_faq(faq_id)
{
	if (confirm("Are you sure that you want to remove this FAQ?"))
	{
		$.post("/admin/ajax/delete_faq", { faq_id: faq_id },
			function(data) {
				if (data == '1')
					$("tr#row_faq_" + faq_id).remove();
				else
					alert("There was an error! Please try again!");
		});
	}
}

function activateNewAccountNumber(user_id, product_id)
{
	var account_number = $( "input#new_account_number" ).val();
	var account_type = $( "select#new_account_type" ).val();
	
	if (account_number == "")
	{
		alert("Please enter an account number before to continue!");
		return false;
	}
		
	$.post( "/admin/ajax/activate_new_account_number", {user_id: user_id, product_id: product_id, account_number: account_number, account_type: account_type},
		function( data ){
			if( data > 0 )
			{
				var account_id = data;
				
				var table_content = $( "table#accounts_table" ).html();
				
				if ( table_content.search( 'No activated accounts!' ) != -1 )
					$( "table#accounts_table tr" ).last().remove();
				
				var account_html = '';
				account_html += '<tr id="account_number_row_' + account_id + '">';
				account_html += '<td style="text-align: center;">' + ( $( "table#accounts_table tr" ).get().length ) + '</td>';
				account_html += '<td style="text-align: left;">Omega Trend EA</td>';
				account_html += '<td style="text-align: center;">';
				account_html += '<input type="text" value="' + account_number + '" style="width: 120px; height: 16px; margin: 0;" id="account_number_' + account_id + '"/>';
				account_html += '<button class="btn btn-small btn-info" type="button" onclick="updateUserAcountNumber(\'' + account_id + '\' );"><i class="icon-refresh icon-white"></i> Update</button>';
				account_html += '</td>';
					
				account_html += '<td style="text-align: center;">' + ( account_type == 1 ? 'DEMO' : 'LIVE' ) + '</td>';
				
				var currentTime = new Date();
				var month = currentTime.getMonth() + 1;
				var day = currentTime.getDate();
				var year = currentTime.getFullYear();
				var hour = currentTime.getHours();
				var minutes = currentTime.getMinutes();
				var seconds = currentTime.getSeconds();
				
				var curr_date = year + '-' + putZero(month) + '-' + putZero(day) + ' ' + putZero(hour) + ':' + putZero(minutes) + ':' + putZero(seconds);

				account_html += '<td style="text-align: center;">' + curr_date + '</td>';
				account_html += '<td style="text-align: center;"><button type="button" class="btn btn-danger btn-small" onclick="deleteUserAccountNumber(\'' + account_id + '\', \'' + product_id + '\');"><i class="icon-remove icon-white"></i> Remove</button></td>';
				account_html += '</tr>';
																
				$( "table#accounts_table" ).append( account_html );
				showFlashMessage("Information Box!", "The account number was activated successfully!", {modal: true, auto_hide: 3000});
			}
			else
			{
				showFlashMessage("Error!", "There was an error activating the new account number. Please try again later.", {modal: true, auto_hide: 3000});
			}
 
	});
}

function putZero(value)
{
	if (value < 10)
		return '0' + value;
	else
		return value;
}

function selectAll( sel )
{
	var action = "";
	for ( i = 0; i < sel.options.length; i ++ )
	{
		if ( sel.options[i].value == "all" )
		{
			if ( sel.options[i].selected )
				action = "select";
			else
				action = "deselect";
			break;
		}
	}
	if ( action == "select" )
	{
		for ( i = 0; i < sel.options.length; i ++ )
		{
			sel.options[i].selected = true;
		}
	}
}

function addEditUserComment(user_id)
{
	$( "#user_comment_popup" ).remove();
	
	var comment = $( "#user_comment_" + user_id ).attr("data-original-title");
	if( comment == "Click to add comment!" )
		comment = "";
	
	var comment_html = '<textarea id="user_comment" style="width: 510px; height: 100px; padding: 5px 10px;">' + comment + '</textarea>';
	
	
	var html_code = '';
	
	html_code += '<div id="user_comment_popup" class="modal hide fade">';
    
	
		html_code += '<div class="modal-header">';
			html_code += '<button type="button" class="close" aria-hidden="true">&times;</button>';
			if (comment == "")
				html_code += '<h3>Add comment</h3>';
			else
				html_code += '<h3>Edit comment</h3>';
		html_code += '</div>';
		
		html_code += '<div class="modal-body">';
			html_code += '<p>' + comment_html + '</p>';
		html_code += '</div>';
	
	
		html_code += '<div class="modal-footer">';
		
		if (comment == "")
			html_code += '<button class="btn btn-info" onclick="saveUserComment(\'' + user_id + '\');">Add</button>';
		else
			html_code += '<button class="btn btn-info" onclick="saveUserComment(\'' + user_id + '\');">Save</button>';
			
			html_code += '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>';
		html_code += '</div>';
	
	
    html_code += '</div>';
	
	$( "body" ).append( html_code );
	$( "#user_comment_popup" ).modal({backdrop: false});
	
}

function saveUserComment(user_id)
{
	var comment = $( "#user_comment" ).val();
	$.post("/admin/ajax/save_user_comment", {user_id: user_id, comment: comment},
		function(data){
			if (data == "0")
			{
				alert("������� ������� ��� �������� �� ���������! ���� �������� ���!");
			}
			else
			{
				$("#user_comment_popup").modal('hide');
				
				if( comment.length > 0 )
				{
					$("#user_comment_" + user_id).addClass("icon-red");
					$("#user_comment_" + user_id).attr("data-original-title", comment);
				}
				else
				{
					$("#user_comment_" + user_id).removeClass("icon-red");
					$("#user_comment_" + user_id).attr("data-original-title", "Click to add comment!");
				}				
			}
 
	   });
}

// New Functionality for BGO_Media
function DisplayArticle(data, recepient_id)
{
    if (data.status == 'error')
    {
            alert("There was an error! Please try again!");
    }

    $("div#"+recepient_id).html('<h5>'+data.html_code+'</h5>');
    $("div#"+recepient_id).attr("class", "alert alert-"+data.status);
}

function GetArticle( article_id )
{
	$.post("/admin/ajax/get_article_not_logged", {article_id: article_id},
		function(data){
                    
                    DisplayArticle(data,'article_result');
			
        }, "json");
}

function GetArticleLogged( article_id )
{
	$.post("/admin/ajax/get_article_logged", {article_id: article_id},
		function(data){
                    
                    DisplayArticle(data, 'logged_article_result');
			
        }, "json");
}

function GetArticlesByCategory( category_id )
{
	$.post("/admin/ajax/get_articles_by_category", {category_id: category_id},
		function(data){
                    
                    DisplayArticle(data, 'logged_category_result');
			
        }, "json");
}

function Login()
{
    //var button_get_article = '<div class="span3" style="background-color: lightblue; padding:5px;"><div id="logged_article_result">GetArticle will load here when you press load button</div><button onclick="GetArticleLogged(1);">Load Article with id = 1</button></div>';
    //var button_get_category = '<div class="span3" style="background-color: lightblue; padding:5px;"><div id="logged_category_result">GetCategory will load here...</div><button onclick="GetArticlesByCategory(1);">Load Category with id = 1</button></div>';
    //var button_logout = '';
    
    $.post("/admin/ajax/login", {email: $("input[name=ajax_email]").val(), password: $("input[name=ajax_password]").val()},
		function(data){
                    //alert(data.status);
                    $("div#login_result").html('<h5>'+data.message+'</h5>');
                    $("div#login_result").attr("class", "alert alert-"+data.status);
                    
                    if (data.status == 'error')
                    {
                            //alert("There was an error! Please try again!");
                    }
                    else
                    {
                        //$("div#ajax_login_form").html(button_get_article + button_get_category + button_logout);
                        $("div#ajax_login_section").attr("class", "span8");
                        $("div#ajax_login_functions").show();
                        
                        $("form#ajax_login_form").hide();
                        $("div#login").hide();
                    }
    }, "json");
}

function Logout()
{
    //var button_get_article = '<div class="span3" style="background-color: lightblue; padding:5px;"><div id="logged_article_result">GetArticle will load here when you press load button</div><button onclick="GetArticleLogged(1);">Load Article with id = 1</button></div>';
    //var button_get_category = '<div class="span3" style="background-color: lightblue; padding:5px;"><div id="logged_category_result">GetCategory will load here...</div><button onclick="GetArticlesByCategory(1);">Load Category with id = 1</button></div>';
    //var button_logout = '';
    
    $.post("/admin/ajax/logout", {email: $("input[name=ajax_email]").val(), password: $("input[name=ajax_password]").val()},
		function(data){
                    //alert(data.status);
                    $("div#login_result").html('<h5>'+data.message+'</h5>');
                    $("div#login_result").attr("class", "alert alert-"+data.status);
                    
                    if (data.status == 'error')
                    {
                            alert("There was an error! Please try again!");
                    }
                    else
                    {
                        //$("div#ajax_login_section").attr("class", "span4");
                        //$("div#ajax_login_functions").hide();
                        
                        $("form#ajax_login_form").show();
                        //$("div#login").show();
                    }
    }, "json");
}

