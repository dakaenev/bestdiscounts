<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	private static $instance;

	public $layout = "main";
	public $page_title = "";
	public $page_description = "";
	public $page_keywords = "";
	
	public $administration = false;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		self::$instance =& $this;
		
		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');

		$this->load->initialize();
		
		log_message('debug', "Controller Class Initialized");
		
		$this->load->model("users_model");
		$this->users_model->check_for_cookie_data_and_log_user();	
		
		load_launch_data();

		$controller = $this->uri->segment(1, "");
		if ($controller != "photo")
		{
			$this->emailsstatistics_model->loadEmailStatisticsConstants();
			$this->emailsstatistics_model->checkForEmailStatistics();
		}
	}

	public static function &get_instance()
	{
		return self::$instance;
	}
	
	public function _output($output)
	{
	    $this->load->library("smarty_parser");
	    
	    $this->smarty_parser->assign("page_title", $this->page_title);
	    $this->smarty_parser->assign("page_description", $this->page_description);
	    $this->smarty_parser->assign("page_keywords", $this->page_keywords);
		
		/*
		$this->smarty_parser->assign( "page_description", isset($_SESSION["page_description"]) ? $_SESSION["page_description"] : "" );
		$this->smarty_parser->assign( "page_keywords", isset($_SESSION["page_keywords"]) ? $_SESSION["page_keywords"] : "" );
		$this->smarty_parser->assign( "page_title", isset($_SESSION["page_title"]) ? $_SESSION["page_title"] : "" );
		*/
		
	    if ($this->administration == false)
	    {
	    	//do something
	    }
	    
		$flash_message = get_flash_message();
	//	var_dump($flash_message);exit;
	    $this->smarty_parser->assign("flash_message", $flash_message);
	    $this->smarty_parser->assign("content", $output);
		
	    if ($this->administration == true)
	    {
			$this->smarty_parser->assign("controller", $this->uri->segment(2, ""));
			
			switch($this->layout)
			{
				case "main"; $layout = "pagelayout"; break;
				case "simple"; $layout = "pagelayout_simple"; break;
				case "empty"; $layout = "pagelayout_empty"; break;
				default: $layout = "pagelayout"; break;
			}
			
	    	$output = $this->smarty_parser->fetch("admin/" . $layout . ".htm");
	    }
	    else 
	    {
			switch($this->layout)
			{
				case "main"; $layout = "pagelayout"; break;
				case "empty"; $layout = "pagelayout_empty"; break;
				default: $layout = "pagelayout"; break;
			}
			
	    	$output = $this->smarty_parser->fetch($layout . ".htm");
	    }
		echo $output;	    
	}
}
// END Controller class

/* End of file Controller.php */
/* Location: ./system/core/Controller.php */