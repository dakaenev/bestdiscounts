<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once("smarty/Smarty.class.php");

class Smarty_parser extends Smarty {

	public function __construct()
	{
		parent::__construct();
		
		$this->template_dir = APPPATH . "templates";
		$this->compile_dir = APPPATH . "templates_c";		
	}
	
}