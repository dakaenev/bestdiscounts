<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Upload extends CI_Upload {

	public function __construct($props = array())
	{
		parent::__construct($props);
		
		$ci =& get_instance();
		$this->db = $ci->db;  
	}
	
	public function create_upload_path()
	{
		$day = date( "d" );
		$month = date( "m" );
		$year = date( "Y" );
		$full_file_path = FCPATH . "files/" . $year . "/" . $month . "/" . $day . "/";
		$month_file_path = FCPATH . "files/" . $year . "/" . $month . "/";
		$year_file_path = FCPATH . "files/" . $year . "/";
                        
		if ( is_dir( $full_file_path ) )
		{
			return $full_file_path;
		}
		else
		{
			if ( is_dir( $month_file_path ) )
			{
				if ( mkdir( $full_file_path ) )
					return $full_file_path;
				else 
					return false;
			}
			elseif ( is_dir( $year_file_path ) )
			{
				mkdir( $month_file_path );
				mkdir( $full_file_path );
				return $full_file_path;				
			}
			else 
			{
				mkdir( $year_file_path );
				mkdir( $month_file_path );
				mkdir( $full_file_path );
				return $full_file_path;
			}
		}
	}

	public function insert_into_db()
	{
		$data = $this->data();
		$data["published"] = date("Y-m-d H:i:s");
		$data["file_path"] = str_replace(str_replace( "\\", "/", FCPATH ) , "/", $data["file_path"]);
		$data["full_path"] = str_replace(str_replace( "\\", "/", FCPATH ) , "/", $data["full_path"]);
		
		$this->db->insert('files', $data); 
		
		$file_id = $this->db->insert_id();
		$data["id"] = $file_id;
		return $data;
	}
	
	public function remove($file_id)
	{
		$this->db->select('full_path');
		$this->db->where('id', $file_id);
		$this->db->limit(1);
		$query = $this->db->get('files');
		$file = $query->row();
		
		if($file && $file->full_path)
		{
			$full_file_path = FCPATH . substr($file->full_path, 1);
			if( file_exists($full_file_path) && unlink($full_file_path) )
			{
				$this->db->where('id', $file_id);
				$this->db->limit(1);
				$this->db->delete('files');
				
				return true;
			}
		}
			
		return false;		
	}
}