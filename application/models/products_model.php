<?php
class Products_model extends CI_Model
{
	public function add($data)
	{
            if(isset($data['categories']))
            {
                $categories = $data['categories'];
                unset($data['categories']);
            }
            
            $insert = $this->db->insert("products", $data);
            if($insert)
            {
                $insert_id = $this->db->insert_id();
                
                if(is_array($categories) && count($categories) > 0)
                {
                    $this->update_categories($insert_id, $categories);
                }
                
                return $insert_id;
            }

            return false;		
	}
        
        public function update($product_id, $data)
	{
            if(isset($data['categories']))
            { 
                $this->update_categories($product_id, $data['categories']);
                unset($data['categories']);
            }
            
            $this->db->where("id", $product_id);
            $this->db->limit(1);

            return $this->db->update("products", $data);		
	}
        
        public function load_product_options($product_id)
	{
		$this->db->select("SQL_CALC_FOUND_ROWS *", false);
                $this->db->where('product_id = '.$product_id);
                $query = $this->db->get("products_options");
		$data = $query->result_array();
				
		return $data;
	}
        
        public function load_option($option_id)
	{
		$this->db->select("SQL_CALC_FOUND_ROWS *", false);
                $this->db->where('id = '.$option_id);
                $query = $this->db->get("products_options");
		$data = $query->result_array();
				
		return $data;
	}
        
        public function insert_update_option($data)
        {
            if(is_array($data))
            {
                if(isset($data['id']))
                {   
                    $option_id = $data['id'];
                    unset($data['id']);
                }
                else
                { 
                    return 'Option ID is missing!';
                }
                 
                if($option_id > 0)
                {
                    $this->db->where("id", $option_id); // Option ID
                    $this->db->limit(1);

                    $result = $this->db->update("products_options", $data);
                    
                    if($result)
                    {
                        return 'Successfully Updated!';
                    }
                    else
                    {
                        return 'Problem when try to update!';
                    }
                }
                else
                {
                    if(isset($data['title']) && $data['title'] != '')
                    {
                        $result = $this->db->insert("products_options", $data);
                        
                        if($result)
                        {
                            return 'Successfully Inserted!';
                        }
                        else
                        {
                            return 'Problem when try to insert!';
                        }
                    }
                    else
                    {
                        return 'Title is required field!';
                    }
                }
            }
            else
            {
                return 'Data Should be array!';
            }
        }
        
        public function delete_option($option_id)
	{
		$this->db->limit(1);
		$result = $this->db->delete("products_options", array("id" => $option_id)); 
		
		return $result;
	}
        
        public function delete($product_id)
	{
		$this->db->limit(1);
		$result = $this->db->delete("products", array("id" => $product_id)); 
		
		return $result;
	}
        
        public function get_all_products($where = "", $offset = 0, $limit = 0, $order_by = "")
	{
		$this->db->select("SQL_CALC_FOUND_ROWS products.*, files.full_path", false);
                $this->db->from('products');
                $this->db->join('files', 'products.file_id = files.id');
		if($where != "")
                {
                    $this->db->where($where);
                }
			
		if ($limit)
			$this->db->limit($limit, $offset);
		
		if ($order_by != "")
			$this->db->order_by($order_by); 
		
		//$query = $this->db->get("products");
                $query = $this->db->get();
		$data["rows"] = $query->result_array();
		
		$query = $this->db->query("SELECT FOUND_ROWS() AS cnt");
		$result_found_rows = $query->row_array();
		$data["count"] = $result_found_rows["cnt"];
				
		return $data;

        
	}
        
        public function get_last_products()
        {
            return $this->get_all_products('', 0, 5, "published DESC");
        }
        
        public function product_exist($value, $param)
	{
		$this->db->where($param, $value);
		$this->db->from("products");
		$this->db->limit(1);
		
		return ( $this->db->count_all_results() > 0 ) ? true : false;
	}
        
        public function get_product_info($product_id)
	{
		$query = $this->db->get_where("products", array("id" => $product_id));
		
                return $query->row_array();
	}

        public function get_product_image($file_id)
	{
		$query = $this->db->get_where("files", array("id" => $file_id));
		
                return $query->row_array();
	}
        
        public function get_minimal_price($product_id)
	{
		$this->db->select('MIN(price) as price');
                $this->db->from('products_options');
                $this->db->where('product_id', $product_id);
                
                $query = $this->db->get();

                $result = $query->row_array();

                return $result;
	}
        
        public function get_product_options($product_id)
        {
            $query = $this->db->get_where("products_options", array("product_id" => $product_id));
		
            $result = $query->result_array();

            return $result;
        }
        
        public function get_product_categories($product_id)
        {
            $query = $this->db->get_where("products_categories", array("product_id" => $product_id));

            $result = $query->result_array();

            $product_categories = array();

            foreach($result as $data)
            {
                $product_categories[] = $data['category_id'];
            }

            return $product_categories;
        }
        
        public function update_categories($product_id, $categories_array)
        {
            if($this->product_exist($product_id, "id"))
            {
                // Remove All categories to this products. By this way user can add/remove categories easily.
                $this->db->delete("products_categories", array("product_id" => $product_id));
                
                if(is_array($categories_array) && count($categories_array) > 0)
                {
                    // Add all checked categories
                    foreach($categories_array as $category_id)
                    {
                        $this->db->insert("products_categories", array("product_id" => $product_id, "category_id" => $category_id));
                    }
                }
            }
            else
            {
                redirect("/admin/products/");
            }
        }
        
        public function add_image($product_id, $type)
        {
            $this->load->library("upload", array("allowed_types" => "*"));
            $product_info = $this->get_product_info($product_id);
            
            $old_file_id = $product_info['file_id'];
/*
            switch ($type)
            {
                case "logo": $old_file_id = $expert_info->logo_id; break;
                case "photo": $old_file_id = $expert_info->photo_id; break;
            }
*/
            if($old_file_id > 0)
                $this->upload->remove($old_file_id);

            $path = $this->upload->create_upload_path();
           
            $this->upload->set_upload_path($path);
            $this->upload->do_upload('file_id');
            $file = $this->upload->insert_into_db();

            if(is_array($file) && isset($file["id"]) && $file["id"] > 0)
            {
                $this->load->library('image_lib');
                
                $config['image_library'] = 'gd2';
                $config['source_image'] = $file['full_path'];
                $config['create_thumb'] = FALSE;
                $config['new_image'] = $path.'thum/new_image.jpg';
                
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 460;
                $config['height'] = 334;

                $this->load->library('image_lib', $config);

                if(!$this->image_lib->resize())
                {
                    echo $this->image_lib->display_errors();
                }
                
                $this->update($product_id, array( 'file_id' => $file["id"] ));
                $file["old_file_id"] = $old_file_id;

                return $file;
            }

            return array();
        }
}
?>