<?php
class Emailsstatistics_model extends CI_Model
{
	function logExist($email_id, $email, $event_id)
	{
		$this->db->where(array("email" => $email, "email_id" => $email_id, "event_id" => $event_id));
		$this->db->from("emails_statistics_logs");
		$this->db->limit(1);
		
		return (bool)($this->db->count_all_results() > 0);
	}
	
	function getAllEmailsStatistics()
	{
		$this->db->select("emails_statistics_logs.*, subject, emails.published AS email_published");
		$this->db->join("emails", "emails_statistics_logs.email_id = emails.id", "inner");
		$this->db->group_by("email_id");
		$this->db->order_by("emails.published desc");
		$query = $this->db->get("emails_statistics_logs");
		$rows = $query->result_array();
		
		if ($rows)
		{
			$events = $this->getEmailStatisticsEvents();
			foreach ($rows as $key => $row)
			{
				$rows[$key]["events"] = array();
				foreach ($events as $event)
				{
					$this->db->from("emails_statistics_logs");
					$this->db->where(array("email_id" => $row["email_id"], "event_id" => $event["id"]));
					$event["cnt"] = $this->db->count_all_results();
					$rows[$key]["events"][$event["id"]] = $event;
				}
			}
		}
		
		return $rows;
	}
	
	function checkForEmailStatistics()
	{ 
		if( $this->input->get("e_id") && (int)$this->input->get("e_id") > 0)
		{
			if( isset( $_COOKIE["email_id"] ) && $_COOKIE["email_id"] )
			{
				if ( (int)$this->input->get("e_id") != $_COOKIE["email_id"] )
				{					
					setcookie( "email_id", (int)$this->input->get("e_id"), ( time() + 60 * 60 * 24 * 30 ), "/" );
					setcookie( "email_statistics_email", $this->decrypt( $this->input->get("email") ), ( time() + 60 * 60 * 24 * 30 ), "/" );
					
					if( isset( $_SESSION["email_statistics"] ) )
					{
						$_SESSION["email_statistics"]["email_id"] = (int)$this->input->get("e_id");
						$_SESSION["email_statistics"]["email"] = $this->decrypt( $this->input->get("email") );
						$this->registerEvent( EC_EVENT_LOAD_WEBSITE );
					}
				}
			}
		}
		
		//www.forex-combo.com/?e_id=2323&email=22
		if( isset( $_SESSION["email_statistics"] ) )
			return;		
			
		if( isset( $_COOKIE["email_id"] ) && $_COOKIE["email_id"] )
		{
			$email_id = $_COOKIE["email_id"];			
			$email = $_COOKIE["email_statistics_email"];
				
			$_SESSION["email_statistics"] = array();
			$_SESSION["email_statistics"]["email_id"] = $email_id;
			$_SESSION["email_statistics"]["email"] = $email;
			
			$this->registerEvent( EC_EVENT_LOAD_WEBSITE );
		}
		
		if( isset( $_SESSION["email_statistics"] ) )
			return;	
				
		
		if( $this->input->get("e_id") && (int)$this->input->get("e_id") > 0)
		{
			$email_id = $this->input->get("e_id");
			$email = $this->decrypt( $this->input->get("email") );
			
			$_SESSION["email_statistics"] = array();
			$_SESSION["email_statistics"]["email_id"] = $email_id;
			$_SESSION["email_statistics"]["email"] = $email;
			
			$this->registerEvent( EC_EVENT_LOAD_WEBSITE );
			
			setcookie( "email_id", $email_id, ( time() + 60 * 60 * 24 * 30 ), "/" );
			setcookie( "email_statistics_email", $email, ( time() + 60 * 60 * 24 * 30 ), "/" );
		}
	}
	
	function registerEvent($event_id)
	{
		if( isset( $_SESSION["email_statistics"] ) )
		{
			$parameters = array();
			$parameters["email_id"] = $_SESSION["email_statistics"]["email_id"];
			$parameters["email"] = $_SESSION["email_statistics"]["email"];
			$parameters["event_id"] = $event_id;
			$parameters["published"] = now();
			
			if ( $this->logExist( $parameters["email_id"], $parameters["email"], $event_id ) )
				return false;
			
			$result = $this->db->insert("emails_statistics_logs", $parameters);
			if( $result )
			{
				$log_id = $this->db->insert_id();
				return $log_id;
			}
	  		else
	  		{
	  			return false;
	  		}  	
		}
		else
			return false;
	}
	
	function registerOpenMailEvent($email_id, $email)
	{
		$parameters = array();
		$parameters["email_id"] = $email_id;
		$parameters["email"] = $email;
		$parameters["event_id"] = EC_EVENT_OPEN_MAIL;
		$parameters["published"] = now();
		
		if ( $this->logExist( $parameters["email_id"], $parameters["email"], EC_EVENT_OPEN_MAIL ) )
				return false;
				
		$result = $this->db->insert("emails_statistics_logs", $parameters);
				
		return $result;
	}
	
	function registerOrderEvent( $email )
	{
		$this->db->where("email", $email);
		$this->db->order_by("published desc");
		$this->db->limit(1);
		$query = $this->db->get("emails_statistics_logs");
		$row = $query->row_array();
		
		if ( $row && is_array( $row ) && count( $row ) > 0 )
		{
		
			$parameters = array();
			$parameters["email_id"] = $row["email_id"];
			$parameters["email"] = $email;
			$parameters["event_id"] = EC_EVENT_ORDER;
			$parameters["published"] = now();
			
			if ( $this->logExist( $parameters["email_id"], $parameters["email"], EC_EVENT_ORDER ) )
				return false;
			
			$result = $this->db->insert("emails_statistics_logs", $parameters);
			if( $result )
			{
				$log_id = $this->db->insert_id();
				return $log_id;
			}
	  		else
	  		{
	  			return false;
	  		}  	
		}
	}
	
	function encrypt( $data )
	{
		$iv_size = mcrypt_get_iv_size( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB );
		$iv = mcrypt_create_iv( $iv_size, MCRYPT_RAND );
		$key = "OTEA email campaign key";
		
		$encrypted_data = mcrypt_encrypt( MCRYPT_RIJNDAEL_256, $key, $data, MCRYPT_MODE_ECB, $iv );
				
		return urlencode( base64_encode( $encrypted_data ) );
	}
	
	function decrypt( $data )
	{
		$iv_size = mcrypt_get_iv_size( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB );
		$iv = mcrypt_create_iv( $iv_size, MCRYPT_RAND );
		$key = "OTEA email campaign key";
				
		$decrypted_data = mcrypt_decrypt( MCRYPT_RIJNDAEL_256, $key, base64_decode( $data ), MCRYPT_MODE_ECB, $iv );
		
		$decrypted_data = trim( $decrypted_data, "\0" );
		
		return $decrypted_data;
	}
	
	function loadEmailStatisticsConstants()
	{
		$query = $this->db->get("emails_statistics_events");
		$rows = $query->result_array();
		if( $rows && is_array( $rows ) && count( $rows ) > 0 )
		{
			foreach( $rows as $row )
			{
				if( !defined( $row["constant"] ) )
					define( $row["constant"], $row["id"] );
			}
		}
		return $rows;
	}
	
	function getEmailStatisticsEvents()
	{
		$query = $this->db->get("emails_statistics_events");
		$rows = $query->result_array();
		
		return $rows;
	}

}
?>