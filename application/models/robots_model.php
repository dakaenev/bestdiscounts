<?php
class Robots_model extends CI_Model
{
	public function add_version($data)
	{
		$insert = $this->db->insert("robot_versions", $data);
		if($insert)
			return $this->db->insert_id();
			
		return false;		
	}

	public function add_version_file($data)
	{
		$insert = $this->db->insert("robot_versions_files", $data);
		if($insert)
			return $this->db->insert_id();
			
		return false;		
	}
	
	public function update_robot_version($version_id, $data)
	{
		$this->db->where("id", $version_id);
		
		return $this->db->update("robot_versions", $data);
	}

	public function update_robot_version_file($id, $data)
	{
		$this->db->where("id", $id);
		
		return $this->db->update("robot_versions_files", $data);
	}
	
	public function get_all_versions()
	{
		$this->db->order_by("version DESC");
		$query = $this->db->get("robot_versions");
		
		$all_versions = $query->result();
		if ($all_versions)
		{
			foreach ($all_versions as $key => $version)
			{
				$all_versions[$key]->files = $this->get_version_files($version->id);
			}
		}
		
		return $all_versions;
	}
	
	public function get_version_file_by_type($version_id, $type)
	{
		$query = $this->db->get_where("robot_versions_files", array("version_id" => $version_id, "type" => $type));
		
		return $query->row();
	}

	public function get_version_file_by_filename($filename)
	{
		$this->db->limit(1);
		$query = $this->db->get_where("robot_versions_files", array("filename" => $filename));
		
		return $query->row();
	}
	
	public function remove_robot_version_file($id)
	{
		$this->db->delete("robot_versions_files", array("id" => $id));
	}
	
	public function get_version_info_by_id($version_id)
	{
		$query = $this->db->get_where("robot_versions", array("id" => $version_id));
		$version_info = $query->row();
		$version_info->files = $this->get_version_files($version_id);
		
		return $version_info;
	}
	
	public function set_current_version($version_id)
	{
		return $this->db->simple_query("UPDATE robot_versions SET is_current = IF(id = $version_id, 1, 0)");
	}
	
	public function delete_robot_version($version_id)
	{
		$version_info = $this->get_version_info_by_id($version_id);
		if ($version_info)
		{
			if (is_array($version_info->files) && $version_info->files)
			{
				foreach ($version_info->files as $file)
				{
					remove_file(FCPATH . substr($file->full_path, 1));
				}
			}
			
			$this->db->delete("robot_versions", array("id" => $version_id));
			$this->db->delete("robot_versions_files", array("version_id" => $version_id));
			
			return true;
		}
		
		return false;
	}
	
	public function get_version_files($version_id)
	{
		$this->db->where("version_id", $version_id);
		$query = $this->db->get("robot_versions_files");
		
		$files = array();
		$result = $query->result();
		if (is_array($result) && $result)
		{
			foreach ($result as $row)
			{
				$files[$row->type] = $row;
			}
		}
		
		return $files;
	}	
	
	public function is_version_unique($version, $version_id = 0)
	{
		$this->db->where(array("version" => $version));
		if ($version_id)
			$this->db->where("id !=", $version_id);
			
		$this->db->from("robot_versions");

		return ($this->db->count_all_results() > 0) ? false : true;
	}
	
	public function get_current_version()
	{
		$this->db->where("is_current", 1);
		$query = $this->db->get("robot_versions");
		$row = $query->row();
		
		if (isset($row->id) && $row->id)
		{
			$row->files = $this->get_version_files($row->id);
			
			return $row;
		}
		
		return null;
	}
}
?>