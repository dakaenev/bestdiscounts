<?php
class Statistics_model extends CI_Model
{
	public function get_stats_start_date()
	{
		$this->db->select("published");
		$this->db->where("payment_processor != 'admin'");
		$this->db->order_by("published ASC");
		$query = $this->db->get("users_products_contracts");
		$row = $query->row();
	
		if (isset($row->published))
			return $row->published;
			
		return null;
	}
	
	public function get_new_customers($start_period, $end_period)
	{
		$this->db->from("users");
		$this->db->where("users.published >= '" . $start_period . "'");
		$this->db->where("users.published <= '" . $end_period . "'");
		$this->db->where("users.user_type = 2");
		$this->db->join("users_products_contracts", "users.id = users_products_contracts.user_id AND product_id = 1 AND contract_id = 1 AND payment_processor != 'admin'", "inner");
		$this->db->group_by("users.id");
		$query = $this->db->get();
		
		return count($query->result_array());
	}
	
	public function get_contract_stats($contract_id, $start_period, $end_period, $refund = false)
	{
		$this->db->from("users_products_contracts");
		$this->db->where("product_id = 1");
		$this->db->where("contract_id", $contract_id);
		$this->db->where("published >= '" . $start_period . "'");
		$this->db->where("published <= '" . $end_period . "'");
		$this->db->where("payment_processor != 'admin'");

		if ($refund)
			$this->db->where("refund = 1");
		
		return $this->db->count_all_results();
	}
	
	public function get_total_stats($start_period, $end_period, $refund = false)
	{
		$this->db->from("users_products_contracts");
		$this->db->where("payment_processor != 'admin'");
		$this->db->where("product_id = 1");
		$this->db->where("published >= '" . $start_period . "'");
		$this->db->where("published <= '" . $end_period . "'");

		if ($refund)
			$this->db->where("refund = 1");

		return $this->db->count_all_results();
	}
	
	public function get_payment_processor_stats($payment_processor, $start_period, $end_period, $refund = false)
	{
		$this->db->from("users_products_contracts");
		$this->db->where("payment_processor = '" . $payment_processor . "'");
		$this->db->where("product_id = 1");
		$this->db->where("published >= '" . $start_period . "'");
		$this->db->where("published <= '" . $end_period . "'");
		
		if ($refund)
			$this->db->where("refund = 1");
			
		return $this->db->count_all_results();
	}
	
}
?>