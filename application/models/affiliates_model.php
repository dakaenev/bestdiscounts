<?php
class Affiliates_model extends CI_Model
{
	public function add_affiliate($data)
	{
		$insert = $this->db->insert("affiliates", $data);
		if($insert)
			return $this->db->insert_id();
			
		return false;		
	}

	public function update_affiliate($affiliate_id, $data)
	{
		$this->db->where("id", $affiliate_id);
		
		return $this->db->update("affiliates", $data);
	}

	public function get_all_affiliates($where = "", $keyword = "")
	{
		if ($where != "")
			$this->db->where($where);
			
		if ($keyword !== false && $keyword != "")
		{
			$this->db->or_like("name", $keyword, "both");
			$this->db->or_like("email", $keyword, "both");
			$this->db->or_like("clickbank", $keyword, "both");
			$this->db->or_like("regnow", $keyword, "both");
		}
		
		$query = $this->db->get("affiliates");
		$result = $query->result_array();
		
		if (is_array($result) && $result)
		{
			foreach ($result as $key => $affiliate)
			{
				$result[$key]["num_sales"] = $this->affiliate_sales($affiliate["id"], $affiliate["clickbank"], $affiliate["regnow"]);
			}
		}
		
		return $result;
	}
	
	public function affiliate_sales($affiliate_id, $clickbank, $regnow)
	{		
		if ($clickbank == "" && $regnow == "")
			return 0;
			
		if ($clickbank != "")
			$this->db->or_where("referrer", $clickbank);

		if ($regnow != "")
			$this->db->or_where("referrer", $regnow);
			
		$this->db->from("users");
		return $this->db->count_all_results();
	}
	
	public function get_affiliate_info_by_id($affiliate_id)
	{
		$query = $this->db->get_where("affiliates", array("id" => $affiliate_id));
		
		return $query->row_array();
	}

	public function get_affiliate_info_by_email($email)
	{
		$query = $this->db->get_where("affiliates", array("email" => $email));
		
		return $query->row_array();
	}
	
	public function exist($value, $field)
	{
		$this->db->from("affiliates");
		$this->db->where($field, $value);
		
		return (bool)($this->db->count_all_results() > 0);
	}

	public function delete_affiliate($affiliate_id)
	{
		return $this->db->delete("affiliates", array("id" => $affiliate_id));;
	}
	
}
?>