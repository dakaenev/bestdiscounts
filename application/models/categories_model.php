<?php
class Categories_model extends CI_Model
{
	public function add($data)
	{
		$insert = $this->db->insert("categories", $data);
		if($insert)
			return $this->db->insert_id();
			
		return false;		
	}
        
        public function update($category_id, $data)
	{
		$this->db->where("id", $category_id);
		$this->db->limit(1);
		
		return $this->db->update("categories", $data);		
	}
        
        public function delete($category_id)
	{
		$this->db->limit(1);
		$result = $this->db->delete("categories", array("id" => $category_id)); 
		
		return $result;
	}
        
        public function get_all_categories($where = "", $offset = 0, $limit = 0, $order_by = "")
	{
		$this->db->select("SQL_CALC_FOUND_ROWS *", false);
		if($where != "")
                {
                    $this->db->where($where);
                }
			
		if ($limit)
			$this->db->limit($limit, $offset);
		
		if ($order_by != "")
			$this->db->order_by($order_by); 
		
		$query = $this->db->get("categories");
		$data["rows"] = $query->result_array();
		
		$query = $this->db->query("SELECT FOUND_ROWS() AS cnt");
		$result_found_rows = $query->row_array();
		$data["count"] = $result_found_rows["cnt"];
				
		return $data;
	}
        
        public function category_exist($value, $param)
	{
		$this->db->where($param, $value);
		$this->db->from("categories");
		$this->db->limit(1);
		
		return ( $this->db->count_all_results() > 0 ) ? true : false;
	}
        
        public function get_category_info($category_id)
	{
		$query = $this->db->get_where("categories", array("id" => $category_id));
		
		return $query->row_array();
	}
        
        public function get_products_into_category($category_id = false, $where = '')
        {
            $this->db->select('*');
            $this->db->from('products_categories');
            $this->db->join('products', 'products.id = products_categories.product_id');
            
            $this->db->where('status', 1);
            $this->db->group_by('product_id');
            if(is_array($category_id))
            {
                $this->db->where_in("category_id",$category_id);
                $this->db->limit(3);
            }
            else
            {
                if($category_id){$this->db->where("category_id",$category_id);}
            }
            
            if($where != '')
            {
                $this->db->where($where);
            }
            $query = $this->db->get();

		
            $result = $query->result_array();
            
            return $result;
        }
        
        public function get_related_products($category_id = false, $product_id = false)
        {
            $this->db->select('*');
            $this->db->from('products_categories');
            $this->db->join('products', 'products.id = products_categories.product_id');
            
            $this->db->where('status', 1);
            $this->db->group_by('product_id');
            
            if(!is_array($category_id))
            {
                
            }
            
            $this->db->where_in("category_id",$category_id);
            $this->db->limit(3);
            
            $query = $this->db->get();
		
            $result = $query->result_array();
            
            return $result;
        }
        
        public function get_products_count_for_category($category_id = false)
        {
            $this->db->select('*');
            $this->db->from('products_categories');
            $this->db->join('products', 'products.id = products_categories.product_id');
            $this->db->where('status', 1);
            $this->db->group_by('product_id');
            if($category_id){$this->db->where("category_id",$category_id);}

            $query = $this->db->get();
            $query = $this->db->query("SELECT FOUND_ROWS() AS cnt");
	    $result_found_rows = $query->row_array();
            $result = $result_found_rows["cnt"];
            
            return $result;
        }
	/*
	
	
	public function get_user_info_by_email($email)
	{
		$query = $this->db->get_where("users", array("email" => $email));
		
		return $query->row_array();
	}
	
	public function is_email_available( $email, $user_id = 0 )
	{
		$this->db->from( "users" );		
		$this->db->like( "email", $email );
		if ( $user_id > 0 )
			$this->db->where( "id <>", $user_id );
			
		return ( $this->db->count_all_results() > 0 ) ? false : true;
	}
		
	public function set_user_cookie($user_info)
	{
		$this->load->library('encrypt');					
		$text = $user_info["id"] . "|" . $user_info["user_type"];
		$encrypted_user_data = $this->encrypt->encode($text);
	//	$cookie_data = array( "name" => "user_data", "value" => $encrypted_user_data, "expire" => ( time() + 60 * 60 * 24 * 7 ), "path" => "/" );
		setcookie( "user_data", $encrypted_user_data, ( time() + 60 * 60 * 24 * 7 ), "/" );
	}
	
	public function log_user($user_info)
	{
		$_SESSION["user"] 				= array();
		$_SESSION["user"]["id"] 		= $user_info["id"];
		$_SESSION["user"]["type"]		= $user_info["user_type"];
		$_SESSION["user"]["email"] 		= $user_info["email"];
		$_SESSION["user"]["name"] 		= $user_info["first_name"] . " " . $user_info["last_name"];
		$_SESSION["user"]["serialized"] = serialize( $user_info );
		$_SESSION["user"]["is_logged"] 	= true;
		
		$this->update($user_info["id"], array("last_login" => now()));
	}
		
	
	
	public function check_for_cookie_data_and_log_user()
	{
		$cookie = ( isset( $_COOKIE["user_data"] ) ) ? $_COOKIE["user_data"] : "";
		if($cookie)
		{	
			$this->load->library('encrypt');
			$decrypted_user_data = $this->encrypt->decode($cookie);
			$user_data = trim( $decrypted_user_data, "\0" );
			$user_data_arr = explode( "|", $user_data );
		//	var_dump($user_data);exit;
			
			$user_info = $this->get_user_info( $user_data_arr[0] );
			if($user_info)
			{
				$this->log_user($user_info);
				return true;
			}
		}
	}
		
	public function logout()
	{
		if ( isset( $_SESSION["user"] ) )
		{
			unset( $_SESSION["user"] );
			delete_cookie("user_data");
		}
	}
        */
}
?>