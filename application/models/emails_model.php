<?php
class Emails_model extends CI_Model
{
	public function add($data)
	{
		$email_id = 0;
		
		if (isset($data["recipients"]) && $data["recipients"])
		{
			$recipients = $data["recipients"];
			unset($data["recipients"]);
		}

		if (isset($data["attachments"]) && $data["attachments"])
		{
			$attachments = $data["attachments"];
			unset($data["attachments"]);
		}
		
		$data["published"] = now();
		
		$insert = $this->db->insert("emails", $data);
		if($insert)
		{
			$email_id = $this->db->insert_id();
			
			if (isset($recipients))
			{
				foreach ($recipients as $recipient)
				{
					$data_recipient = array();
					$data_recipient["email_id"] 		= $email_id;
					$data_recipient["recipient_name"]	= $recipient["recipient_name"];
					$data_recipient["recipient_email"] 	= $recipient["recipient_email"];
					$data_recipient["type"] 			= $recipient["type"];
					$data_recipient["unsubscribe"] 		= (isset($recipient["unsubscribe"])) ? $recipient["unsubscribe"] : "";
					
					$this->db->insert("emails_recipients", $data_recipient);
				}
			}

			if (isset($attachments))
			{
				foreach ($attachments as $attachment)
				{
					$data_attachment = array();
					$data_attachment["email_id"] 		= $email_id;
					$data_attachment["file_id"]			= $attachment["file_id"];
					$data_attachment["delete_after"] 	= $attachment["delete_after"];
					
					$this->db->insert("emails_attachments", $data_attachment);
				}
			}
		}
			
		return $email_id;	
	}
		
	public function get_all_emails_from_db($where = "", $order_by = "", $limit = 0, $offset = 0)
	{
		if ($where != "")
			$this->db->where($where);
		if ($order_by != "")	
			$this->db->order_by($order_by);
		if ($limit > 0)	
			$this->db->limit($limit, $offset);
		
		$this->db->select("emails.*, emails_recipients.id AS recipient_id, recipient_name, recipient_email, type, unsubscribe");
		$this->db->join("emails_recipients", "emails.id = emails_recipients.email_id", "inner");
		$query = $this->db->get('emails');
		
		$emails = array();
		
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$this->db->where("email_id", $row->id);
				$this->db->join('files', 'emails_attachments.file_id = files.id');
				$attachments = $this->db->get('emails_attachments');
				$row->attachments = array();
				foreach ($attachments->result() as $attachment)
				{
					$row->attachments[] = $attachment;
				}
				
				$emails[] = $row;
			}
		}
		
		return $emails;
	}
	
	public function add_attachment($file_id, $delete_after = 0)
	{
		$this->attachments[] = array("file_id" => $file_id, "delete_after" => $delete_after);
	}
	
	public function update_email_status($status, $email_id)
	{
		$this->db->set('status', $status);
		$this->db->set('tried', 'tried + 1', false);
		if ($status == 1) 
			$this->db->set('sent_date', now());
		$this->db->where("id", $email_id);

		$this->db->update('emails'); 
	}

	public function update_recipient_status($status, $recipient_id)
	{
		$this->db->set('status', $status);
		$this->db->set('tried', 'tried + 1', false);
		if ($status == 1) 
			$this->db->set('sent_date', now());
		$this->db->where("id", $recipient_id);

		$this->db->update('emails_recipients'); 
	}
	
	public function get_predefined_data($type)
	{
		$query = $this->db->get_where("emails_predefined_data", array("type" => $type));
		
		return $query->result_array();
	}
	
	public function add_predefined_data($data)
	{
		$insert = $this->db->insert("emails_predefined_data", $data);
		if ($insert)
			return $this->db->insert_id();
			
		return null;
	}
	
	public function update_predefined_data($value, $type)
	{
		if (strlen($value) == 0) return;
		
		if (!$this->predefined_data_exist($value, "value", $type))
		{
			$parameters = array();
			$parameters["value"] = $value;
			$parameters["type"] = $type;
			$this->add_predefined_data($parameters);
		}
	}
	
	public function predefined_data_exist($value, $param, $type)
	{
		$this->db->from("emails_predefined_data");
		$this->db->where($param, $value);
		$this->db->where("type", $type);
		
		$count = $this->db->count_all_results();
		
		return (bool)($count > 0);
	}
	
	public function get_email_info_by_id($email_id)
	{
		$data = array();
	
		$this->db->limit(1);
		$query = $this->db->get_where("emails", array("id" => $email_id));
		
		$data = $query->row_array();
		
		$this->db->select("recipient_name, recipient_email, type");
		$query = $this->db->get_where("emails_recipients", array("email_id" => $email_id));
		$data["recipients"] = $query->result_array();
		
		$this->db->select("file_name, full_path");
		$this->db->join("files", "emails_attachments.file_id = files.id");
		$query = $this->db->get_where("emails_attachments", array("email_id" => $email_id));
		$data["attachments"] = $query->result_array();
		
		return $data;
	}
	
	public function get_all_emails($where = "", $limit = 0, $offset = 0)
	{
		$data = array("rows" => array(), "count" => 0);
		$this->db->select("SQL_CALC_FOUND_ROWS emails.*", false);
		
		if ($where != "")
		{
			$this->db->join('emails_recipients', 'emails_recipients.email_id = emails.id');
			$this->db->where($where);
		}
		
		if ($limit)
			$this->db->limit($limit, $offset);
			
		$this->db->order_by("emails.published DESC");		
		$query = $this->db->get("emails");
		$data["rows"] = $query->result_array();
		
		$query = $this->db->query("SELECT FOUND_ROWS() as cnt");
		$count_arr = $query->row_array();
		$data["count"] = $count_arr["cnt"];
		
		return $data;
	}
	
	
}
?>