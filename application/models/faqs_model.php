<?php
class Faqs_model extends CI_Model
{
	public function get_all($where = "")
	{
		if ($where != "")	
			$this->db->where($where);
			
		$this->db->order_by("sort_order ASC");
		$query = $this->db->get("faqs");
		
		return $query->result_array();
	}
	
	public function add_faq($data)
	{
		$insert = $this->db->insert("faqs", $data);
		if($insert)
			return $this->db->insert_id();
			
		return false;		
	}

	public function update_faq($faq_id, $data)
	{
		$this->db->where("id", $faq_id);
		
		return $this->db->update("faqs", $data);
	}
	
	public function get_faq_info_by_id($faq_id)
	{
		$query = $this->db->get_where("faqs", array("id" => $faq_id));
		
		return $query->row_array();
	}

	public function delete_faq($faq_id)
	{
		return $this->db->delete("faqs", array("id" => $faq_id));;
	}
	
}
?>