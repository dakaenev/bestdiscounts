<?php /* Smarty version Smarty-3.1.13, created on 2014-10-04 19:13:41
         compiled from "application\templates\admin\emails.htm" */ ?>
<?php /*%%SmartyHeaderCode:737254282a096e0591-83196152%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '53f0bb88cc2486d5b2260bf317cbf98e9c1b03ba' => 
    array (
      0 => 'application\\templates\\admin\\emails.htm',
      1 => 1412442820,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '737254282a096e0591-83196152',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_54282a09973e84_15236680',
  'variables' => 
  array (
    'action' => 0,
    'alert_message' => 0,
    'active_clients' => 0,
    'client' => 0,
    'active_affiliates' => 0,
    'affiliate' => 0,
    'email_to' => 0,
    'email_from' => 0,
    'predefined_from' => 0,
    'pf' => 0,
    'name' => 0,
    'predefined_name' => 0,
    'subject' => 0,
    'predefined_subject' => 0,
    'predefined_message' => 0,
    'index' => 0,
    'message' => 0,
    'make_statistics' => 0,
    'keyword' => 0,
    'all_emails' => 0,
    'email' => 0,
    'navigator' => 0,
    'email_info' => 0,
    'attachment' => 0,
    'recipient' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54282a09973e84_15236680')) {function content_54282a09973e84_15236680($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include 'D:\\xampp\\htdocs\\bestdiscountshere_com\\application\\libraries\\smarty\\plugins\\modifier.date_format.php';
?><ul class="nav nav-tabs">
	<li <?php if ($_smarty_tpl->tpl_vars['action']->value=="view_sent"||$_smarty_tpl->tpl_vars['action']->value==''){?>class="active"<?php }?>><a href="/admin/emails/view_sent">Виж изпратените е-мейли</a></li>
	<li <?php if ($_smarty_tpl->tpl_vars['action']->value=="send"){?>class="active"<?php }?>><a href="/admin/emails/send">Изпрати е-мейл</a></li>
	<li <?php if ($_smarty_tpl->tpl_vars['action']->value=="email_statistics"){?>class="active"<?php }?>><a href="/admin/email_statistics">Статистика</a></li>
</ul>

<?php if ($_smarty_tpl->tpl_vars['action']->value=="send"){?>
	<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			CKEDITOR.replace('message', { height: 400 });
		});
	</script>
	
	<legend>Изпрати е-мейл</legend>
	
	<?php if (isset($_smarty_tpl->tpl_vars['alert_message']->value)){?><?php echo $_smarty_tpl->tpl_vars['alert_message']->value;?>
<?php }?>

	<form method="post" action="" enctype="multipart/form-data">
		<div class="clearfix">
			<div class="pull-left">
				<label>Активни клиенти:</label>
				<select name="active_clients[]" multiple="multiple" class="input-xxlarge" onchange="selectAll(this);">
				<?php if ($_smarty_tpl->tpl_vars['active_clients']->value['count']){?>
					<option value="all">------------------------------------------- Избери всички -------------------------------------------</option>
					<?php  $_smarty_tpl->tpl_vars['client'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['client']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['active_clients']->value['rows']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['client']->key => $_smarty_tpl->tpl_vars['client']->value){
$_smarty_tpl->tpl_vars['client']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['client']->key;
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['client']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['client']->value['first_name'];?>
 <?php echo $_smarty_tpl->tpl_vars['client']->value['last_name'];?>
 &lt;<?php echo $_smarty_tpl->tpl_vars['client']->value['email'];?>
&gt;</option>
					<?php } ?>
				<?php }?>
				</select><br />
				<label class="checkbox">
					<input type="checkbox" name="unsubscribe_link[u]" value="1" <?php if (isset($_POST['unsubscribe_link']['u'])){?>checked="checked"<?php }?> /> Добави линк за отписване
				</label>				
			</div>
			<div class="pull-left" style="margin-left: 30px;">
				<label>Партньори:</label>
				<select name="active_affiliates[]" multiple="multiple" class="input-xxlarge" onchange="selectAll(this);">
				<?php if ($_smarty_tpl->tpl_vars['active_affiliates']->value){?>
					<option value="all">------------------------------------------- Избери всички -------------------------------------------</option>
					<?php  $_smarty_tpl->tpl_vars['affiliate'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['affiliate']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['active_affiliates']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['affiliate']->key => $_smarty_tpl->tpl_vars['affiliate']->value){
$_smarty_tpl->tpl_vars['affiliate']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['affiliate']->key;
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['affiliate']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['affiliate']->value['name'];?>
 &lt;<?php echo $_smarty_tpl->tpl_vars['affiliate']->value['email'];?>
&gt;</option>
					<?php } ?>
				<?php }?>
				</select><br />
				<label class="checkbox">
					<input type="checkbox" name="unsubscribe_link[a]" value="1" <?php if (isset($_POST['unsubscribe_link']['a'])){?>checked="checked"<?php }?> /> Добави линк за отписване
				</label>								
			</div>
		</div>
		
		<div class="space-30px"></div>
		<label>До</label>
		<input type="text" name="email_to" class="input-xxlarge" value="<?php if (isset($_smarty_tpl->tpl_vars['email_to']->value)){?><?php echo $_smarty_tpl->tpl_vars['email_to']->value;?>
<?php }?>" /> или 
		<input type="file" name="csv_emails" /> (CSV name;email)

		<label>От</label>
		<input type="text" name="email_from" id="email_from" class="input-xxlarge" value="<?php if (isset($_smarty_tpl->tpl_vars['email_from']->value)){?><?php echo $_smarty_tpl->tpl_vars['email_from']->value;?>
<?php }?>" /> или 
		<select name="predefined_from" id="predefined_from" class="input-xxlarge" onchange="$( 'input#email_from' ).val( $( 'select#predefined_from' ).val() );">
			<option value="">избери</option>
			<?php  $_smarty_tpl->tpl_vars['pf'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['pf']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['predefined_from']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['pf']->key => $_smarty_tpl->tpl_vars['pf']->value){
$_smarty_tpl->tpl_vars['pf']->_loop = true;
?>
				<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pf']->value['value'], ENT_QUOTES, 'UTF-8', true);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pf']->value['value'], ENT_QUOTES, 'UTF-8', true);?>
</option>
			<?php } ?>
		</select>

		<label>Име</label>
		<input type="text" name="name" id="name" class="input-xxlarge" value="<?php if (isset($_smarty_tpl->tpl_vars['name']->value)){?><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
<?php }?>" /> или 
		<select name="predefined_name" id="predefined_name" class="input-xxlarge" onchange="$( 'input#name' ).val( $( 'select#predefined_name' ).val() );">
			<option value="">избери</option>
			<?php  $_smarty_tpl->tpl_vars['pf'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['pf']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['predefined_name']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['pf']->key => $_smarty_tpl->tpl_vars['pf']->value){
$_smarty_tpl->tpl_vars['pf']->_loop = true;
?>
				<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pf']->value['value'], ENT_QUOTES, 'UTF-8', true);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pf']->value['value'], ENT_QUOTES, 'UTF-8', true);?>
</option>
			<?php } ?>
		</select>

		<label>Относно</label>
		<input type="text" name="subject" id="subject" class="input-xxlarge" value="<?php if (isset($_smarty_tpl->tpl_vars['subject']->value)){?><?php echo $_smarty_tpl->tpl_vars['subject']->value;?>
<?php }?>" /> или 
		<select name="predefined_subject" id="predefined_subject" class="input-xxlarge" onchange="$( 'input#subject' ).val( $( 'select#predefined_subject' ).val() );">
			<option value="">избери</option>
			<?php  $_smarty_tpl->tpl_vars['pf'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['pf']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['predefined_subject']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['pf']->key => $_smarty_tpl->tpl_vars['pf']->value){
$_smarty_tpl->tpl_vars['pf']->_loop = true;
?>
				<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pf']->value['value'], ENT_QUOTES, 'UTF-8', true);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pf']->value['value'], ENT_QUOTES, 'UTF-8', true);?>
</option>
			<?php } ?>
		</select>
		
		<label>Text <a href="javascript: void(0);" onclick="$('div#predefined_message').slideToggle();">(кликни за избор)</a></label>
		<?php if (isset($_smarty_tpl->tpl_vars['predefined_message']->value)&&is_array($_smarty_tpl->tpl_vars['predefined_message']->value)&&$_smarty_tpl->tpl_vars['predefined_message']->value){?>
			<div class="space-5px"></div>
			<div style="display: none; height: 500px; overflow: auto; border: 1px solid #666;" id="predefined_message">
				<?php  $_smarty_tpl->tpl_vars['pf'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['pf']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['predefined_message']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['pf']->key => $_smarty_tpl->tpl_vars['pf']->value){
$_smarty_tpl->tpl_vars['pf']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['pf']->key;
?>
					<div id="predefined_message_<?php echo $_smarty_tpl->tpl_vars['index']->value+1;?>
" style="cursor: pointer; background-color: #fff; margin: 10px 0; padding: 5px 10px; font-size: 11px;" onclick="CKEDITOR.instances.message.setData( $( 'div#predefined_message_<?php echo $_smarty_tpl->tpl_vars['index']->value+1;?>
' ).html() ); $('div#predefined_message').slideToggle();">
						<?php echo $_smarty_tpl->tpl_vars['pf']->value['value'];?>

					</div>
				<?php } ?>
			</div>
			<div class="space-5px"></div>
		<?php }?>
		
		<textarea name="message" class="input-xxlarge"><?php if (isset($_smarty_tpl->tpl_vars['message']->value)){?><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
<?php }?></textarea>
		<div class="space-20px"></div>
		
		<div class="clearfix">
			<div class="pull-left">
				<label class="checkbox">
					<input type="checkbox" name="make_statistics" id="make_statistics" <?php if (isset($_smarty_tpl->tpl_vars['make_statistics']->value)){?>checked="checked"<?php }?> value="1" /> Пази статистика
				</label>
			
				<label>Файлове</label>
				<input type="file" name="file_1" /><br />
				<input type="file" name="file_2" /><br />
				<input type="file" name="file_3" /><br />
				<input type="file" name="file_4" /><br />
				<input type="file" name="file_5" />
			</div>
			<div class="pull-right">
				<div class="clearfix">
					<div class="pull-right"><input type="checkbox" name="use_template" value="1" /></div>
					<div class="pull-right" style="padding: 2px 5px 0 0;">Използвай темплейт</div>
				</div>
				<div class="space-20px"></div>
				<button class="btn btn-info btn-large" style="width: 200px;">Изпрати</button>
			</div>
		</div>
		<input type="hidden" name="send_email" value="1" />
	</form>

<?php }elseif($_smarty_tpl->tpl_vars['action']->value=="view_sent"){?>

	
	<div class="clearfix">
		<h1 class="pull-left">Изпратени е-мейли</h1>
		
		<div class="input-append pull-right" style="padding-top: 15px;">
			<form action="/admin/emails/view_sent" method="get" style="margin: 0;">
				<input class="input-medium" id="appendedInputButtons" size="16" type="text" name="keyword" value="<?php if (isset($_smarty_tpl->tpl_vars['keyword']->value)&&$_smarty_tpl->tpl_vars['keyword']->value){?><?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>"><button class="btn" type="submit">Търсене</button>
			</form>
		</div>
	</div>
	<table class="table table-striped">
		<thead>
			<tr>
			<th style="text-align: center;">ID</th>
			<th style="text-align: center;">Име</th>
			<th style="text-align: center;">Е-мейл</th>
			<th style="text-align: center;">Относно</th>
			<th style="text-align: center;">Дата на добавяне</th>
			<th style="text-align: center;">Статус</th>
			<th style="text-align: right;">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
		<?php if ($_smarty_tpl->tpl_vars['all_emails']->value['count']>0){?>
			<?php  $_smarty_tpl->tpl_vars['email'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['email']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['all_emails']->value['rows']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['email']->key => $_smarty_tpl->tpl_vars['email']->value){
$_smarty_tpl->tpl_vars['email']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['email']->key;
?>
			<tr>
				<td style="text-align: center;"><?php echo $_smarty_tpl->tpl_vars['email']->value['id'];?>
</td>
				<td style="text-align: left;"><?php echo $_smarty_tpl->tpl_vars['email']->value['from_name'];?>
</td>
				<td style="text-align: left;"><?php echo $_smarty_tpl->tpl_vars['email']->value['from_email'];?>
</td>
				<td style="text-align: left;"><?php echo $_smarty_tpl->tpl_vars['email']->value['subject'];?>
</td>
				<td style="text-align: center;"><?php echo $_smarty_tpl->tpl_vars['email']->value['published'];?>
</td>
				<td style="text-align: center;"><img src="/images/admin/<?php if ($_smarty_tpl->tpl_vars['email']->value['status']){?>yes<?php }else{ ?>no<?php }?>.png" alt="<?php if ($_smarty_tpl->tpl_vars['email']->value['status']){?>Active<?php }else{ ?>Inactive<?php }?>" /></td>
				<td style="text-align: center;"><button class="btn btn-small btn-info" type="button"onclick="window.location = '/admin/emails/view/<?php echo $_smarty_tpl->tpl_vars['email']->value['id'];?>
';"><i class="icon-info-sign icon-white"></i> Преглед</button></td>
			</tr>
			<?php } ?>
		<?php }?>
		</tbody>
	</table>
	
	<?php echo $_smarty_tpl->tpl_vars['navigator']->value;?>

	
<?php }elseif($_smarty_tpl->tpl_vars['action']->value=="view"){?>

	<h1><?php echo $_smarty_tpl->tpl_vars['email_info']->value['subject'];?>
</h1>
	
	<legend style="margin-bottom: 10px;">Personal info</legend>
	<div class="well">
		<table border="0" cellpadding="2" cellspacing="0" width="100%">
			<tr>
				<td width="65%">
					<strong>Sender: </strong>
					<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['email_info']->value['from_name'], ENT_QUOTES, 'UTF-8', true);?>
 &lt;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['email_info']->value['from_email'], ENT_QUOTES, 'UTF-8', true);?>
&gt;
				</td>
				<td>
					<strong>Published: </strong>
					<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['email_info']->value['published'],"%d/%m/%Y %H:%M:%S");?>

				</td>
			</tr>
			<tr>
				<td>
					<strong>Subject: </strong>
					<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['email_info']->value['subject'], ENT_QUOTES, 'UTF-8', true);?>

				</td>
				<td>
					<strong>Priority: </strong>
					<?php echo $_smarty_tpl->tpl_vars['email_info']->value['priority'];?>

				</td>
			</tr>
			<tr>
				<td>
					<strong>Status: </strong>
					<?php if ($_smarty_tpl->tpl_vars['email_info']->value['status']==0&&$_smarty_tpl->tpl_vars['email_info']->value['tried']==5){?><span class="text-error">Not sent</span><?php }elseif($_smarty_tpl->tpl_vars['email_info']->value['status']==0&&$_smarty_tpl->tpl_vars['email_info']->value['tried']<5){?><span class="text-warning">Waiting</span><?php }elseif($_smarty_tpl->tpl_vars['email_info']->value['status']==1){?><span class="text-success">Sent</span><?php }?>
				</td>
				<td>
					<strong>Attachments: </strong>
					<?php if (isset($_smarty_tpl->tpl_vars['email_info']->value['attachments'])&&is_array($_smarty_tpl->tpl_vars['email_info']->value['attachments'])&&count($_smarty_tpl->tpl_vars['email_info']->value['attachments'])>0){?>
					<?php  $_smarty_tpl->tpl_vars['attachment'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['attachment']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['email_info']->value['attachments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['attachment']->key => $_smarty_tpl->tpl_vars['attachment']->value){
$_smarty_tpl->tpl_vars['attachment']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['attachment']->key;
?>
						<?php if ($_smarty_tpl->tpl_vars['index']->value!=0){?>, <?php }?>
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attachment']->value['file_name'], ENT_QUOTES, 'UTF-8', true);?>
 
					<?php } ?>
					<?php }else{ ?>
						<span class="text-error">None</span>
					<?php }?>				
				</td>
			</tr>
			<tr>
				<td>
					<strong>Recipients: </strong>				
					<?php  $_smarty_tpl->tpl_vars['recipient'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recipient']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['email_info']->value['recipients']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recipient']->key => $_smarty_tpl->tpl_vars['recipient']->value){
$_smarty_tpl->tpl_vars['recipient']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['recipient']->key;
?>
						<?php if ($_smarty_tpl->tpl_vars['index']->value!=0){?>, <?php }?>
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['recipient']->value['recipient_name'], ENT_QUOTES, 'UTF-8', true);?>
 &lt;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['recipient']->value['recipient_email'], ENT_QUOTES, 'UTF-8', true);?>
&gt; <?php if ($_smarty_tpl->tpl_vars['recipient']->value['type']==2){?>(hidden copy)<?php }?> 
					<?php } ?>
				</td>
				<td>&nbsp;</td>
			</tr>			
		</table>
	</div>

	<legend style="margin-bottom: 10px;">Content</legend>
	<div class="well"><?php echo $_smarty_tpl->tpl_vars['email_info']->value['mail_content'];?>
</div>

<?php }?><?php }} ?>