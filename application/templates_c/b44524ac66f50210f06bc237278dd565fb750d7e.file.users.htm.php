<?php /* Smarty version Smarty-3.1.13, created on 2014-10-04 21:39:47
         compiled from "application\templates\admin\users.htm" */ ?>
<?php /*%%SmartyHeaderCode:9355535fe3d7b04a96-33207972%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b44524ac66f50210f06bc237278dd565fb750d7e' => 
    array (
      0 => 'application\\templates\\admin\\users.htm',
      1 => 1412451584,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9355535fe3d7b04a96-33207972',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_535fe3d87372d2_28745870',
  'variables' => 
  array (
    'action' => 0,
    'error' => 0,
    'redirect' => 0,
    'keyword' => 0,
    'users' => 0,
    'num_users' => 0,
    'user' => 0,
    'clients_cnt' => 0,
    'offset' => 0,
    'navigator' => 0,
    'alert_message' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535fe3d87372d2_28745870')) {function content_535fe3d87372d2_28745870($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include 'D:\\xampp\\htdocs\\bestdiscountshere_com\\application\\libraries\\smarty\\plugins\\modifier.date_format.php';
?><?php if (isset($_smarty_tpl->tpl_vars['action']->value)&&$_smarty_tpl->tpl_vars['action']->value=="login"){?>
	<div id="login" class="span4">
	
		<form action="/admin/users/login" method="post" class="clearfix" style="margin: 0;">
			<legend>CONTROL PANEL</legend>
			
			<?php if (isset($_smarty_tpl->tpl_vars['error']->value)&&$_smarty_tpl->tpl_vars['error']->value){?>
				<div class="alert alert-error">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $_smarty_tpl->tpl_vars['error']->value;?>

				</div>
			<?php }?>
			
			<input type="hidden" name="redirect" value="<?php if (isset($_smarty_tpl->tpl_vars['redirect']->value)&&$_smarty_tpl->tpl_vars['redirect']->value){?><?php echo $_smarty_tpl->tpl_vars['redirect']->value;?>
<?php }?>" />
	
			<input type="text" name="email" placeholder="email" class="span4" />
			<input type="password" name="password" placeholder="password" class="span4" />
			
			<label class="checkbox">
				<input type="checkbox" name="remember_me" id="remember_me" value="1" /> Запомни ме
			</label>
			
			<input type="submit" name="login" value="Вход" class="btn btn-info pull-right" />
			
		</form>
	</div>

<?php }elseif(isset($_smarty_tpl->tpl_vars['action']->value)&&($_smarty_tpl->tpl_vars['action']->value=="customers"||$_smarty_tpl->tpl_vars['action']->value=="inactive_customers"||$_smarty_tpl->tpl_vars['action']->value=="administrators")){?>

    <ul class="nav nav-tabs">
		<li <?php if ($_smarty_tpl->tpl_vars['action']->value=="customers"){?>class="active"<?php }?>><a href="/admin/users/customers">Клиенти</a></li>
		<li <?php if ($_smarty_tpl->tpl_vars['action']->value=="inactive_customers"){?>class="active"<?php }?>><a href="/admin/users/inactive_customers">Неактивни клиенти</a></li>
		<li <?php if ($_smarty_tpl->tpl_vars['action']->value=="administrators"){?>class="active"<?php }?>><a href="/admin/users/administrators">Администратори</a></li>
		<li><a href="/admin/users/add">Добави нов потребител</a></li>
    </ul>
	
	<?php if ($_smarty_tpl->tpl_vars['action']->value=="customers"){?>
		<h1 class="pull-left">Клиенти</h1>
	<?php }elseif($_smarty_tpl->tpl_vars['action']->value=="inactive_customers"){?>
		<h1 class="pull-left">Неактивни клиенти</h1>
	<?php }else{ ?>
		<h1 class="pull-left">Администратори</h1>
	<?php }?>
	
	<div class="input-append pull-right" style="padding-top: 15px;">
		<form action="/admin/users/<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
" method="get" style="margin: 0;">
			<input class="input-medium" id="appendedInputButtons" size="16" type="text" name="keyword" value="<?php if (isset($_smarty_tpl->tpl_vars['keyword']->value)&&$_smarty_tpl->tpl_vars['keyword']->value){?><?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>"><button class="btn" type="submit">Търсене</button>
		</form>
	</div>

	<table class="table table-striped">
		<thead>
			<tr>
			<th style="text-align: center;">#</th>
			<th style="text-align: center; padding-left: 0; padding-right: 0;">&nbsp;</th>
			<th style="text-align: left;">Име</th>
			<th style="text-align: left;">Фамилия</th>
			<th style="text-align: left;">Е-мейл</th>
			<th style="text-align: center;">&nbsp;</th>
			<th style="text-align: center;">Дата на добавяне</th>
			<th style="text-align: right;">&nbsp;</th>
			</tr>
		</thead>
	
		<tbody>
		
		<?php if (isset($_smarty_tpl->tpl_vars['users']->value)&&$_smarty_tpl->tpl_vars['users']->value){?>
			<?php $_smarty_tpl->tpl_vars["clients_cnt"] = new Smarty_variable($_smarty_tpl->tpl_vars['num_users']->value, null, 0);?>
			<?php  $_smarty_tpl->tpl_vars['user'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['users']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['user']->key => $_smarty_tpl->tpl_vars['user']->value){
$_smarty_tpl->tpl_vars['user']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['user']->key;
?>
				<tr id="row_user_<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['user']->value['is_affiliate']==1){?>class="error"<?php }elseif(date('Y-m-d')==smarty_modifier_date_format($_smarty_tpl->tpl_vars['user']->value['published'],"%Y-%m-%d")){?>class="success"<?php }?>>
					<td style="text-align: center;"><?php echo $_smarty_tpl->tpl_vars['clients_cnt']->value-$_smarty_tpl->tpl_vars['offset']->value;?>
</td>
					<td  style="text-align: center; padding-left: 0; padding-right: 0;">
						<?php if (strlen($_smarty_tpl->tpl_vars['user']->value['comment'])>0){?>
							<i class="icon-comment icon-red user-comment" id="user_comment_<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['comment'], ENT_QUOTES, 'UTF-8', true);?>
" onclick="addEditUserComment('<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
');"></i>
						<?php }else{ ?>
							<i class="icon-comment user-comment" id="user_comment_<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
" title="Click to add comment!" onclick="addEditUserComment('<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
');"></i>
						<?php }?>
					</td>
					<td style="text-align: left;"><?php echo $_smarty_tpl->tpl_vars['user']->value['first_name'];?>
</td>
					<td style="text-align: left;"><?php echo $_smarty_tpl->tpl_vars['user']->value['last_name'];?>
</td>
					<td style="text-align: left;"><?php echo $_smarty_tpl->tpl_vars['user']->value['email'];?>
</td>
					<td style="text-align: center;"><div class="<?php if ($_smarty_tpl->tpl_vars['user']->value['country']){?>flag flag-<?php echo mb_strtolower($_smarty_tpl->tpl_vars['user']->value['country'], 'UTF-8');?>
<?php }?>"></div></td>
					<td style="text-align: center;"><?php echo $_smarty_tpl->tpl_vars['user']->value['published'];?>
</td>
					<td style="text-align: right;">
						<button class="btn btn-small btn-info" type="button"onclick="window.location = '/admin/users/edit/<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
';"><i class="icon-edit icon-white"></i> Промени</button>&nbsp;&nbsp;
						<button class="btn btn-small btn-danger" type="button" onclick="delete_user('<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
');"><i class="icon-remove icon-white"></i> Изтрий</button></td>
				</tr>
				<?php $_smarty_tpl->tpl_vars["clients_cnt"] = new Smarty_variable($_smarty_tpl->tpl_vars['clients_cnt']->value-1, null, 0);?>
			<?php } ?>
		<?php }else{ ?>
			<tr>
				<td colspan="8">Няма резултат!</td>
			</tr>
		<?php }?>
			
		</tbody>
	</table>
	
	
	
	<script type="text/javascript" language="javascript">
	
		$(document).ready(function() {

			$('i.user-comment').tooltip({placement: "right"});
		});
		
	</script>
	

	<?php echo $_smarty_tpl->tpl_vars['navigator']->value;?>


	

<?php }elseif(isset($_smarty_tpl->tpl_vars['action']->value)&&$_smarty_tpl->tpl_vars['action']->value=="edit"){?>

	<ul class="nav nav-tabs">
		<li><a href="/admin/users/customers">Клиенти</a></li>
		<li><a href="/admin/users/inactive_customers">Неактивни клиенти</a></li>
		<li><a href="/admin/users/administrators">Администратори</a></li>
		<li><a href="/admin/users/add">Добави нов потребител</a></li>
    </ul>
		
	<form action="/admin/users/edit/<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
" method="post" style="margin: 0;">
		<input type="hidden" name="save" value="1" />		
		<legend><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['first_name'], ENT_QUOTES, 'UTF-8', true);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['last_name'], ENT_QUOTES, 'UTF-8', true);?>
's profile</legend>
		
		<?php if (isset($_smarty_tpl->tpl_vars['alert_message']->value)&&$_smarty_tpl->tpl_vars['alert_message']->value){?><?php echo $_smarty_tpl->tpl_vars['alert_message']->value;?>
<?php }?>
		
		<div class="clearfix">
			<div class="pull-left" style="">
				<label>Име:</label>
				<input type="text" name="first_name" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['first_name'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['first_name'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Фамилия:</label>
				<input type="text" name="last_name" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['last_name'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['last_name'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Държава:</label>
				<input type="text" name="country" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['country'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['country'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Град:</label>
				<input type="text" name="city" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['city'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['city'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>ПК:</label>
				<input type="text" name="post_code" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['post_code'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['post_code'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Адрес</label>
				<textarea name="address" style="width: 350px;"><?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['address'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['address'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?></textarea>
			</div>
			<div class="pull-left" style="margin-left: 30px;">
				<label>Е-мейл:</label>
				<input type="text" name="email" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['email'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['email'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Допълнителен е-мейл:</label>
				<input type="text" name="additional_email" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['additional_email'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['additional_email'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Телефон</label>
				<input type="text" name="phone" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['phone'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['phone'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Референт:</label>
				<input type="text" name="referrer" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['referrer'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['referrer'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Фирма:</label>
				<input type="text" name="company" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['company'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['company'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Коментар:</label>
				<textarea name="comment" style="width: 350px;"><?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['comment'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['comment'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?></textarea>
			</div>
			<div class="pull-left" style="margin-left: 30px; width: 370px;">
				<label>Парола:</label>
				<input type="password" name="password" style="width: 350px;" value="" />
				<br /><br />
				<label class="clearfix">
					Статус: 
					<span class="pull-right">
						<?php if ($_smarty_tpl->tpl_vars['user']->value['status']==1){?><span class="label label-success" id="user_status">Active</span><?php }else{ ?><span class="label label-important" id="user_status">Неактивен</span><?php }?> 
						<button type="button" class="btn btn-small btn-info" onclick="changeUserStatus('<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
');"><i class="icon-refresh icon-white"></i> Смени</button>
					</span>
				</label>
				
				<label class="clearfix">Създаден на: <span class="label label-info pull-right"><?php echo $_smarty_tpl->tpl_vars['user']->value['published'];?>
</span></label>
				<label class="clearfix">Последно влизане: <span class="label pull-right"><?php if ($_smarty_tpl->tpl_vars['user']->value['last_login']){?><?php echo $_smarty_tpl->tpl_vars['user']->value['last_login'];?>
<?php }else{ ?>no info<?php }?></span></label>
				<label class="clearfix">IP Аддрес:</label>
				<label class="checkbox">
					<input type="checkbox" name="is_affiliate" value="1" <?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php if ($_smarty_tpl->tpl_vars['data']->value['is_affiliate']==1){?>checked="checked"<?php }?><?php }else{ ?><?php if ($_smarty_tpl->tpl_vars['user']->value['is_affiliate']==1){?>checked="checked"<?php }?><?php }?> /> Партньор
				</label>
				<label class="checkbox">
					<input type="checkbox" name="newsletter" value="1" <?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php if ($_smarty_tpl->tpl_vars['data']->value['newsletter']==1){?>checked="checked"<?php }?><?php }else{ ?><?php if ($_smarty_tpl->tpl_vars['user']->value['newsletter']==1){?>checked="checked"<?php }?><?php }?> /> Бюлетин
				</label>
				<br /><br />
				<div style="text-align: center; padding-top: 78px;"><button type="submit" class="btn btn-info btn-large" style="width: 200px;">Запази</button></div>
			</div>
		</div>
	</form>

<?php }elseif(isset($_smarty_tpl->tpl_vars['action']->value)&&$_smarty_tpl->tpl_vars['action']->value=="add"){?>

	<ul class="nav nav-tabs">
		<li><a href="/admin/users/customers">Клиенти</a></li>
		<li><a href="/admin/users/inactive_customers">Неактивни клиенти</a></li>
		<li><a href="/admin/users/administrators">Администратори</a></li>
		<li class="active"><a href="/admin/users/add">Добави нов потребител</a></li>
    </ul>
		
	<form action="/admin/users/add" method="post" style="margin: 0;">
		<input type="hidden" name="add" value="1" />		
		<legend>Добавяне на нов потребител</legend>
		
		<?php if (isset($_smarty_tpl->tpl_vars['alert_message']->value)&&$_smarty_tpl->tpl_vars['alert_message']->value){?><?php echo $_smarty_tpl->tpl_vars['alert_message']->value;?>
<?php }?>
		
		<div class="clearfix">
			<div class="pull-left" style="">
				<label>Име:</label>
				<input type="text" name="first_name" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['first_name'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Фамилия:</label>
				<input type="text" name="last_name" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['last_name'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Държава:</label>
				<input type="text" name="country" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['country'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Град:</label>
				<input type="text" name="city" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['city'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>ПК:</label>
				<input type="text" name="post_code" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['post_code'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Адрес:</label>
				<textarea name="address" style="width: 350px;"><?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['address'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?></textarea>
			</div>
			<div class="pull-left" style="margin-left: 30px;">
				<label>Е-мейл:</label>
				<input type="text" name="email" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['email'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Допълнителен е-мейл:</label>
				<input type="text" name="additional_email" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['additional_email'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Телефон:</label>
				<input type="text" name="phone" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['phone'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Референт:</label>
				<input type="text" name="referrer" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['referrer'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Фирма:</label>
				<input type="text" name="company" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['company'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Коментар:</label>
				<textarea name="comment" style="width: 350px;"><?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['comment'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?></textarea>
			</div>
			<div class="pull-left" style="margin-left: 30px; width: 370px;">
				<label>Парола:</label>
				<input type="password" name="password" style="width: 350px;" value="" />
				
				
				
				<br /><br />
				<label class="checkbox inline">
					<input type="checkbox" name="status" value="1" <?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php if ($_smarty_tpl->tpl_vars['data']->value['status']==1){?>checked="checked"<?php }?><?php }else{ ?>checked="checked"<?php }?> /> Активен
				</label>
				<label class="checkbox inline">
					<input type="checkbox" name="is_affiliate" value="1" <?php if (isset($_smarty_tpl->tpl_vars['data']->value)&&$_smarty_tpl->tpl_vars['data']->value['is_affiliate']==1){?>checked="checked"<?php }?> /> Партньор
				</label>
				<label class="checkbox inline">
					<input type="checkbox" name="newsletter" value="1" <?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php if ($_smarty_tpl->tpl_vars['data']->value['newsletter']==1){?>checked="checked"<?php }?><?php }else{ ?>checked="checked"<?php }?> /> Бюлетин
				</label>
				
				<br /><br />
				<label class="radio inline">
					<input type="radio" name="user_type" value="2" <?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php if ($_smarty_tpl->tpl_vars['data']->value['user_type']==2){?>checked="checked"<?php }?><?php }elseif(!isset($_smarty_tpl->tpl_vars['data']->value)){?>checked="checked"<?php }?> /> Клиент
				</label>
				<label class="radio inline">
					<input type="radio" name="user_type" value="1" <?php if (isset($_smarty_tpl->tpl_vars['data']->value)&&$_smarty_tpl->tpl_vars['data']->value['user_type']==1){?>checked="checked"<?php }?> /> Администратор
				</label>
								
								
				<div style="text-align: center; padding-top: 57px;"><button type="submit" class="btn btn-info btn-large" style="width: 200px;">Добави потребител</button></div>
			</div>
		</div>
	</form>

<?php }?>

                    <?php }} ?>