<?php /* Smarty version Smarty-3.1.13, created on 2014-10-04 21:22:36
         compiled from "application\templates\page_not_found.htm" */ ?>
<?php /*%%SmartyHeaderCode:7887535fe3b1ec6161-48670181%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ae33d6da36b21282680189d4032b07e60f0ae582' => 
    array (
      0 => 'application\\templates\\page_not_found.htm',
      1 => 1412450555,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7887535fe3b1ec6161-48670181',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_535fe3b21335f7_46774986',
  'variables' => 
  array (
    'categories_list' => 0,
    'products_list' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535fe3b21335f7_46774986')) {function content_535fe3b21335f7_46774986($_smarty_tpl) {?><div class="container floated">
<!-- Sidebar -->
	<div class="four floated sidebar left">
		<aside class="sidebar">

			<!-- Search -->
			<nav class="widget-search">
				<form action="" method="get">
					<button class="search-btn-widget"></button>
					<input class="search-field" type="text" onblur="if(this.value=='')this.value='Search';" onfocus="if(this.value=='Search')this.value='';" value="Search" />
				</form>
			</nav>
			<div class="clearfix"></div>

			<!-- Categories -->
			<nav class="widget">
				<h4>Категории</h4>
				<ul class="categories">
					<?php echo $_smarty_tpl->tpl_vars['categories_list']->value;?>

				</ul>
			</nav>

			<!-- Products -->
			<div class="widget">
				<h4>Последни 5 добавени оферти</h4>
				<ul class="categories">
					<?php echo $_smarty_tpl->tpl_vars['products_list']->value;?>

				</ul>
			</div>

			<!-- Tweets-->
			<div class="widget">
				<h4>Twitter</h4>
				<ul id="twitter-blog"></ul>
					<script type="text/javascript">
						jQuery(document).ready(function($){
						$.getJSON('twitter.php?url='+encodeURIComponent('statuses/user_timeline.json?screen_name=Vasterad&count=2'), function(tweets){
							$("#twitter-blog").html(tz_format_twitter(tweets));
						}); });
					</script>
				<div class="clearfix"></div>
			</div>
		</aside>
	</div>
	<!-- Sidebar / End -->
        <div class="eleven floated right">
            <div class="shop-page page-content">
	
		<div class="row">
			<div class="span1" style="margin: 0;">&nbsp;</div>
			<div class="span7">
				<div class="page-not-found-main">
					<h2>404 <i class="icon-file"></i></h2>
					<p>Тази страница я няма</p>
					<p>Най-вероятно тя е преместена, или изобщо липсва.</p>
					<p>Ако проблема продължава <a href="<?php echo base_url();?>
contact-us" title="Contact us">пишете ни</a></p>
				</div>
			</div>
			<div class="span4">
				<h4>Някои полезни линкове</h4>
				<ul class="nav nav-list primary">
					<li><a href="<?php echo base_url();?>
">Начало</a></li>
					<li><a href="<?php echo base_url();?>
about-us">За нас</a></li>
					<li><a href="<?php echo base_url();?>
frequently-asked-questions">Ч.З.В.</a></li>
					<li><a href="<?php echo base_url();?>
Sitemap">Карта на сайта</a></li>
					<li><a href="<?php echo base_url();?>
contact-us">Контакт</a></li>
				</ul>
                                <br>
			</div>
		</div>
	
</div></div>
</div>
<?php }} ?>