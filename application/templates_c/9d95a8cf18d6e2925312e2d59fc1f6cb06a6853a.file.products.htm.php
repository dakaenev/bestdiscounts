<?php /* Smarty version Smarty-3.1.13, created on 2014-10-04 21:54:26
         compiled from "application\templates\admin\products.htm" */ ?>
<?php /*%%SmartyHeaderCode:1378654282c6167b4f9-69334525%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9d95a8cf18d6e2925312e2d59fc1f6cb06a6853a' => 
    array (
      0 => 'application\\templates\\admin\\products.htm',
      1 => 1412452464,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1378654282c6167b4f9-69334525',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_54282c61a0f196_30097376',
  'variables' => 
  array (
    'action' => 0,
    'keyword' => 0,
    'products' => 0,
    'num_products' => 0,
    'product' => 0,
    'products_cnt' => 0,
    'offset' => 0,
    'navigator' => 0,
    'alert_message' => 0,
    'product_image' => 0,
    'data' => 0,
    'categories_list' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54282c61a0f196_30097376')) {function content_54282c61a0f196_30097376($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include 'D:\\xampp\\htdocs\\bestdiscountshere_com\\application\\libraries\\smarty\\plugins\\modifier.date_format.php';
?><?php if (isset($_smarty_tpl->tpl_vars['action']->value)&&($_smarty_tpl->tpl_vars['action']->value=="show_all")){?>

    <ul class="nav nav-tabs">
		<li <?php if ($_smarty_tpl->tpl_vars['action']->value=="show_all"){?>class="active"<?php }?>><a href="/admin/products/show_all">Преглед на всички</a></li>
		<li><a href="/admin/products/add">Добави нов продукт</a></li>
    </ul>
	
	<?php if ($_smarty_tpl->tpl_vars['action']->value=="show_all"){?>
		<h1 class="pull-left">Преглед на всички</h1>
	<?php }?>
	
	<div class="input-append pull-right" style="padding-top: 15px;">
		<form action="/admin/products/<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
" method="get" style="margin: 0;">
			<input class="input-medium" id="appendedInputButtons" size="16" type="text" name="keyword" value="<?php if (isset($_smarty_tpl->tpl_vars['keyword']->value)&&$_smarty_tpl->tpl_vars['keyword']->value){?><?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>"><button class="btn" type="submit">Търсене</button>
		</form>
	</div>

	<table class="table table-striped">
		<thead>
			<tr>
			<th class="w25px center">#</th>
			<th class="center">Заглавие</th>
			<th class="w140px center">Дата на добавяне</th>
                        <th class="w140px center">Последна промяна</th>
			<th class="w160px center">&nbsp;</th>
			</tr>
		</thead>
	
		<tbody>
		
		<?php if (isset($_smarty_tpl->tpl_vars['products']->value)&&$_smarty_tpl->tpl_vars['products']->value){?>
			<?php $_smarty_tpl->tpl_vars["products_cnt"] = new Smarty_variable($_smarty_tpl->tpl_vars['num_products']->value, null, 0);?>
			<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value){
$_smarty_tpl->tpl_vars['product']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['product']->key;
?>
				<tr id="row_product_<?php echo $_smarty_tpl->tpl_vars['product']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['product']->value['status']==0){?>class="error"<?php }elseif(date('Y-m-d')==smarty_modifier_date_format($_smarty_tpl->tpl_vars['product']->value['published'],"%Y-%m-%d")){?>class="success"<?php }?>>
                                
					<td class="center"><?php echo $_smarty_tpl->tpl_vars['products_cnt']->value-$_smarty_tpl->tpl_vars['offset']->value;?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['product']->value['title'];?>
</td>
					<td class="center"><?php echo $_smarty_tpl->tpl_vars['product']->value['published'];?>
</td>
                                        <td class="center"><?php if ($_smarty_tpl->tpl_vars['product']->value['modified']==''){?> - <?php }else{ ?> <?php echo $_smarty_tpl->tpl_vars['product']->value['modified'];?>
 <?php }?></td>
					<td class="right" style="width:230px;">
                                                <button class="btn btn-small" onclick="var win = window.open('/product/<?php echo $_smarty_tpl->tpl_vars['product']->value['id'];?>
', '_blank'); win.focus();"><i class="icon-play"></i></button>
						<button class="btn btn-small btn-info" type="button"onclick="window.location = '/admin/products/edit/<?php echo $_smarty_tpl->tpl_vars['product']->value['id'];?>
';"><i class="icon-edit icon-white"></i> Промени</button>&nbsp;&nbsp;
						<button class="btn btn-small btn-danger" type="button" onclick="delete_product('<?php echo $_smarty_tpl->tpl_vars['product']->value['id'];?>
');"><i class="icon-remove icon-white"></i> Изтрий</button>
                                        </td>
				</tr>
				<?php $_smarty_tpl->tpl_vars["products_cnt"] = new Smarty_variable($_smarty_tpl->tpl_vars['products_cnt']->value-1, null, 0);?>
			<?php } ?>
		<?php }else{ ?>
			<tr>
				<td colspan="5">Няма резултат!</td>
			</tr>
		<?php }?>
			
		</tbody>
	</table>
	
	
	
	<script type="text/javascript" language="javascript">
	
		$(document).ready(function() {

			$('i.user-comment').tooltip({placement: "right"});
		});
		
	</script>
	

	<?php echo $_smarty_tpl->tpl_vars['navigator']->value;?>


	

<?php }elseif(isset($_smarty_tpl->tpl_vars['action']->value)&&$_smarty_tpl->tpl_vars['action']->value=="edit"){?>
<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
                CKEDITOR.replace('s_description', { height: 150});
                CKEDITOR.replace('f_description', { height: 400});
                
                $('#false_file').click(function(){
                    $("#file_id").click();
                });
        });
</script>
	<ul class="nav nav-tabs">
		<li><a href="/admin/products/show_all">Преглед на всички</a></li>
		<li><a href="/admin/products/add">Добави нов продукт</a></li>
    </ul>
		
	<form action="/admin/products/edit/<?php echo $_smarty_tpl->tpl_vars['product']->value['id'];?>
" enctype="multipart/form-data" method="post" style="margin: 0;">
		<input type="hidden" name="save" value="1" />		
		<legend>Product <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
</legend>
                <input type="file" name="file_id" id="file_id" style="display:none;">
		<?php if (isset($_smarty_tpl->tpl_vars['alert_message']->value)&&$_smarty_tpl->tpl_vars['alert_message']->value){?><?php echo $_smarty_tpl->tpl_vars['alert_message']->value;?>
<?php }?>
		
		<div class="clearfix">
			<div class="span3 pull-left">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['product_image']->value['full_path'];?>
" target="_blank">
                                <img alt="<?php echo $_smarty_tpl->tpl_vars['product_image']->value['file_name'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['product_image']->value['full_path'];?>
" style="display:block; margin-bottom:3px;" />
                            </a>
                            <button type="button" id="false_file" class="btn btn-small btn-inverse"><?php if ($_smarty_tpl->tpl_vars['product']->value['file_id']>0){?><i class="icon-refresh icon-white"></i> Смени снимката<?php }else{ ?><i class="icon-plus icon-white"></i> Добави снимка<?php }?></button> 
			</div>
			
                        <div class="span3 pull-left">
				<label>Заглавие:</label>
				<input type="text" class="w98percent" name="title" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                               
                                <label>URL:</label>
                                <input type="text" class="w98percent" name="url" disabled="disabled" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                
                                <label class="clearfix">
                                    <label>Статус:</label>
                                    <span class="pull-right">
                                            <?php if ($_smarty_tpl->tpl_vars['product']->value['status']==1){?><span class="label label-success" id="product_status">Активен</span><?php }else{ ?><span class="label label-important" id="product_status">Неактивен</span><?php }?> 
                                            <button type="button" class="btn btn-small btn-info" onclick="changeProductStatus('<?php echo $_smarty_tpl->tpl_vars['product']->value['id'];?>
');"><i class="icon-refresh icon-white"></i> Смени</button>
                                    </span>
				</label> 
			</div>
			<div class="span3 pull-left">
                                <label>Meta title:</label>
				<input type="text" class="w98percent" name="meta_title" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['meta_title'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['meta_title'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Meta description:</label>
				<input type="text" class="w98percent" name="meta_description" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['meta_description'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['meta_description'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Meta keywords:</label>
				<input type="text" class="w98percent" name="meta_keywords" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['meta_keywords'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['meta_keywords'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
			</div>
                        <div class="span3 pull-left">
				<label>Добавяне / Премахване на категории</label>
				<select class="w98percent" name="categories[]" style="height:160px;" multiple="multiple">
                                <?php echo $_smarty_tpl->tpl_vars['categories_list']->value;?>
    
                                </select>
			</div>
                </div>
                <br />
                <label>Кратко описание</label>
                <textarea name="s_description" class="input-xxlarge"><?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['short_description'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['short_description'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?></textarea>
		<br />
                <br />
                <label>Пълно описание</label>
                <textarea name="f_description" class="input-xxlarge"><?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['description'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['description'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?></textarea>
                <div class="clearfix">
                    <div class="span4" style="padding-top: 10px;"><a href="#myModal" role="button" data-toggle="modal" class="btn btn-info btn-large" style="width: 200px;" onclick="load_product_options(<?php echo $_smarty_tpl->tpl_vars['product']->value['id'];?>
);"><i class="icon-tags icon-white"></i> Атрибути</a></div>
                    <div class="span4" style="padding-top: 10px;">
                        <label class="clearfix">Създаден: <span class="label label-info pull-right"><?php echo $_smarty_tpl->tpl_vars['product']->value['published'];?>
</span></label>
                        <label class="clearfix">Променен: <span class="label pull-right"><?php if ($_smarty_tpl->tpl_vars['product']->value['modified']){?><?php echo $_smarty_tpl->tpl_vars['product']->value['modified'];?>
<?php }else{ ?>no info<?php }?></span></label>
                    </div>
                    <div class="span4 right" style="padding-top: 10px;"><button type="submit" class="btn btn-info btn-large" style="width: 200px;"><i class="icon-ok icon-white"></i> Запази</button></div>
                </div>              
	</form>

        <!-- Modal -->
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel"><span class="btn btn-info" onclick="prepare_insert_option();"><i class="icon-plus icon-white"></i></span> Атрибути</h3>
            </div>
            <div class="modal-body">
                <div id="response_div" class="alert alert-success hide"></div> 
                <table class="table table-striped" id="load_result" border="1">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Заглавие</th>
                            <th class="w160px">Промяна</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <div id="add_edit_form" style="display:none !important;">
                    <h5><u id="action_title">Вмъкване на атрибут</u></h5>
                    <form id="options_form">
                        <div class="span6 left">
                                <label class="clearfix"> Заглавие <input type="text" name="option_title" class="w98percent" /></label>
                                <label class="clearfix"> URL за купуване: <input type="text" name="url_b" class="w98percent" /></label>
                                <label class="clearfix"> Цена: <input type="text" name="price_now" class="w98percent" /></label>
                                <label class="clearfix"> Намаление: <input type="text" name="discount" class="w98percent" /></label>
                        </div>
                        <div class="span6 left">
                            <label class="clearfix"> Описание: <input type="text" name="descr" class="w98percent" /></label>
                            <label class="clearfix"> URL за преглед: <input type="text" name="url_v" class="w98percent" /></label>
                            <label class="clearfix"> Стара цена: <input type="text" name="price_old" class="w98percent" /></label>
                            <label class="clearfix"> Купон за отстъпка: <input type="text" name="coupon" class="w98percent" /></label>
                        </div>
                        <input type="hidden" name="id" />
                        <input type="hidden" name="product_id" value="<?php echo $_smarty_tpl->tpl_vars['product']->value['id'];?>
" />
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button id="add_edit_form_cancel" style="display: none !important;" class="btn" onclick="cancel_form_option();">Откажи</button>
                <button id="add_edit_form_submit" style="display: none !important;" class="btn btn-primary" onclick="insert_update_option();">Запази</button>
            </div>
        </div>
<?php }elseif(isset($_smarty_tpl->tpl_vars['action']->value)&&$_smarty_tpl->tpl_vars['action']->value=="add"){?>
<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
                CKEDITOR.replace('s_description', { height: 150});
                CKEDITOR.replace('f_description', { height: 400});
        });
</script>
	<ul class="nav nav-tabs">
		<li><a href="/admin/products/show_all">Преглед на всички</a></li>
		<li class="active"><a href="/admin/products/add">Добави нов продукт</a></li>
    </ul>
		
	<form action="/admin/products/add" method="post" style="margin: 0;">
		<input type="hidden" name="add" value="1" />		
		<legend>Добави нов продукт</legend>
                
		<?php if (isset($_smarty_tpl->tpl_vars['alert_message']->value)&&$_smarty_tpl->tpl_vars['alert_message']->value){?><?php echo $_smarty_tpl->tpl_vars['alert_message']->value;?>
<?php }?>
		
		<div class="clearfix">
			<div class="span4 pull-left">
				<label>Заглавие:</label>
				<input type="text" class="w98percent" name="title" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				
                                <label>URL:</label>
                                <input type="text" class="w98percent" name="url" disabled="disabled" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
			
                                <label>Статус:</label>
                                    <label class="radio inline">
                                            <input type="radio" name="status" value="1" <?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php if ($_smarty_tpl->tpl_vars['data']->value['status']==1){?>checked="checked"<?php }?><?php }elseif(!isset($_smarty_tpl->tpl_vars['data']->value)){?>checked="checked"<?php }?> /> Active
                                    </label>
                                    <label class="radio inline">
                                            <input type="radio" name="status" value="0" <?php if (isset($_smarty_tpl->tpl_vars['data']->value)&&$_smarty_tpl->tpl_vars['data']->value['status']==0){?>checked="checked"<?php }?> /> Suspend
                                    </label>
			</div>

			<div class="span4 pull-left">
				<label>Meta Title:</label>
				<input type="text" class="w98percent" name="meta_title" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['meta_title'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				
                                <label>Meta Description:</label>
				<input type="text" class="w98percent" name="meta_description" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['meta_description'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				
                                <label>Meta Keywords:</label>
				<input type="text" class="w98percent" name="meta_keywords" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['meta_keywords'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
			</div>
                    
                        <div class="span4 pull-left">
				<label>Добавяне / Премахване на категория</label>
				<select class="w98percent" name="categories[]" style="height:160px;" multiple="multiple">
                                <?php echo $_smarty_tpl->tpl_vars['categories_list']->value;?>
    
                                </select>
			</div>
              
                </div>
                <br />
                <label>Кратко описание</label>
                <textarea name="s_description" class="input-xxlarge"><?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['short_description'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?></textarea>
		<br />
                <br />
                <label>Пълно описание</label>
                <textarea name="f_description" class="input-xxlarge"><?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['description'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?></textarea>
		<div style="text-align: center; padding-top: 10px;"><button type="submit" class="btn btn-info btn-large" style="width: 200px;">Добави продукта</button></div>
	</form>

<?php }?>

                    <?php }} ?>