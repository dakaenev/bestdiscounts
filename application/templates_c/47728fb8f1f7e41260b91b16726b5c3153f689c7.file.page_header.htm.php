<?php /* Smarty version Smarty-3.1.13, created on 2014-10-04 18:24:18
         compiled from "application\templates\page_header.htm" */ ?>
<?php /*%%SmartyHeaderCode:12713535fe3b1170073-86217888%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '47728fb8f1f7e41260b91b16726b5c3153f689c7' => 
    array (
      0 => 'application\\templates\\page_header.htm',
      1 => 1412439857,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12713535fe3b1170073-86217888',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_535fe3b11f4b05_84389072',
  'variables' => 
  array (
    'item_type' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535fe3b11f4b05_84389072')) {function content_535fe3b11f4b05_84389072($_smarty_tpl) {?><!-- Wrapper / Start -->
<div id="wrapper">

<!-- Header
================================================== -->
<div id="top-line"></div>

<!-- 960 Container -->
<div class="container">

	<!-- Header -->
	<header id="header">

		<!-- Logo -->
		<div class="ten columns">
			<div id="logo">
				<h1><!-- <a href="index.html"><img src="../../theme/images/logo.png" alt="лого" /></a> --> Helpful Bulgaria</h1>
				<div>Намаления без компромис!</div>
				<div class="clearfix"></div>
			</div>
		</div>

		<!-- Social / Contact -->
		<div class="six columns">

			<!-- Social Icons -->
			<ul class="social-icons">
				<li class="twitter"><a href="#">Twitter</a></li>
				<li class="facebook"><a href="#">Facebook</a></li>
				<li class="dribbble"><a href="#">Dribbble</a></li>
				<li class="linkedin"><a href="#">LinkedIn</a></li>
				<li class="rss"><a href="#">RSS</a></li>
			</ul>

			<div class="clearfix"></div>

			<!-- Contact Details -->
			<div class="contact-details">Телефон за контакт: +359 32 123 456</div>

			<div class="clearfix"></div>

			<!-- Search --><!--
			<nav class="top-search">
				<form action="404-page.html" method="get">
					<button class="search-btn"></button>
					<input class="search-field" type="text" onblur="if(this.value=='')this.value='Search';" onfocus="if(this.value=='Search')this.value='';" value="Search" />
				</form>
			</nav> -->

		</div>
	</header>
	<!-- Header / End -->

	<div class="clearfix"></div>

</div>
<!-- 960 Container / End -->


<!-- Navigation
================================================== -->
<nav id="navigation" class="style-1">

<div class="left-corner"></div>
<div class="right-corner"></div>

<ul class="menu" id="responsive">

	<li><a <?php if ($_smarty_tpl->tpl_vars['item_type']->value=="Home"){?>href="#" id="current"<?php }else{ ?>href="/index.php"<?php }?>><i class="halflings white home"></i> Начало</a></li>

	<li><a href="#"><i class="halflings white file"></i> За нас</a></li>

	<li><a href="#"><i class="halflings white envelope"></i> Контакт</a></li>
        <!--
	<li><a href="#"><i class="icon-tags white"></i> Coupon using guide</a></li>
	
	<li><a href="#"><i class="halflings white file"></i> Privacy policy</a></li>
        -->
</ul>
</nav>
<div class="clearfix"></div>

<!-- Content
================================================== -->
<div id="content">


<!-- 960 Container -->
<div class="container floated">

	<div class="sixteen floated page-title">

		<h2><?php echo $_smarty_tpl->tpl_vars['item_type']->value;?>
</h2>

		<nav id="breadcrumbs">
		   <!--
                        <ul>
				<li>You are here:</li>
				<li><a href="">Home</a></li>
				<li>Left Sidebar</li>
			</ul> 
                    -->
		</nav>

	</div>

</div>
<!-- 960 Container / End --><?php }} ?>