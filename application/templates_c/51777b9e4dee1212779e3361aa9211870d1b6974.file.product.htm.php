<?php /* Smarty version Smarty-3.1.13, created on 2014-10-04 18:38:44
         compiled from "application\templates\product.htm" */ ?>
<?php /*%%SmartyHeaderCode:90154280c0e5c6648-65611979%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '51777b9e4dee1212779e3361aa9211870d1b6974' => 
    array (
      0 => 'application\\templates\\product.htm',
      1 => 1412440722,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '90154280c0e5c6648-65611979',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_54280c0e6a5f62_33723020',
  'variables' => 
  array (
    'categories_list' => 0,
    'products_list' => 0,
    'item_title' => 0,
    'product_categories' => 0,
    'option' => 0,
    'categories_pieces' => 0,
    'item_img' => 0,
    'item_text' => 0,
    'product_options' => 0,
    'product_related' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54280c0e6a5f62_33723020')) {function content_54280c0e6a5f62_33723020($_smarty_tpl) {?><div class="container floated">
    <!-- Sidebar -->
	<div class="four floated sidebar left">
		<aside class="sidebar">

			<!-- Search -->
			<nav class="widget-search">
				<form action="" method="get">
					<button class="search-btn-widget"></button>
					<input class="search-field" name="keyword" type="text" onblur="if(this.value=='')this.value='Search';" onfocus="if(this.value=='Search')this.value='';" value="Search" />
				</form>
			</nav>
			<div class="clearfix"></div>

			<!-- Categories -->
			<nav class="widget">
				<h4>Категории</h4>
				<ul class="categories">
					<?php echo $_smarty_tpl->tpl_vars['categories_list']->value;?>

				</ul>
			</nav>

			<!-- Products -->
			<div class="widget">
				<h4>Последни 5 добавени оферти</h4>
				<ul class="categories">
					<?php echo $_smarty_tpl->tpl_vars['products_list']->value;?>

				</ul>
			</div>

			<!-- Tweets-->
			<div class="widget">
				<h4>Twitter</h4>
				<ul id="twitter-blog"></ul>
					<script type="text/javascript">
						jQuery(document).ready(function($){
						$.getJSON('twitter.php?url='+encodeURIComponent('statuses/user_timeline.json?screen_name=Vasterad&count=2'), function(tweets){
							$("#twitter-blog").html(tz_format_twitter(tweets));
						}); });
					</script>
				<div class="clearfix"></div>
			</div>
		</aside>
	</div>
	<!-- Sidebar / End -->
	<!-- Page Content -->
	<div class="eleven floated right">
		<div class="shop-page page-content">
<!--
			<div class="six columns">
				<section class="flexslider shop">
					<ul class="slides">
						<li><a href="images/shop/shop-02a.jpg" rel="fancybox-gallery" title="Digital Camera #1"><img src="images/shop/shop-02a.jpg" alt="" /></a></li>
						<li><a href="images/shop/shop-02b.jpg" rel="fancybox-gallery" title="Digital Camera #2"><img src="images/shop/shop-02b.jpg" alt="" /></a></li>
					</ul>
				</section>
			</div>
-->
			<div>
				<div class="product-info">
					<h3 class="title"><?php echo $_smarty_tpl->tpl_vars['item_title']->value;?>
</h3>
                                        <i class="icon-folder-open"></i> Категории:
                                        
                                        <?php  $_smarty_tpl->tpl_vars['option'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['option']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['product_categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['option']->key => $_smarty_tpl->tpl_vars['option']->value){
$_smarty_tpl->tpl_vars['option']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['option']->key;
?>
                                        <?php $_smarty_tpl->createLocalArrayVariable("categories_pieces", null, 0);
$_smarty_tpl->tpl_vars["categories_pieces"]->value[] = (((((('<a href="/category/').($_smarty_tpl->tpl_vars['option']->value['id'])).('/" title="')).($_smarty_tpl->tpl_vars['option']->value['meta_title'])).('">')).($_smarty_tpl->tpl_vars['option']->value['name'])).('</a>');?>
                                        <?php } ?>
                                        <?php echo implode(', ',$_smarty_tpl->tpl_vars['categories_pieces']->value);?>

                                        
                                        <p>
                                            <img class="image-left" src="<?php if (isset($_smarty_tpl->tpl_vars['item_img']->value)){?><?php echo $_smarty_tpl->tpl_vars['item_img']->value;?>
<?php }else{ ?>../../theme/images/portfolio/portfolio-02.jpg<?php }?>" style="width: 45%;" alt="" hspace="10" vspace="10" align="left" />
                                            <?php echo $_smarty_tpl->tpl_vars['item_text']->value;?>

                                            <center>
                                                <?php  $_smarty_tpl->tpl_vars['option'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['option']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['product_options']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['option']->key => $_smarty_tpl->tpl_vars['option']->value){
$_smarty_tpl->tpl_vars['option']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['option']->key;
?>
                                                <h2 class="option_text_1"><?php echo $_smarty_tpl->tpl_vars['option']->value['title'];?>
 (<?php echo $_smarty_tpl->tpl_vars['option']->value['description'];?>
) с <?php echo $_smarty_tpl->tpl_vars['option']->value['discount'];?>
% намаление</h2>
                                                <span class="option_text_2">Цена: <s style="color:red;"><?php echo $_smarty_tpl->tpl_vars['option']->value['old_price'];?>
</s> <?php echo $_smarty_tpl->tpl_vars['option']->value['price'];?>
</span>
                                                <span class="option_text_3">Код за отстъпка: <?php echo $_smarty_tpl->tpl_vars['option']->value['coupon'];?>
</span>
                                                <div class="clearfix"></div>
						<!-- <a href="<?php echo $_smarty_tpl->tpl_vars['option']->value['buy_url'];?>
" class="button color">Купи</a> -->
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['option']->value['visit_url'];?>
" class="button color">Преглед</a>
                                                <div class="clearfix"></div>
                                                <span class="option_text_2">Спестяваш: <?php echo $_smarty_tpl->tpl_vars['option']->value['old_price']-$_smarty_tpl->tpl_vars['option']->value['price'];?>
 лв.</span>
                                                <br />
                                                <?php } ?>
                                            </center>
                                        </p>
<!--
					<div class="product-amount">
						<input type="text" value="1"/>
						<div class="increase-value"><span class="icon-plus"></span></div>
					</div>
-->					<div class="clearfix"></div>
                                        <div class="line" style="margin-top: 20px; margin-bottom: 30px;"></div>
					<!-- Related Products -->
                                        <div class="sixteen columns">
                                                <h3 style="margin-bottom: 10px;">Свързани продукти</h3>
                                        </div>

                                        <?php  $_smarty_tpl->tpl_vars['option'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['option']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['product_related']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['option']->key => $_smarty_tpl->tpl_vars['option']->value){
$_smarty_tpl->tpl_vars['option']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['option']->key;
?>
                                        <!-- Shop Item -->
                                        <div class="four-shop columns">
                                                <div class="shop-item">
                                                        <figure>
                                                                <a href="/product/<?php echo $_smarty_tpl->tpl_vars['option']->value['product_id'];?>
/<?php echo $_smarty_tpl->tpl_vars['option']->value['url'];?>
" class="badge"><img src="<?php if (isset($_smarty_tpl->tpl_vars['option']->value['image'])){?><?php echo $_smarty_tpl->tpl_vars['option']->value['image'];?>
<?php }else{ ?>../../theme/images/shop/shop-02.jpg<?php }?>" alt="" /></a>
                                                                <figcaption class="item-description">
                                                                        <a href="/product/<?php echo $_smarty_tpl->tpl_vars['option']->value['product_id'];?>
/<?php echo $_smarty_tpl->tpl_vars['option']->value['url'];?>
"><h5><?php echo $_smarty_tpl->tpl_vars['option']->value['title'];?>
</h5></a>
                                                                        <span>$499</span>
                                                                        <!-- <a href="/product/<?php echo $_smarty_tpl->tpl_vars['option']->value['product_id'];?>
/<?php echo $_smarty_tpl->tpl_vars['option']->value['url'];?>
" class="button color">Add to Cart</a> -->
                                                                </figcaption>
                                                        </figure>
                                                </div>
                                        </div>
                                        <?php } ?>
                                        
				</div>
			</div>
			
			<div class="clearfix"></div>
			<br />
			<!--
			<div class="eleven columns">
				
				<ul class="tabs-nav">
					<li class="active"><a href="#tab1">Description</a></li>
					<li><a href="#tab2">Additional Information</a></li>
				</ul>

				
				<div class="tabs-container">
					<div class="tab-content" id="tab1">
						<?php echo $_smarty_tpl->tpl_vars['item_text']->value;?>

					</div>

					<div class="tab-content" id="tab2">
						<table class="standard-table shop">

							<tr>
								<th>Length</th>
								<td>18cm</td>
							</tr>

							<tr>
								<th>Width</th>
								<td>13cm</td>
							</tr>

							<tr>
								<th>Height</th>
								<td>12cm</td>
							</tr>
							
							<tr>
								<th>Color</th>
								<td>Black</td>
							</tr>
							
							<tr>
								<th>Multiselect Attributes</th>
								<td>Camera Lenses, SRL Cameras</td>
							</tr>

						</table>
					</div>

				</div>
			</div>
                        
			<div class="clearfix"></div>
			<div class="line" style="margin-top: 20px; margin-bottom: 30px;"></div>
			
			-->
			<div class="clearfix"></div>
			<div style="margin-top: -10px;"></div>
	
		</div>
	</div>
	<!-- Page Content / End -->
</div>
<!-- 960 Container / End --><?php }} ?>