<?php /* Smarty version Smarty-3.1.13, created on 2014-10-04 19:14:24
         compiled from "application\templates\admin\header.htm" */ ?>
<?php /*%%SmartyHeaderCode:12673535fe3d88208a2-68006257%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8408916166c1d26e17587ce99bcd9b63ddceaf94' => 
    array (
      0 => 'application\\templates\\admin\\header.htm',
      1 => 1412442860,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12673535fe3d88208a2-68006257',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_535fe3d89a4767_35914587',
  'variables' => 
  array (
    'flash_message' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535fe3d89a4767_35914587')) {function content_535fe3d89a4767_35914587($_smarty_tpl) {?><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Control Panel</title>
<meta name="description" content="www.omega-trend.com" />
<meta name="keywords" content="www.omega-trend.com" />
<meta name="Author" content="www.omega-trend.com" />
<meta name="classification" content="Internet" />
<meta name="GOOGLEBOT" content="noindex,nofollow" />
<meta name="robots" content="noindex,nofollow" />

<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

<link type="text/css" rel="stylesheet" href="/css/admin/style.css" />
<link type="text/css" rel="stylesheet" href="/css/admin/rules.css" />
<link type="text/css" rel="stylesheet" href="/js/jquery/css/ui-lightness/jquery-ui-1.8.23.custom.css" />

<link type="text/css" rel="stylesheet" href="/bootstrap/css/bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css" />

<link type="text/css" rel="stylesheet" href="/css/admin/flags.css" />

<script type="text/javascript" language="javascript" src="/js/jquery/jquery-1.8.1.min.js"></script>
<script type="text/javascript" language="javascript" src="/js/jquery/jquery-ui-1.8.23.custom.min.js"></script>
<script type="text/javascript" language="javascript" src="/js/flashmessage.js"></script>
<script type="text/javascript" language="javascript" src="/js/admin/functions.js"></script>

<script type="text/javascript" language="javascript" src="/bootstrap/js/bootstrap.js"></script>

<!--[if lt IE 9]>
<script type="text/javascript" >
	document.createElement("nav" );
	document.createElement("header" );
	document.createElement("footer" );
	document.createElement("section" );
	document.createElement("aside" );
	document.createElement("article" );
</script>
<![endif]-->

<script type="text/javascript" language="javascript">

	function chnageScreenSize(btn)
	{
	
		if ($(btn).hasClass("active"))
		{
			//$(btn).removeClass("active")
			$("div#wrapper").removeClass("full-screen");
			$.post("/admin/ajax/full_screen/off");
		}
		else
		{
			//$(btn).addClass("active")
			$("div#wrapper").addClass("full-screen");
			$.post("/admin/ajax/full_screen/on");
		}
	}
		
	function setFullScreenOn()
	{
		$("div#wrapper").addClass("full-screen");
		$("button#full_screen_switch").addClass("active");
	}
	
	$(document).ready(function() {

		<?php if (isset($_COOKIE['full_screen'])&&$_COOKIE['full_screen']==1){?>
			setFullScreenOn();
		<?php }?>

	});
	
	
</script>

<?php echo $_smarty_tpl->tpl_vars['flash_message']->value;?>


</head><?php }} ?>