<?php /* Smarty version Smarty-3.1.13, created on 2014-10-04 18:31:24
         compiled from "application\templates\page_footer.htm" */ ?>
<?php /*%%SmartyHeaderCode:21294535fe3b1233c89-10355114%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2b44001a22a3f080000447da427f4f4832acb506' => 
    array (
      0 => 'application\\templates\\page_footer.htm',
      1 => 1412440283,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21294535fe3b1233c89-10355114',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_535fe3b1271b17_76418766',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535fe3b1271b17_76418766')) {function content_535fe3b1271b17_76418766($_smarty_tpl) {?>    </div>
    <!-- Content / End -->
</div>
<!-- Wrapper / End -->

<!-- Footer / Start -->
<footer id="footer">
	<!-- 960 Container -->
	<div class="container">

		<!-- About -->
		<div class="four columns">
			<h4>Helpful Bulgaria</h4>
			<p>Най-полезното в България, на най-ниски цени!</p>
		</div>

		<!-- Contact Details -->
		<div class="four columns">
			<h4>Contact Details</h4>
			<ul class="contact-details-alt">
				<li><i class="halflings white map-marker"></i> <p><strong>Адрес:</strong> Пловдив 4000, <br>бул. Съединение №44</p></li>
				<li><i class="halflings white user"></i> <p><strong>Телефон:</strong> +359 32 123 456</p></li>
				<li><i class="halflings white envelope"></i> <p><strong>Е-Mail:</strong> <a href="#">contact@helpfulbg.com</a></p></li>
			</ul>
		</div>

		<!-- Photo Stream -->
		<div class="four columns">
			<h4>Снимки от България</h4>
			<div class="flickr-widget">
				<script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=6&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=72179079@N00"></script>
				<div class="clearfix"></div>
			</div>
		</div>

		<!-- Twitter -->
		<div class="four columns">
			<h4>Twitter</h4>
			<ul id="twitter"></ul>
				<script type="text/javascript">
					jQuery(document).ready(function($){
					$.getJSON('twitter.php?url='+encodeURIComponent('statuses/user_timeline.json?screen_name=Vasterad&count=1'), function(tweets){
						$("#twitter").html(tz_format_twitter(tweets));
					}); });
				</script>
			<div class="clearfix"></div>
		</div>

	</div>
	<!-- 960 Container / End -->

</footer>
<!-- Footer / End -->


<!-- Footer Bottom / Start  -->
<footer id="footer-bottom">

	<!-- 960 Container -->
	<div class="container">

		<!-- Copyrights -->
		<div class="eight columns">
			<div class="copyright">
				© Copyright 2013 by <a href="#">Nevia</a>. All Rights Reserved.
			</div>
		</div>

		<!-- Menu -->
		<div class="eight columns">
			<nav id="sub-menu">
				<ul>
					<li><a href="#">Начало</a></li>
					<li><a href="#">За нас</a></li>
					<li><a href="#">Контакти</a></li>
				</ul>
			</nav>
		</div>

	</div>
	<!-- 960 Container / End -->

</footer>
<!-- Footer Bottom / End --><?php }} ?>