<?php /* Smarty version Smarty-3.1.13, created on 2014-10-04 19:06:51
         compiled from "application\templates\admin\email_statistics.htm" */ ?>
<?php /*%%SmartyHeaderCode:742854302870b87a47-76110366%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1bd47608a053d8885f162281708d5b122a9f8c07' => 
    array (
      0 => 'application\\templates\\admin\\email_statistics.htm',
      1 => 1412442408,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '742854302870b87a47-76110366',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_54302870be8a98_27708021',
  'variables' => 
  array (
    'action' => 0,
    'alert_message' => 0,
    'events' => 0,
    'event' => 0,
    'email_statistics' => 0,
    'index' => 0,
    'email_stat' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54302870be8a98_27708021')) {function content_54302870be8a98_27708021($_smarty_tpl) {?><ul class="nav nav-tabs">
	<li <?php if ($_smarty_tpl->tpl_vars['action']->value=="view_sent"||$_smarty_tpl->tpl_vars['action']->value==''){?>class="active"<?php }?>><a href="/admin/emails/view_sent">Виж изпратените е-мейли</a></li>
	<li <?php if ($_smarty_tpl->tpl_vars['action']->value=="send"){?>class="active"<?php }?>><a href="/admin/emails/send">изпрати е-мейл</a></li>
	<li <?php if ($_smarty_tpl->tpl_vars['action']->value=="email_statistics"){?>class="active"<?php }?>><a href="/admin/email_statistics">Статистика</a></li>
</ul>

<style type="text/css">

table td
{
	vertical-align: middle !important;
}

</style>

<div class="clearfix">
	<h1 class="pull-left">Е-мейл статистика</h1>		
</div>

<?php if (isset($_smarty_tpl->tpl_vars['alert_message']->value)&&$_smarty_tpl->tpl_vars['alert_message']->value){?><?php echo $_smarty_tpl->tpl_vars['alert_message']->value;?>
<?php }?>

<table class="table table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Относно</th>
			<th style="text-align: center;">Дата на изпращане</th>
			<?php  $_smarty_tpl->tpl_vars['event'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['event']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['events']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['event']->key => $_smarty_tpl->tpl_vars['event']->value){
$_smarty_tpl->tpl_vars['event']->_loop = true;
?>
				<th style="text-align: center;"><?php echo $_smarty_tpl->tpl_vars['event']->value['name'];?>
</th>
			<?php } ?>
		</tr>
	</thead>

	<tbody>
	
	<?php if (isset($_smarty_tpl->tpl_vars['email_statistics']->value)&&$_smarty_tpl->tpl_vars['email_statistics']->value){?>
		<?php  $_smarty_tpl->tpl_vars['email_stat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['email_stat']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['email_statistics']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['email_stat']->key => $_smarty_tpl->tpl_vars['email_stat']->value){
$_smarty_tpl->tpl_vars['email_stat']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['email_stat']->key;
?>
		<tr>
			<td><?php echo $_smarty_tpl->tpl_vars['index']->value+1;?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['email_stat']->value['subject'];?>
</td>
			<td style="text-align: center;"><?php echo $_smarty_tpl->tpl_vars['email_stat']->value['email_published'];?>
</td>
			<?php  $_smarty_tpl->tpl_vars['event'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['event']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['events']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['event']->key => $_smarty_tpl->tpl_vars['event']->value){
$_smarty_tpl->tpl_vars['event']->_loop = true;
?>
				<td style="text-align: center;"><?php echo $_smarty_tpl->tpl_vars['email_stat']->value['events'][$_smarty_tpl->tpl_vars['event']->value['id']]["cnt"];?>
</td>
			<?php } ?>
		</tr>
		<?php } ?>
	<?php }else{ ?>
		<tr>
			<td colspan="9">Няма резултат!</td>
		</tr>
	<?php }?>
		
	</tbody>
</table>
	

<?php }} ?>