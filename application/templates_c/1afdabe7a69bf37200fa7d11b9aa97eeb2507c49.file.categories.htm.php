<?php /* Smarty version Smarty-3.1.13, created on 2014-10-04 18:58:39
         compiled from "application\templates\admin\categories.htm" */ ?>
<?php /*%%SmartyHeaderCode:3018854302572542b56-48598591%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1afdabe7a69bf37200fa7d11b9aa97eeb2507c49' => 
    array (
      0 => 'application\\templates\\admin\\categories.htm',
      1 => 1412441918,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3018854302572542b56-48598591',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_543025726887f4_85347556',
  'variables' => 
  array (
    'action' => 0,
    'keyword' => 0,
    'categories' => 0,
    'num_categories' => 0,
    'category' => 0,
    'categories_cnt' => 0,
    'offset' => 0,
    'navigator' => 0,
    'alert_message' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_543025726887f4_85347556')) {function content_543025726887f4_85347556($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include 'D:\\xampp\\htdocs\\bestdiscountshere_com\\application\\libraries\\smarty\\plugins\\modifier.date_format.php';
?><?php if (isset($_smarty_tpl->tpl_vars['action']->value)&&($_smarty_tpl->tpl_vars['action']->value=="show_all"||$_smarty_tpl->tpl_vars['action']->value=="inactive_customers"||$_smarty_tpl->tpl_vars['action']->value=="administrators")){?>

    <ul class="nav nav-tabs">
		<li <?php if ($_smarty_tpl->tpl_vars['action']->value=="show_all"){?>class="active"<?php }?>><a href="/admin/categories/show_all">Преглед на активните</a></li>
		<li><a href="/admin/categories/add">Добави нова категория</a></li>
    </ul>
	
	<?php if ($_smarty_tpl->tpl_vars['action']->value=="show_all"){?>
		<h1 class="pull-left">Преглед на активните</h1>
	<?php }?>
	
	<div class="input-append pull-right" style="padding-top: 15px;">
		<form action="/admin/categories/<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
" method="get" style="margin: 0;">
			<input class="input-medium" id="appendedInputButtons" size="16" type="text" name="keyword" value="<?php if (isset($_smarty_tpl->tpl_vars['keyword']->value)&&$_smarty_tpl->tpl_vars['keyword']->value){?><?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>"><button class="btn" type="submit">Търсене</button>
		</form>
	</div>

	<table class="table table-striped">
		<thead>
			<tr>
			<th class="w25px center">#</th>
			<th class="center">Име</th>
			<th class="w140px center">Добавена на</th>
                        <th class="w140px center">Модифицирана на </th>
			<th class="w160px center">&nbsp;</th>
			</tr>
		</thead>
	
		<tbody>
		
		<?php if (isset($_smarty_tpl->tpl_vars['categories']->value)&&$_smarty_tpl->tpl_vars['categories']->value){?>
			<?php $_smarty_tpl->tpl_vars["categories_cnt"] = new Smarty_variable($_smarty_tpl->tpl_vars['num_categories']->value, null, 0);?>
			<?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value){
$_smarty_tpl->tpl_vars['category']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['category']->key;
?>
				<tr id="row_category_<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['category']->value['status']==0){?>class="error"<?php }elseif(date('Y-m-d')==smarty_modifier_date_format($_smarty_tpl->tpl_vars['category']->value['published'],"%Y-%m-%d")){?>class="success"<?php }?>>
                                
					<td class="center"><?php echo $_smarty_tpl->tpl_vars['categories_cnt']->value-$_smarty_tpl->tpl_vars['offset']->value;?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['category']->value['name'];?>
</td>
					<td class="center"><?php echo $_smarty_tpl->tpl_vars['category']->value['published'];?>
</td>
                                        <td class="center"><?php if ($_smarty_tpl->tpl_vars['category']->value['modified']==''){?> - <?php }else{ ?> <?php echo $_smarty_tpl->tpl_vars['category']->value['modified'];?>
 <?php }?></td>
					<td class="right" style="width:200px;">
						<button class="btn btn-small btn-info" type="button"onclick="window.location = '/admin/categories/edit/<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
';"><i class="icon-edit icon-white"></i> Промени</button>&nbsp;&nbsp;
						<button class="btn btn-small btn-danger" type="button" onclick="delete_category('<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
');"><i class="icon-remove icon-white"></i> Изтрий</button>
                                        </td>
				</tr>
				<?php $_smarty_tpl->tpl_vars["categories_cnt"] = new Smarty_variable($_smarty_tpl->tpl_vars['categories_cnt']->value-1, null, 0);?>
			<?php } ?>
		<?php }else{ ?>
			<tr>
				<td colspan="5">Няма резултат!</td>
			</tr>
		<?php }?>
			
		</tbody>
	</table>
	
	
	
	<script type="text/javascript" language="javascript">
	
		$(document).ready(function() {

			$('i.user-comment').tooltip({placement: "right"});
		});
		
	</script>
	

	<?php echo $_smarty_tpl->tpl_vars['navigator']->value;?>


	

<?php }elseif(isset($_smarty_tpl->tpl_vars['action']->value)&&$_smarty_tpl->tpl_vars['action']->value=="edit"){?>

	<ul class="nav nav-tabs">
		<li><a href="/admin/categories/show_all">Преглед на всички активни</a></li>
		<li><a href="/admin/categories/add">Добави нова категория</a></li>
    </ul>
		
	<form action="/admin/categories/edit/<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
" method="post" style="margin: 0;">
		<input type="hidden" name="save" value="1" />		
		<legend>Category <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</legend>
		
		<?php if (isset($_smarty_tpl->tpl_vars['alert_message']->value)&&$_smarty_tpl->tpl_vars['alert_message']->value){?><?php echo $_smarty_tpl->tpl_vars['alert_message']->value;?>
<?php }?>
		
		<div class="clearfix">
			<div class="pull-left" style="">
				<label>Име:</label>
				<input type="text" name="name" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<!--
                                <label>Status:</label>
                                
                                <label class="radio inline">
					<input type="radio" name="status" value="1" <?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php if ($_smarty_tpl->tpl_vars['data']->value['status']==1){?>checked="checked"<?php }?><?php }elseif(!isset($_smarty_tpl->tpl_vars['data']->value)){?>checked="checked"<?php }?> /> Active
				</label>
				
                                <label class="radio inline">
					<input type="radio" name="status" value="0" <?php if (isset($_smarty_tpl->tpl_vars['data']->value)&&$_smarty_tpl->tpl_vars['data']->value['status']==0){?>checked="checked"<?php }?> /> Suspend
				</label> -->
                               
                                <label class="clearfix">
					Статус: 
					<span class="pull-right">
						<?php if ($_smarty_tpl->tpl_vars['category']->value['status']==1){?><span class="label label-success" id="category_status">Active</span><?php }else{ ?><span class="label label-important" id="category_status">Inactive</span><?php }?> 
						<button type="button" class="btn btn-small btn-info" onclick="changeCategoryStatus('<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
');"><i class="icon-refresh icon-white"></i> Change</button>
					</span>
				</label>
                                
                                <br /><br />
				<label class="clearfix">Създадена: <span class="label label-info pull-right"><?php echo $_smarty_tpl->tpl_vars['category']->value['published'];?>
</span></label>
				<label class="clearfix">Модифицирана: <span class="label pull-right"><?php if ($_smarty_tpl->tpl_vars['category']->value['modified']){?><?php echo $_smarty_tpl->tpl_vars['category']->value['modified'];?>
<?php }else{ ?>no info<?php }?></span></label>
				
                                
			</div>
			<div class="pull-left" style="margin-left: 30px; width: 370px;">
                                <label>Meta title:</label>
				<input type="text" name="meta_title" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['meta_title'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['meta_title'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Meta description:</label>
				<input type="text" name="meta_description" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['meta_description'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['meta_description'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Meta keywords:</label>
				<input type="text" name="meta_keywords" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['meta_keywords'], ENT_QUOTES, 'UTF-8', true);?>
<?php }else{ ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['meta_keywords'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
			</div>
                        <div style="text-align: center; padding-top: 78px;"><button type="submit" class="btn btn-info btn-large" style="width: 200px;">Запази категорията</button></div>
		</div>
	</form>

<?php }elseif(isset($_smarty_tpl->tpl_vars['action']->value)&&$_smarty_tpl->tpl_vars['action']->value=="add"){?>

	<ul class="nav nav-tabs">
		<li><a href="/admin/categories/show_all">Преглед на активните</a></li>
		<li class="active"><a href="/admin/categories/add">Добави нова категория</a></li>
    </ul>
		
	<form action="/admin/categories/add" method="post" style="margin: 0;">
		<input type="hidden" name="add" value="1" />		
		<legend>Добавяне на нова категория</legend>
                
		<?php if (isset($_smarty_tpl->tpl_vars['alert_message']->value)&&$_smarty_tpl->tpl_vars['alert_message']->value){?><?php echo $_smarty_tpl->tpl_vars['alert_message']->value;?>
<?php }?>
		
		<div class="clearfix">
			<div class="pull-left" style="">
				<label>Име:</label>
				<input type="text" name="name" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				
                                <label>Статус:</label>
                                
                                <label class="radio inline">
					<input type="radio" name="status" value="1" <?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php if ($_smarty_tpl->tpl_vars['data']->value['status']==1){?>checked="checked"<?php }?><?php }elseif(!isset($_smarty_tpl->tpl_vars['data']->value)){?>checked="checked"<?php }?> /> Active
				</label>
				
                                <label class="radio inline">
					<input type="radio" name="status" value="0" <?php if (isset($_smarty_tpl->tpl_vars['data']->value)&&$_smarty_tpl->tpl_vars['data']->value['status']==0){?>checked="checked"<?php }?> /> Suspend
				</label>
								
				<div style="text-align: center; padding-top: 57px;"><button type="submit" class="btn btn-info btn-large" style="width: 200px;">Добави</button></div>
                                
			</div>

			<div class="pull-left" style="margin-left: 30px; width: 370px;">
				<label>Meta Title:</label>
				<input type="text" name="meta_title" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['meta_title'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Meta Description:</label>
				<input type="text" name="meta_description" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['meta_description'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
				<label>Meta Keywords:</label>
				<input type="text" name="meta_keywords" style="width: 350px;" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value)){?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['meta_keywords'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
			</div>
		</div>
	</form>

<?php }?>

                    <?php }} ?>