<?php /* Smarty version Smarty-3.1.13, created on 2014-09-28 17:09:54
         compiled from "application\templates\admin\pagelayout.htm" */ ?>
<?php /*%%SmartyHeaderCode:2826542824c27fbec9-85444521%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8eb96de743ecfc9a9c587b74a3aca15a1566136a' => 
    array (
      0 => 'application\\templates\\admin\\pagelayout.htm',
      1 => 1370280912,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2826542824c27fbec9-85444521',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_542824c2971d91_62011982',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_542824c2971d91_62011982')) {function content_542824c2971d91_62011982($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">

	<?php echo $_smarty_tpl->getSubTemplate ("admin/header.htm", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<body>
	
	<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("admin/page_header.htm", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		
	<section id="page-content">
		<?php echo $_smarty_tpl->getSubTemplate ("admin/content.htm", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	</section>
	
	<br />
	<br />
	<br />
	
	<?php echo $_smarty_tpl->getSubTemplate ("admin/page_footer.htm", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	</div>
</body>
</html><?php }} ?>