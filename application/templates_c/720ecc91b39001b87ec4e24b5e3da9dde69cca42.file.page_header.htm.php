<?php /* Smarty version Smarty-3.1.13, created on 2014-10-04 18:46:23
         compiled from "application\templates\admin\page_header.htm" */ ?>
<?php /*%%SmartyHeaderCode:28539542824c2a406a0-64104600%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '720ecc91b39001b87ec4e24b5e3da9dde69cca42' => 
    array (
      0 => 'application\\templates\\admin\\page_header.htm',
      1 => 1412441181,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '28539542824c2a406a0-64104600',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_542824c2ab8a44_21631683',
  'variables' => 
  array (
    'controller' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_542824c2ab8a44_21631683')) {function content_542824c2ab8a44_21631683($_smarty_tpl) {?><header>
	<div class="content">
		<div class="clearfix">
		<div class="logo pull-left" style="padding: 20px 0 0 0; height: 60px;">
			<a href="/admin"><span style="font-size: 36px;">Helpful Bulgaria</span></a>
		</div>
		<div class="screen-buttons pull-right">
			<button class="btn btn-small" type="button" data-toggle="button" id="full_screen_switch" onclick="chnageScreenSize(this);"><i class="icon-fullscreen"></i> На цял екран</button>
		</div>
		</div>
		<div class="main-menu">	
			<ul class="nav nav-pills">
				
				<li class="<?php if ($_smarty_tpl->tpl_vars['controller']->value==''||$_smarty_tpl->tpl_vars['controller']->value=='home'){?>active<?php }?>"><a href="/admin"><i class="icon-home"></i> Начало</a></li>
				
				<li class="<?php if ($_smarty_tpl->tpl_vars['controller']->value=='users'){?>active <?php }?>dropdown">
					<a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="icon-user"></i> Потребители <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="/admin/users/add"><i class="icon-plus"></i> Добави нов потребител</a></li>
						<li class="divider"></li>
						<li><a href="/admin/users/customers"><i class="icon-ok"></i> Клиенти</a></li>
						<li><a href="/admin/users/inactive_customers"><i class="icon-remove"></i> Неактивни клиенти</a></li>
						<li><a href="/admin/users/administrators"><i class="icon-user"></i> Администратори</a></li>
					</ul>
				</li>
				
                                <li class="<?php if ($_smarty_tpl->tpl_vars['controller']->value=='categories'){?>active <?php }?>dropdown">
					<a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="icon-folder-open"></i> Категории <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="/admin/categories/add"><i class="icon-plus"></i> Добавяне на нова категория</a></li>
						<li class="divider"></li>
						<li><a href="/admin/categories/show_all"><i class="icon-ok"></i> Преглед на категории</a></li>
						<!--
                                                <li><a href="/admin/users/inactive_customers"><i class="icon-remove"></i> Inactive Customers</a></li>
						<li><a href="/admin/users/administrators"><i class="icon-user"></i> Administrators</a></li> -->
					</ul>
				</li>
                                
				<li class="<?php if ($_smarty_tpl->tpl_vars['controller']->value=='affiliates'){?>active <?php }?>dropdown">
					<a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="icon-briefcase"></i> Продукти <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="/admin/products/add"><i class="icon-plus"></i> Добавяне на нов продукт</a></li>
						<li class="divider"></li>
						<li><a href="/admin/products/show_all"><i class="icon-list"></i> Всички продукти</a></li>
					</ul>					
				</li>
				
				<li class="<?php if ($_smarty_tpl->tpl_vars['controller']->value=='emails'){?>active <?php }?>dropdown">
					<a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="icon-envelope"></i> E-мейли <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="/admin/emails/send"><i class="icon-edit"></i> Изпрати е-маил</a></li>
						<li><a href="/admin/emails/view_sent"><i class="icon-check"></i> Преглед на е-мейли</a></li>
						<li class="divider"></li>
						<li><a href="/admin/email_statistics"><i class="icon-list"></i> Преглед на статистика за мейлите</a></li>
					</ul>
				</li>
				
				
				
				<li class="<?php if ($_smarty_tpl->tpl_vars['controller']->value=='statistics'){?>active<?php }?>"><a href="/admin/statistics"><i class="icon-signal"></i> Статистика</a></li>
				
				<li class="<?php if ($_smarty_tpl->tpl_vars['controller']->value=='support'){?>active<?php }?>"><a href="/admin/support"><i class="icon-question-sign"></i> Поддръжка</a></li>
				
									
				
				<li><a href="/admin/users/logout"><i class="icon-off"></i> Изход</a></li>
				
			</ul>
		</div>
	</div>
</header>
<?php }} ?>