<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Unsubscribe extends CI_Controller {

	public function index()
	{
		$_SESSION["page_description"] = "Unsubscribe. Best Discounts Here";
		$_SESSION["page_keywords"] = "";
		$_SESSION["page_title"] = "Unsubscribe. Best Discounts Here";	
		
		$unsubscribe = "";
		$send_error = false;
		$action = "";
		
		if ($this->input->get("email"))
		{
			$this->load->model("affiliates_model");
			$this->load->model("users_model");
			
			$email = $this->input->get("email");
			$type  = $this->input->get("type");
			$hash  = $this->input->get("hash");
				
			if ($email !== false && $type !== false && $hash !== false)
			{
				$original_hash = "";
				switch( $type )
				{
					case "a" :
					{
						$original_hash = md5( $email . "affiliate" );
						if( $hash == $original_hash )
						{
							if ($this->affiliates_model->exist($email, "email"))
							{
								$affiliate = $this->affiliates_model->get_affiliate_info_by_email( $email );
								$this->affiliates_model->update_affiliate( $affiliate['id'], array( "newsletter" => 0 ) );
								$action = "success";
							}
							else 
								$action = "email_does_not_exist";
						}
						else 
							$action = "incorrect_data";
					} break;
					case "u" :
					{
						$original_hash = md5( $email . "user" );
						if( $hash == $original_hash )
						{
							if ($this->users_model->user_exist($email, "email"))
							{
								$user = $this->users_model->get_user_info_by_email( $email );
								$this->users_model->update($user['id'], array( "newsletter" => 0 ) );
								$action = "success";
							}
							else 
								$action = "email_does_not_exist";
						}
						else 
							$action = "incorrect_data";
					} break;
					default:
					{
						$action = "incorrect_data";
					}
				}
			}
			else 
			{
				$action = "missing_data";
			}
		}
		else
			$action = "missing_data";
		
	//	$action = "success";
		$this->smarty_parser->assign( "action", $action );
		
		$output = $this->smarty_parser->fetch("unsubscribe.htm");
		$this->output->set_output($output);
	}
}