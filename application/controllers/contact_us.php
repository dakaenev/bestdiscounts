<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_us extends CI_Controller {

	public function index()
	{
		$_SESSION["page_description"] = "Contact us. Best Discounts Here";
		$_SESSION["page_keywords"] = "Best Discounts Here";
		$_SESSION["page_title"] = "Contact us. Best Discounts Here";
		
		if ($this->input->post("send"))
		{
			$this->load->library("form_validation");
			$this->form_validation->set_error_delimiters("#", "");
			
			if ($this->form_validation->run("contact_us"))
			{
				$name 		= $this->input->post("name");
				$email 		= $this->input->post("email");
				$subject 	= $this->input->post("subject");
				$message 	= $this->input->post("message");
				
				$mail_body = new Smarty_parser();
				$mail_body->assign( "name", $name );
				$mail_body->assign( "email", $email );
				$mail_body->assign( "subject", $subject );
				$mail_body->assign( "message", $message );
				$text_body = $mail_body->fetch( "emails/contacts.htm" );
				
				$data_email = array();
				$data_email["from_name"] = $name;
				$data_email["from_email"] = $email;
				$data_email["subject"] = "Contacts - " . $subject;
				$data_email["mail_content"] = $text_body;
				$data_email["priority"] = 1;
				$data_email["published"] = now();
				$data_email["recipients"] = array();
				$data_email["recipients"][] = array( "recipient_name" => SUPPORT_EMAIL_FROM_NAME, "recipient_email" => SUPPORT_EMAIL, "type" => "1");
				
				$this->load->model("emails_model");
				if ($this->emails_model->add($data_email))
				{
					set_flash_message( "Information Box", "Your message has been sent successfully! Our team will contact you as soon as possible.", array( "show_title" => true, "modal" => true, "width" => 400, "auto_hide" => 0, "show_button" => true ) );
					redirect("/contact_us");
				}
				else
				{					
					set_flash_message( "Information Box", "Wrong login details! Please try again.", array( "show_title" => true, "modal" => true, "width" => 400, "auto_hide" => 0, "show_button" => true ) );
				}				
			}
			else
			{
				$errors = explode( "#", validation_errors() );
				array_shift( $errors );
	
				foreach ($this->input->post() as $key => $value)
				{
					$this->smarty_parser->assign($key, $value);
				}
	
				set_flash_message( "Information Box", "An error has occured! Please fill the form correctly.", array( "show_title" => true, "modal" => true, "width" => 400, "auto_hide" => 0, "show_button" => true ), $errors );
			}			
		}
		
		$output = $this->smarty_parser->fetch("contact_us.htm");
		$this->output->set_output($output);
	}
}