<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scripts extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->administration = true;
		
		check_rights(USER_ADMIN);
		
		$action = $this->uri->segment(3);
		$this->smarty_parser->assign("action", $action);
	}
	
	public function index()
	{
		
	}
	
	public function backtest2real()
	{
		if ($this->input->post("generate"))
		{
			$this->load->library("form_validation");
			$this->form_validation->set_error_delimiters("#", "");

			if ($this->form_validation->run("generate_statement"))
			{
				$this->generate_statement_script();
			}
			else
			{
				$errors = explode("#", validation_errors());
				array_shift( $errors );

				set_alert_message("Wrong data! Please check the list below.", "", $errors, "error");
				
				foreach ($this->input->post() as $key => $value)
					$this->smarty_parser->assign($key, $value);				
			}
		}
		
		if (has_alert_message())
		{
			$this->smarty_parser->assign('alert_message', get_alert_message());
		}
		
		$output = $this->smarty_parser->fetch("admin/scripts.htm");
		$this->output->set_output($output);
	}
	
	private function generate_statement_script()
	{
		$path = getcwd() . DIRECTORY_SEPARATOR;
		if (isset($_FILES['statement']) && $_FILES['statement']['size'] > 0)
		{
			$path_original_file = $path . 'files/StrategyTester.htm';
			move_uploaded_file($_FILES['statement']['tmp_name'], $path_original_file);
			
			$account_number = $this->input->post("acc_num");
			$account_name = $this->input->post("acc_name");
			$date_generated = date("Y F j, H:i");
			$pair = strtolower($this->input->post("symbol"));
			
			$original_file_html = file_get_contents($path_original_file);
			if (strpos($_SERVER['HTTP_HOST'], 'localhost') === false)
				$original_file_html = str_replace("\r\n", "\n", $original_file_html);
			
			preg_match('/<tr align=left><td>Initial deposit<\/td><td align=right>([0-9\-]+\.[0-9]{2})<\/td>/', $original_file_html, $matches);
			$deposit_num = $matches[1];
			$deposit = number_format($matches[1], 2, ".", " ");
			
			preg_match('/<tr align=left><td>Total net profit<\/td><td align=right>([0-9\-]+\.[0-9]{2})<\/td>/', $original_file_html, $matches);
			$closed_trade_pl_num = $matches[1];
			$closed_trade_pl = number_format($matches[1], 2, ".", " ");
			
			$balance = number_format($deposit_num + $closed_trade_pl_num, 2, ".", " ");
			$equity = $free_margin = $balance;
			/*
			preg_match('/<td>Gross profit<\/td><td align=right>([0-9\-]+\.[0-9]{2})<\/td>/', $original_file_html, $matches);
			$gross_profit = number_format($matches[1], 2, ".", " ");
			
			preg_match('/<td>Gross loss<\/td><td align=right>([0-9\-]+\.[0-9]{2})<\/td>/', $original_file_html, $matches);
			$gross_loss = number_format($matches[1], 2, ".", " ");
			
			$total_net_profit = $closed_trade_pl;
			
			preg_match('/<tr align=left><td>Profit factor<\/td><td align=right>([0-9]+\.[0-9]{2})<\/td>/', $original_file_html, $matches);
			$profit_factor = number_format($matches[1], 2, ".", " ");
		
			preg_match('/<td>Expected payoff<\/td><td align=right>([0-9\-]+\.[0-9]{2})<\/td>/', $original_file_html, $matches);
			$expected_payoff = number_format($matches[1], 2, ".", " ");
			
			preg_match('/<tr align=left><td>Absolute drawdown<\/td><td align=right>([0-9\-]+\.[0-9]{2})<\/td>/', $original_file_html, $matches);
			$absolute_drawdown = number_format($matches[1], 2, ".", " ");
		
			preg_match('/<td>Maximal drawdown<\/td><td align=right>([0-9\-]+\.[0-9]{2}\s\([0-9]+\.[0-9]{2}%\))<\/td>/', $original_file_html, $matches);
			$maximal_drawdown_str = $matches[1];
			$maximal_drawdown_sum = reset(explode(" ", $maximal_drawdown_str));
			$maximal_drawdown_sum = number_format($maximal_drawdown_sum, 2, ".", " ");
			$maximal_drawdown = $maximal_drawdown_sum . " " . end(explode(" ", $maximal_drawdown_str));
		
			preg_match('/<td>Relative drawdown<\/td><td align=right>([0-9\-]+\.[0-9]{2}%\s\([0-9]+\.[0-9]{2}\))<\/td>/', $original_file_html, $matches);
			$relative_drawdown_str = $matches[1];
			$relative_drawdown_sum = substr(end(explode(" ", $relative_drawdown_str)), 1, -1);
			$relative_drawdown_sum = number_format($relative_drawdown_sum, 2, ".", " ");
			$relative_drawdown = reset(explode(" ", $relative_drawdown_str)) . " (" . $relative_drawdown_sum . ")";
			
			preg_match('/<tr align=left><td>Total trades<\/td><td align=right>([0-9]+)<\/td>/', $original_file_html, $matches);
			$total_trades = number_format($matches[1], 0, ".", " ");
			
			preg_match('/<td>Short positions \(won %\)<\/td><td align=right>([0-9\-]+\s\([0-9]+\.[0-9]{2}%\))<\/td>/', $original_file_html, $matches);
			$short_positions_won_str = $matches[1];
			$short_positions_won_sum = reset(explode(" ", $short_positions_won_str));
			$short_positions_won_sum = number_format($short_positions_won_sum, 0, ".", " ");
			$short_positions_won = $short_positions_won_sum . " " . end(explode(" ", $short_positions_won_str));
		
			preg_match('/<td>Long positions \(won %\)<\/td><td align=right>([0-9\-]+\s\([0-9]+\.[0-9]{2}%\))<\/td>/', $original_file_html, $matches);
			$long_positions_won_str = $matches[1];
			$long_positions_won_sum = reset(explode(" ", $long_positions_won_str));
			$long_positions_won_sum = number_format($long_positions_won_sum, 0, ".", " ");
			$long_positions_won = $long_positions_won_sum . " " . end(explode(" ", $long_positions_won_str));
		
			preg_match('/<td>Profit trades \(% of total\)<\/td><td align=right>([0-9\-]+\s\([0-9]+\.[0-9]{2}%\))<\/td>/', $original_file_html, $matches);
			$profit_trades_str = $matches[1];
			$profit_trades_sum = reset(explode(" ", $profit_trades_str));
			$profit_trades_sum = number_format($profit_trades_sum, 0, ".", " ");
			$profit_trades = $profit_trades_sum . " " . end(explode(" ", $profit_trades_str));
		
			preg_match('/<td>Loss trades \(% of total\)<\/td><td align=right>([0-9\-]+\s\([0-9]+\.[0-9]{2}%\))<\/td>/', $original_file_html, $matches);
			$loss_trades_str = $matches[1];
			$loss_trades_sum = reset(explode(" ", $loss_trades_str));
			$loss_trades_sum = number_format($loss_trades_sum, 0, ".", " ");
			$loss_trades = $loss_trades_sum . " " . end(explode(" ", $loss_trades_str));
			
			preg_match('/<tr align=left><td colspan=2 align=right>Largest<\/td><td>profit trade<\/td><td align=right>([0-9\-]+\.[0-9]{2})<\/td>/', $original_file_html, $matches);
			$largest_profit_trade = number_format($matches[1], 2, ".", " ");
		
			preg_match('/<tr align=left><td colspan=2 align=right>Largest<\/td><td>profit trade<\/td><td align=right>[0-9\-]+\.[0-9]{2}<\/td><td>loss trade<\/td><td align=right>([0-9\-]+\.[0-9]{2})<\/td>/', $original_file_html, $matches);
			$largest_loss_trade = number_format($matches[1], 2, ".", " ");
		
			preg_match('/<tr align=left><td colspan=2 align=right>Average<\/td><td>profit trade<\/td><td align=right>([0-9\-]+\.[0-9]{2})<\/td>/', $original_file_html, $matches);
			$average_profit_trade = number_format($matches[1], 2, ".", " ");
		
			preg_match('/<tr align=left><td colspan=2 align=right>Average<\/td><td>profit trade<\/td><td align=right>[0-9\-]+\.[0-9]{2}<\/td><td>loss trade<\/td><td align=right>([0-9\-]+\.[0-9]{2})<\/td>/', $original_file_html, $matches);
			$average_loss_trade = number_format($matches[1], 2, ".", " ");
		
			preg_match('/<tr align=left><td colspan=2 align=right>Maximum<\/td><td>consecutive wins \(profit in money\)<\/td><td align=right>([0-9]+\s\([0-9]+\.[0-9]{2}\))<\/td>/', $original_file_html, $matches);
			$max_consecutive_wins_str = $matches[1];
			$max_consecutive_wins_sum = substr(end(explode(" ", $max_consecutive_wins_str)), 1, -1);
			$max_consecutive_wins_sum = number_format($max_consecutive_wins_sum, 2, ".", " ");
			$max_consecutive_wins = number_format(reset(explode(" ", $max_consecutive_wins_str)), 0, ".", " ") . " (" . $max_consecutive_wins_sum . ")";
		
			preg_match('/<tr align=left><td colspan=2 align=right>Maximum<\/td><td>consecutive wins \(profit in money\)<\/td><td align=right>[0-9]+\s\([0-9]+\.[0-9]{2}\)<\/td><td>consecutive losses \(loss in money\)<\/td><td align=right>([0-9]+\s\([0-9\-]+\.[0-9]{2}\))<\/td>/', $original_file_html, $matches);
			$max_consecutive_losses_str = $matches[1];
			$max_consecutive_losses_sum = substr(end(explode(" ", $max_consecutive_losses_str)), 1, -1);
			$max_consecutive_losses_sum = number_format($max_consecutive_losses_sum, 2, ".", " ");
			$max_consecutive_losses = number_format(reset(explode(" ", $max_consecutive_losses_str)), 0, ".", " ") . " (" . $max_consecutive_losses_sum . ")";
		
			preg_match('/<td>consecutive profit \(count of wins\)<\/td><td align=right>([0-9]+\.[0-9]{2}\s\([0-9]+\))<\/td>/', $original_file_html, $matches);
			$max_consecutive_profit_str = $matches[1];
			$max_consecutive_profit_sum = reset(explode(" ", $max_consecutive_profit_str));
			$max_consecutive_profit_sum = number_format($max_consecutive_profit_sum, 2, ".", " ");
			$max_consecutive_profit = $max_consecutive_profit_sum . " (" . number_format(substr(end(explode(" ", $max_consecutive_profit_str)), 1, -1), 0, ".", " ") . ")";
		
			preg_match('/<td>consecutive loss \(count of losses\)<\/td><td align=right>([0-9\-]+\.[0-9]{2}\s\([0-9]+\))<\/td>/', $original_file_html, $matches);
			$max_consecutive_loss_str = $matches[1];
			$max_consecutive_loss_sum = reset(explode(" ", $max_consecutive_loss_str));
			$max_consecutive_loss_sum = number_format($max_consecutive_loss_sum, 2, ".", " ");
			$max_consecutive_loss = $max_consecutive_loss_sum . " (" . number_format(substr(end(explode(" ", $max_consecutive_loss_str)), 1, -1), 0, ".", " ") . ")";
			
			preg_match('/<tr align=left><td colspan=2 align=right>Average<\/td><td>consecutive wins<\/td><td align=right>(\d)<\/td>/', $original_file_html, $matches);
			$average_consecutive_wins = number_format($matches[1], 0, ".", " ");
		
			preg_match('/<tr align=left><td colspan=2 align=right>Average<\/td><td>consecutive wins<\/td><td align=right>\d<\/td><td>consecutive losses<\/td><td align=right>(\d)<\/td>/', $original_file_html, $matches);
			$average_consecutive_losses = number_format($matches[1], 0, ".", " ");*/
			
			$search_str = '<img src="StrategyTester.gif" width=820 height=200 border=0 alt="Graph"><br>
<table width=820 cellspacing=1 cellpadding=3 border=0>';
			$pos = strpos($original_file_html, $search_str);
			$closed_trades_html = substr($original_file_html, $pos + strlen($search_str));
		//		var_dump($pos);exit;
			$first = true;
			$trades_arr = array();
			preg_match_all('/[<tr bgcolor="#E0E0E0" align=right>|<tr align=right>](.*)<\/tr>/', $closed_trades_html, $matches);
		//	var_dump($matches[1]);exit;
			foreach ($matches[1] as $row)
			{
				if ($first)
				{
					$first = false;
					continue;
				}
				
			//	echo htmlspecialchars($row) . '<br />';
			//	preg_match_all('/<td>|<td class=msdate>|<td class=mspt>|<td style="mso-number-format:0\\\.00000;">|<td colspan=2>(.*)<\/td>/', $row, $macthes_row);
				preg_match('/<td>([0-9]+)<\/td>/', $row, $macthes_row, PREG_OFFSET_CAPTURE);
				$row_id = $macthes_row[1][0];
				$offset = $macthes_row[1][1];
				$row = substr($row, $offset);
			//	var_dump($row_id);
				
				preg_match('/<td class=msdate>([0-9\.\s:]+)<\/td>/', $row, $macthes_row, PREG_OFFSET_CAPTURE);
				$row_date = $macthes_row[1][0];
				$offset = $macthes_row[1][1];
				$row = substr($row, $offset);
				
				preg_match('/<td>([\w\/]+)<\/td>/', $row, $macthes_row, PREG_OFFSET_CAPTURE);
				$row_action = $macthes_row[1][0];
				$offset = $macthes_row[1][1];
				$row = substr($row, $offset);
		
				if ($row_action == 'modify')
					continue;
					
				preg_match('/<td>([0-9]+)<\/td>/', $row, $macthes_row, PREG_OFFSET_CAPTURE);
				$row_order = $macthes_row[1][0];
				$offset = $macthes_row[1][1];
				$row = substr($row, $offset);
			//	echo $row_id . ' - "' . $row_order . '" ';
			
				preg_match('/<td class=mspt>([\d\.]+)<\/td>/', $row, $macthes_row, PREG_OFFSET_CAPTURE);
				$trades_arr[$row_order]['lot_size'] = $macthes_row[1][0];
				$offset = $macthes_row[1][1];
				$row = substr($row, $offset);
		
				preg_match('/<td style="mso-number-format:0\\\.00000;">([\d\.]+)<\/td>/', $row, $macthes_row, PREG_OFFSET_CAPTURE);
				$row_price = $macthes_row[1][0];
				$offset = $macthes_row[1][1];
				$row = substr($row, $offset);
				
				if ($row_action == 'buy' || $row_action == 'sell')
				{
					$trades_arr[$row_order]['id'] = $row_id;
					$trades_arr[$row_order]['open_date'] = $row_date;
					$trades_arr[$row_order]['open_price'] = $row_price;
					$trades_arr[$row_order]['action'] = $row_action;
					$trades_arr[$row_order]['open_time'] = strtotime(str_replace('.', '-', $row_date) . ':00');
				}
				else if ($row_action == 's/l' || $row_action == 't/p' || $row_action == 'close')
				{
					$trades_arr[$row_order]['close_date'] = $row_date;
					$trades_arr[$row_order]['close_price'] = $row_price;
						
					preg_match('/<td style="mso-number-format:0\\\.00000;" align=right>([\d\.]+)<\/td>/', $row, $macthes_row, PREG_OFFSET_CAPTURE);
					$trades_arr[$row_order]['sl'] = $macthes_row[1][0];
					$offset = $macthes_row[1][1];
					$row = substr($row, $offset);
			
					preg_match('/<td style="mso-number-format:0\\\.00000;" align=right>([\d\.]+)<\/td>/', $row, $macthes_row, PREG_OFFSET_CAPTURE);
					$trades_arr[$row_order]['tp'] = $macthes_row[1][0];
					$offset = $macthes_row[1][1];
					$row = substr($row, $offset);
					
					preg_match('/<td class=mspt>([\d\.\-]+)<\/td>/', $row, $macthes_row, PREG_OFFSET_CAPTURE);
					$trades_arr[$row_order]['profit'] = number_format($macthes_row[1][0], 2, ".", " ");
					$offset = $macthes_row[1][1];
					$row = substr($row, $offset);
					/*
					preg_match('/<td class=mspt>([\d\.\-]+)<\/td>/', $row, $macthes_row, PREG_OFFSET_CAPTURE);
					$trades_arr[$row_order]['balance'] = $macthes_row[1][0];
					$offset = $macthes_row[1][1];
					$row = substr($row, $offset);	*/		
				}
		
			//	var_dump($macthes_row[1]);exit;
			}
			
			$tickets_values = array();
			$tickets_path = $path . 'files/tickets.txt';
			$handle = fopen($tickets_path, 'r');
			if ($handle)
			{
				while ($line = fgets($handle))
				{
					$tickets_values[] = $line;
				}
				fclose($handle);
				eval('$tickets_arr = array(' . implode(',', $tickets_values) . ');');
			}
			
			$br = 0;
			$first_ticket = 121768504;
			$tickets_per_hour = 7000;
			$tickets_per_min = 100;
			$rand_start_ratio = 0.95;
			$rand_end_ratio = 1.05;	
			$new_tickets = array();
			$last_ticket = end($tickets_arr);
			$trades_html = '';
			
		//	var_dump($trades_arr);exit;
			$first_row = true;
			foreach ($trades_arr as $order => $trade)
			{
				if ($first_row)
				{
					$deposit_time = strtotime("-2 days", $trades_arr[1]["open_time"]-(6*60*60 + 14*60)); // minus 2 dena, 6 chasa i 14 minuti ot pyrvia trade
					$deposit_date = date("Y.m.d H:i", $deposit_time);
					$trades_html .= '<tr align=right><td title="Deposit (AC-13455328682536)">' . $tickets_arr[0] . '</td><td class=msdate nowrap>' . $deposit_date . '</td><td>balance</td><td colspan=10 align=left>Deposit (AC-13455328682536)</td><td class=mspt>' . $deposit . '</td></tr>' . "\r\n";
					$last_ticket = $tickets_arr[0];
					
					$first_row = false;
					$br++;
				}
				
				if (isset($tickets_arr[$order]))
					$ticket = $tickets_arr[$order];
				else
				{
					$time_diff = $trade['open_time'] - $trades_arr[$order-1]['open_time'];
					$mins_diff = intval($time_diff / 60);
					$ticket = $last_ticket + rand($tickets_per_min*$mins_diff*$rand_start_ratio, $tickets_per_min*$mins_diff*$rand_end_ratio);
					$last_ticket = $ticket;
					$new_tickets[$order] = $ticket;
				}
		
				$trades_html .= '<tr ' . (($br % 2 != 0) ? 'bgcolor=#E0E0E0 ' : '') . 'align=right><td>' . $ticket . '</td><td nowrap>' . $trade['open_date'] . '</td><td>' . $trade['action'] . '</td><td class=mspt>' . $trade['lot_size'] . '</td><td>' . $pair . '</td><td style="mso-number-format:0\.000;">' . $trade['open_price'] . '</td><td style="mso-number-format:0\.000;">' . $trade['sl'] . '</td><td style="mso-number-format:0\.000;">' . $trade['tp'] . '</td><td class=msdate nowrap>' . $trade['close_date'] . '</td><td style="mso-number-format:0\.000;">' . $trade['close_price'] . '</td><td class=mspt>0.00</td><td class=mspt>0.00</td><td class=mspt>0.00</td><td class=mspt>' . $trade['profit'] . '</td></tr>' . "\r\n";
				$br++;
			}
			
			if (count($new_tickets) > 0)
			{
				$handle = fopen($tickets_path, 'a');
				if ($handle)
				{
					$new_line_str = (strpos($_SERVER['HTTP_HOST'], 'localhost') === false) ? "\n" : "\r\n";
					foreach ($new_tickets as $order_id => $order_ticket)
					{
						fputs($handle, $order_id . " => " . $order_ticket . $new_line_str);
					}
					fclose($handle);
				}
			}
			
			$html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
	<title>Statement: ' . $account_number . ' - ' . $account_name . '</title>
	<meta name="generator" content="MetaQuotes Software Corp.">
	<link rel="help" href="http://www.metaquotes.net">
	<style type="text/css" media="screen">
	<!--
	td { font: 8pt Tahoma,Arial; }
	//-->
	</style>
	<style type="text/css" media="print">
	<!--
	td { font: 7pt Tahoma,Arial; }
	//-->
	</style>
	<style type="text/css">
	<!--
	.msdate { mso-number-format:"General Date"; }
	.mspt   { mso-number-format:\#\,\#\#0\.00;  }
	//-->
	</style>
  </head>
<body topmargin=1 marginheight=1>
<div align=center>
<div style="font: 20pt Times New Roman"><b>Alpari NZ Limited</b></div><br>';
		
			$html .= '<table cellspacing=1 cellpadding=3 border=0>
<tr align=left>
	<td colspan=2><b>Account: ' . $account_number . '</b></td>
	<td colspan=6><b>Name: ' . $account_name . '</b></td>
	<td colspan=2><b>Currency: USD</b></td>
	<td colspan=4 align=right><b>' . $date_generated . '</b></td></tr>

<tr align=left><td colspan=14><b>Closed Transactions:</b></td></tr>
<tr align=center bgcolor="#C0C0C0">
   <td>Ticket</td><td nowrap>Open Time</td><td>Type</td><td>Size</td><td>Item</td>
   <td>Price</td><td>S / L</td><td>T / P</td><td nowrap>Close Time</td>
   <td>Price</td><td>Commission</td><td>Taxes</td><td>Swap</td><td>Profit</td></tr>' . $trades_html;
			
			$html .= '<tr align=right>
	<td colspan=10>&nbsp;</td>
	<td class=mspt>0.00</td>
	<td class=mspt>0.00</td>
	<td class=mspt>0.00</td>
	<td class=mspt>' . $closed_trade_pl . '</td>
</tr>

<tr align=right>
	<td colspan=12 align=right><b>Closed P/L:</b></td>
	<td colspan=2 align=right title="Commission + Swap + Profit + Taxes" class=mspt><b>' . $closed_trade_pl . '</b></td>
</tr>';
			
			$html .= '<tr align=left><td colspan=14><b>Open Trades:</b></td></tr>
<tr align=center bgcolor="#C0C0C0">
	<td>Ticket</td><td nowrap>Open Time</td><td>Type</td><td>Size</td><td>Item</td>
	<td>Price</td><td>S / L</td><td>T / P</td><td>&nbsp;</td>
	<td>Price</td><td>Commission</td><td>Taxes</td><td>Swap</td><td>Profit</td></tr>
<tr align=right><td colspan=13 align=center>No transactions</td></tr>


<tr align=right>
	<td colspan=10>&nbsp;</td>
	<td class=mspt>0.00</td>
	<td class=mspt>0.00</td>
	<td class=mspt>0.00</td>
	<td class=mspt>0.00</td>
</tr>
<tr><td colspan=10>&nbsp;</td><td colspan=2 align=right><b>Floating P/L:</b></td>
	<td colspan=2 align=right title="Commission + Swap + Profit + Taxes" class=mspt><b>0.00</b></td></tr>


<tr><td colspan=14 style="font: 1pt arial">&nbsp;</td></tr>';
			
			$html .= '<tr align=left><td colspan=14><b>Summary:</b></td></tr>
<tr align=right>
	<td colspan=2><b>Deposit/Withdrawal:</b></td>
	<td colspan=2 class=mspt><b>' . $deposit . '</b></td>
	<td colspan=4><b>Credit Facility:</b></td>
	<td class=mspt><b>0.00</b></td>
	<td colspan=5>&nbsp;</td></tr>
	
<tr align=right>
	<td colspan=2><b>Closed Trade P/L:</b></td>
	<td colspan=2 class=mspt><b>' . $closed_trade_pl . '</b></td>
	<td colspan=4><b>Floating P/L:</b></td>
	<td class=mspt><b>0.00</b></td>
	<td colspan=3><b>Margin:</b></td>
	<td colspan=2 class=mspt><b>0.00</b></td></tr>

<tr align=right>
	<td colspan=2><b>Balance:</b></td>
	<td colspan=2 class=mspt><b>' . $balance . '</b></td>
	<td colspan=4><b>Equity:</b></td>
	<td class=mspt><b>' . $equity . '</b></td>
	<td colspan=3><b>Free Margin:</b></td>
	<td colspan=2 class=mspt><b>' . $free_margin . '</b></td></tr>';
		/*   
			$html .= '<tr><td colspan=14 style="font: 1pt arial">&nbsp;</td></tr>
		<tr align=left><td colspan=14><b>Details:</b></td></tr>
		
		<tr align=center><td colspan=14><img src="StrategyTester.gif" width=780 height=200 border=0 alt="Graph"></td></tr>
		
		<tr align=right>
			<td colspan=2><b>Gross Profit:</b></td>
			<td colspan=2 class=mspt><b>' . $gross_profit . '</b></td>
			<td colspan=4><b>Gross Loss:</b></td>
			<td class=mspt><b>' . $gross_loss . '</b></td>
			<td colspan=3><b>Total Net Profit:</b></td>
			<td colspan=2 class=mspt><b>' . $total_net_profit . '</b></td></tr>
			
		<tr align=right>
			<td colspan=2><b>Profit Factor:</b></td>
			<td colspan=2 class=mspt><b>' . $profit_factor . '</b></td>
			<td colspan=4><b>Expected Payoff:</b></td>
			<td class=mspt><b>' . $expected_payoff . '</b></td>
			<td colspan=5>&nbsp;</td></tr>
			
		<tr align=right>
			<td colspan=2><b>Absolute Drawdown:</b></td>
			<td colspan=2 class=mspt><b>' . $absolute_drawdown . '</b></td>
			<td colspan=4><b>Maximal Drawdown:</b></td>
			<td class=mspt><b>' . $maximal_drawdown . '</b></td>
			<td colspan=3><b>Relative Drawdown:</b></td>
			<td colspan=2 class=mspt><b>' . $relative_drawdown . '</b></td></tr>
		
		<tr><td colspan=14 style="font: 1pt arial">&nbsp;</td></tr>
			
		<tr align=right>
			<td colspan=2><b>Total Trades:</b></td>
			<td colspan=2 class=mspt><b>' . $total_trades . '</b></td>
			<td colspan=4><b>Short Positions (won %):</b></td>
			<td class=mspt><b>' . $short_positions_won . '</b></td>
			<td colspan=3><b>Long Positions (won %):</b></td>
			<td colspan=2 class=mspt><b>' . $long_positions_won . '</b></td></tr>
		
		<tr align=right>
			<td colspan=8><b>Profit Trades (% of total):</b></td>
			<td class=mspt><b>' . $profit_trades . '</b></td>
			<td colspan=3><b>Loss trades (% of total):</b></td>
			<td colspan=2 class=mspt><b>' . $loss_trades . '</b></td></tr>
		
		<tr align=right>
			<td colspan=2><b>Largest</b></td>
			<td colspan=6><b>profit trade:</b></td>
			<td class=mspt><b>' . $largest_profit_trade . '</b></td>
			<td colspan=3><b>loss trade:</b></td>
			<td colspan=2 class=mspt><b>' . $largest_loss_trade . '</b></td></tr>
		
		<tr align=right>
			<td colspan=2><b>Average</b></td>
			<td colspan=6><b>profit trade:</b></td>
			<td class=mspt><b>' . $average_profit_trade . '</b></td>
			<td colspan=3><b>loss trade:</b></td>
			<td colspan=2 class=mspt><b>' . $average_loss_trade . '</b></td></tr>
		
		<tr align=right>
			<td colspan=2><b>Maximum</b></td>
			<td colspan=6><b>consecutive wins ($):</b></td>
			<td class=mspt><b>' . $max_consecutive_wins . '</b></td>
			<td colspan=3><b>consecutive losses ($):</b></td>
			<td colspan=2 class=mspt><b>' . $max_consecutive_losses . '</b></td></tr>
		
		<tr align=right>
			<td colspan=2><b>Maximal</b></td>
			<td colspan=6><b>consecutive profit (count):</b></td>
			<td class=mspt><b>' . $max_consecutive_profit . '</b></td>
			<td colspan=3><b>consecutive loss (count):</b></td>
			<td colspan=2 class=mspt><b>' . $max_consecutive_loss . '</b></td></tr>
		
		<tr align=right>
			<td colspan=2><b>Average</b></td>
			<td colspan=6><b>consecutive wins:</b></td>
			<td class=mspt><b>' . $average_consecutive_wins . '</b></td>
			<td colspan=3><b>consecutive losses:</b></td>
			<td colspan=2 class=mspt><b>' . $average_consecutive_losses . '</b></td></tr>';*/
			
			$html .= '</table>
</div></body></html>';
		
			echo $html;
			exit;
		}
	}
	
	public function check_statement_file()
	{
		if (isset($_FILES['statement']) && $_FILES['statement']['size'] > 0 && $_FILES['statement']['type'] == 'text/html')
			return true;
			
		$this->form_validation->set_message("check_statement_file", "Please upload a valid statement file.");
		return false;
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */