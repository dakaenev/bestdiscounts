<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->administration = true;
                if(strtolower($this->uri->segment(3)) != 'get_article_not_logged' && strtolower($this->uri->segment(3)) != 'login')
                {
                    if(strtolower($this->uri->segment(3)) == 'get_article_logged' || strtolower($this->uri->segment(3)) == 'get_articles_by_category')
                    {
                        if(!check_rights(USER_ADMIN, false))
                        {
                            echo json_encode(array('status' => 'error', 'html_code' => 'You are not logged!'));
                            exit;
                        }
                    }
                    else
                    {
                        check_rights(USER_ADMIN);
                    }
                }
	}
	
	public function full_screen($mode)
	{
		$this->load->helper('cookie');
		
		if ($mode == "on")
		{
			$cookie = array(
				'name'   => 'full_screen',
				'value'  => '1',
				'expire' => (30 * 24 * 60 * 60),
				'path'   => '/'
			);

			set_cookie($cookie); 
		}
		else
		{
			delete_cookie("full_screen");
		}
		
		echo "1";
		
		exit;
	}
	
        public function load_product_options()
        {
            if ($this->input->post("product_id"))
            {
                    $this->load->model("products_model");
                    $result = $this->products_model->load_product_options($this->input->post("product_id"));	

                    if ($result)
                    {
                           if(is_array($result))
                           {
                               $counter = 0;
                               foreach($result as $option)
                               {
                                   $js_array = array();
                                   foreach($option as $k => $v)
                                   {
                                       //$js_array[] = ''
                                   }
                                   //$option['option_id'] = $option['id'];
                                   //unset($option['id']);
                                   //echo json_encode($option);
                                   echo '<tr id="option_'.$option['id'].'">
                                       <td>'.(++$counter).'</td>
                                       <td>'.$option['title'].'</td>
                                       <td>
                                        <button class="btn btn-small btn-info pull-left" onclick="prepare_edit_option('.$option['id'].');" type="button"><i class="icon-edit icon-white"></i>Edit</button>
                                        <button class="btn btn-small btn-danger pull-right" onclick="delete_option('.$option['id'].');" type="button"><i class="icon-remove icon-white"></i>Remove</button>
                                       </td>
                                      </tr>';
                               }
                           }
                           else
                           {
                               echo '<tr><td>Wrong return data type. It should be array!</td></tr>';
                           }
                    }
                    else
                    {
                           echo "There are no options!";
                    }
            }
            else
                    echo "Wrong posted data!";

            exit;
        }
        
        public function load_option()
        {
            if ($this->input->post("option_id"))
            {
                    $this->load->model("products_model");
                    $result = $this->products_model->load_option($this->input->post("option_id"));	

                    if ($result)
                    {
                           if(is_array($result))
                           {
                               echo json_encode($result);
                           }
                           else
                           {
                               echo '<tr><td>Wrong return data type. It should be array!</td></tr>';
                           }
                    }
                    else
                    {
                           echo "Non existing option!";
                    }
            }
            else
                    echo "Wrong posted data!";

            exit;
        }
        
        public function insert_update_option()
        {
            if ($this->input->post("data"))
            {
                    $old_data = array();
                    parse_str($this->input->post("data"), $old_data);
                    
                    $new_data = array(
                        'id'            =>   $old_data['id'],
                        'product_id'    =>   $old_data['product_id'],
                        'title'         =>   $old_data['option_title'],
                        'description'   =>   $old_data['descr'],
                        'buy_url'       =>   $old_data['url_b'],
                        'visit_url'     =>   $old_data['url_v'],
                        'price'         =>   $old_data['price_now'],
                        'old_price'     =>   $old_data['price_old'],
                        'discount'      =>   $old_data['discount'],
                        'coupon'        =>   $old_data['coupon'],
                    );
                    
                    $this->load->model("products_model");
                    $result = $this->products_model->insert_update_option($new_data);	

                    if ($result)
                    {
                        echo $result;
                    }
                    else
                    {
                        echo $result;
                        echo "Error occur on ".__LINE__.' in '.__FILE__;
                    }
            }
            else
            {
                echo "Wrong posted data!";
            }
            exit;
        }
        
	public function delete_user()
	{
		if ($this->input->post("user_id"))
		{
			$this->load->model("users_model");
			$result = $this->users_model->delete($this->input->post("user_id"));	
			
			if ($result)
			{
				$this->users_model->delete_user_product($this->input->post("user_id"), 1);
				$this->users_model->delete_user_accounts($this->input->post("user_id"), 1);
				
				echo "1";
			}
			else
				echo "0";
		}
		else
			echo "0";
		
		exit;
	}
	
        public function delete_option()
	{
		if ($this->input->post("option_id"))
		{
			$this->load->model("products_model");
			$result = $this->products_model->delete_option($this->input->post("option_id"));	
			
			if ($result)
			{
				echo "1";
			}
			else
				echo "0";
		}
		else
			echo "0";
		
		exit;
	}
        
        public function delete_category()
	{
		if ($this->input->post("category_id"))
		{
			$this->load->model("categories_model");
			$result = $this->categories_model->delete($this->input->post("category_id"));	
			
			if ($result)
			{
				//$this->categories_model->delete_user_product($this->input->post("user_id"), 1);
				//$this->categories_model->delete_user_accounts($this->input->post("user_id"), 1);
				
				echo "1";
			}
			else
				echo "0";
		}
		else
			echo "0";
		
		exit;
	}
        
        public function delete_product()
	{
		if ($this->input->post("product_id"))
		{
			$this->load->model("products_model");
			$result = $this->products_model->delete($this->input->post("product_id"));	
			
			if ($result)
			{
				//$this->categories_model->delete_user_product($this->input->post("user_id"), 1);
				//$this->categories_model->delete_user_accounts($this->input->post("user_id"), 1);
				
				echo "1";
			}
			else
				echo "0";
		}
		else
			echo "0";
		
		exit;
	}
        
	public function change_user_status()
	{
		if ($this->input->post("user_id"))
		{
			$user_id = $this->input->post("user_id");
			$this->load->model("users_model");
			$user = $this->users_model->get_user_info($user_id);
			
			if ($user)
			{
				if ($user["status"] == 1)
					$status = 0;
				else
					$status = 1;
				$data = array("id" => $this->input->post("user_id"), "status" => $status);	
				$result = $this->users_model->update($user_id, $data);
				if ($result)
					echo ($status == 1 ? "active" : "inactive");
				else
					echo "error";
			}
			else
				echo "error";
		}
		else
			echo "error";
		
		exit;
	}

        public function change_category_status()
	{
		if ($this->input->post("category_id"))
		{
			$category_id = $this->input->post("category_id");
			$this->load->model("categories_model");
			$category = $this->categories_model->get_category_info($category_id);
			
			if ($category)
			{
				if ($category["status"] == 1)
					$status = 0;
				else
					$status = 1;
				$data = array("id" => $this->input->post("category_id"), "status" => $status);	
				$result = $this->categories_model->update($category_id, $data);
				if ($result)
					echo ($status == 1 ? "active" : "inactive");
				else
					echo "error";
			}
			else
				echo "error";
		}
		else
			echo "error";
		
		exit;
	}

        public function change_product_status()
	{
		if ($this->input->post("product_id"))
		{
			$product_id = $this->input->post("product_id");
			$this->load->model("products_model");
			$product = $this->products_model->get_product_info($product_id);
			
			if ($product)
			{
				if ($product["status"] == 1)
					$status = 0;
				else
					$status = 1;
				$data = array("id" => $this->input->post("product_id"), "status" => $status);	
				$result = $this->products_model->update($product_id, $data);
				if ($result)
					echo ($status == 1 ? "active" : "inactive");
				else
					echo "error";
			}
			else
                        {
				echo "error";
                        }
		}
		else
                {
			echo "error";
                }
		exit;
	}

	public function update_user_account_numbers()
	{
		if ($this->input->post("user_id") && $this->input->post("product_id"))
		{
			$user_id = $this->input->post("user_id");
			$product_id = $this->input->post("product_id");
			$max_live = $this->input->post("max_live");
			$max_demo = $this->input->post("max_demo");
			
			$this->load->model("users_model");
			$result = $this->users_model->update_user_account_numbers($user_id, $product_id, $max_live, $max_demo);
			
			if ($result)
				echo "1";
			else
				echo "0";
		}
		else
			echo "0";
		
		exit;
	}
	
	public function delete_user_product()
	{
		if ($this->input->post("user_id") && $this->input->post("product_id"))
		{
			$user_id = $this->input->post("user_id");
			$product_id = $this->input->post("product_id");
			
			$this->load->model("users_model");
			$result = $this->users_model->delete_user_product($user_id, $product_id);
			
			if ($result)
				echo "1";
			else
				echo "0";
		}
		else
			echo "0";
		
		exit;
	}
	
	public function update_user_account_number()
	{
		if ($this->input->post("account_id") && $this->input->post("account_number"))
		{
			$account_id = $this->input->post("account_id");
			$account_number = $this->input->post("account_number");
			
			$this->load->model("users_model");
			$result = $this->users_model->update_user_account_number($account_id, $account_number);
			
			if ($result)
				echo "1";
			else
				echo "0";
		}
		else
			echo "0";
		
		exit;
	}
	
	public function delete_user_account_number()
	{
		if ($this->input->post("account_id"))
		{
			$account_id = $this->input->post("account_id");
			
			$this->load->model("users_model");
			$result = $this->users_model->delete_user_account_number($account_id);
			
			if ($result)
				echo "1";
			else
				echo "0";
		}
		else
			echo "0";
		
		exit;
	}

	public function change_current_robot_version()
	{
		$version_id = $this->input->post("version_id");
		if ($version_id)
		{
			$this->load->model("robots_model");
			$this->robots_model->set_current_version($version_id);
		}
		
		exit;
	}
	
	public function delete_robot_version()
	{
		$version_id = $this->input->post("version_id");
		if ($version_id)
		{
			$this->load->model("robots_model");
			if ($this->robots_model->delete_robot_version($version_id))
				echo '1';
			else
				echo '0';
		}
		else
			echo '0';
		
		exit;
	}

	public function delete_affiliate()
	{
		$affiliate_id = $this->input->post("affiliate_id");
		if ($affiliate_id)
		{
			$this->load->model("affiliates_model");
			if ($this->affiliates_model->delete_affiliate($affiliate_id))
				echo '1';
			else
				echo '0';
		}
		else
			echo '0';
		
		exit;
	}

	public function delete_faq()
	{
		$faq_id = $this->input->post("faq_id");
		if ($faq_id)
		{
			$this->load->model("faqs_model");
			if ($this->faqs_model->delete_faq($faq_id))
				echo '1';
			else
				echo '0';
		}
		else
			echo '0';
		
		exit;
	}
	
	public function activate_new_account_number()
	{
		$user_id = $this->input->post("user_id");
		$product_id = $this->input->post("product_id");
		$account_number = $this->input->post("account_number");
		$account_type = $this->input->post("account_type");
		
		if ($user_id && $product_id && $account_number && $account_type)
		{
			$this->load->model("users_model");
			
			$data = array();
			$data["user_id"] = $user_id;
			$data["product_id"] = $product_id;
			$data["account_number"] = $account_number;
			$data["account_type"] = $account_type;
			$data["published"] = date("Y-m-d H:i:s");
			
			$result = $this->users_model->activate_new_account_number($data);
			if ($result)
				echo $result;
			else
				echo '0';
		}
		else
			echo '0';
		
		exit;
	}

	public function save_user_comment()
	{
		if ($this->input->post("user_id"))
		{
			$user_id = $this->input->post("user_id");
			$comment = $this->input->post("comment");
			$this->load->model("users_model");
			
			$data = array();
			$data["comment"] = $comment;
			$result = $this->users_model->update($user_id, $data);
			
			if ($result)
				echo "1";
			else
				echo "0";
		}
		else
			echo "0";
		
		exit;
	}
	
        public function get_articles()
        {
            $status = '';
            $html = '';
                            
            if ($this->input->post("article_id") || $this->input->post("category_id"))
            {
                    $this->load->model("articles_model");
                    if($this->input->post("article_id"))
                    {
                        $data = $this->articles_model->get_article($this->input->post("article_id"));	
                    }
                    else
                    {
                        $data = $this->articles_model->get_articles_by_category($this->input->post("category_id"));
                    }
                    
                    if ($data)
                    {
                           if(is_array($data))
                           {
                               $status = 'success';
                               foreach($data as $row)
                               {
                                   $html .= $row['id'].'|'.$row['title'].'|'.$row['text'].'<br>';
                               }
                           }
                           else
                           {
                               $status = 'error';
                               $html = 'Wrong return data type. It should be array!';
                           }
                    }
                    else
                    {
                           $status = 'info';
                           $html = 'We can\'t found article with id = '.$this->input->post("article_id");
                    }
            }
            else
            {
                    $status = 'error';
                    $html = 'Wrong posted data!';
            }
            
            echo json_encode(array('status' => $status, 'html_code' => $html));
            exit;
        }
        
        public function get_article_not_logged()
        {
            return $this->get_articles();
            exit;
        }
        
        public function get_article_logged()
        {
            return $this->get_article_not_logged();
            exit;
        }
        
        public function get_articles_by_category()
        {
            return $this->get_articles();
            exit;
        }
        
        public function login()
        {
                $status = '';
                $message = '';
            
                $this->load->library("form_validation");
                $this->form_validation->set_error_delimiters("#", "");

                if ($this->form_validation->run("login_admin"))
                {
                        $email = $this->input->post("email");
                        $password = $this->input->post("password");

                        $this->load->model("users_model");
                        $user_info = $this->users_model->get_user_info_by_email($email);
                        if ($user_info)
                        {
                                if ($user_info["user_type"] == USER_ADMIN && md5($password) == $user_info["password"])
                                {
                                        $this->users_model->log_user($user_info);
                                        $status = 'success';
                                        $message = "Welcome ".$_SESSION["user"]["name"]."!";
                                        //set_flash_message("Information Box", "Welcome ".$_SESSION["user"]["name"]."!", array("auto_hide" => 2000));
                                        
                                }
                                else
                                {
                                        $status = 'error';
                                        $message = "Wrong login details! <br>Please try again.";
                                        //set_flash_message( "Information Box", "Wrong login details! Please try again.", array( "show_title" => true, "modal" => true, "width" => 400, "auto_hide" => 0, "show_button" => true ) );
                                }
                        }
                        else
                        {
                                $status = 'error'; 
                                $message = "Wrong login details! <br>Please try again.";
                                //set_flash_message( "Information Box", "Wrong login details! Please try again.", array( "show_title" => true, "modal" => true, "width" => 400, "auto_hide" => 0, "show_button" => true ) );
                        }
                }
                else
                {
                        $status = 'error';
                        
                        $errors = explode( "#", validation_errors() );
                        array_shift( $errors );
                        $message = "An error has occured! Please fill the form correctly.";
                        //set_flash_message( "Information Box", "An error has occured! Please fill the form correctly.", array( "show_title" => true, "modal" => true, "width" => 400, "auto_hide" => 0, "show_button" => true ), $errors );
                }
                
                echo json_encode(array('status' => $status, 'message' => $message));
                exit;
        }
        
        public function logout()
        {
            $this->load->model("users_model");
            $this->users_model->logout();

            $message = "You logged out successfully!";
            $status = 'success';
            
            echo json_encode(array('status' => $status, 'message' => $message));
            exit;
        }
}

?>