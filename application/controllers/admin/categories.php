<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->administration = true;
		$this->load->model("categories_model");
		if ($this->uri->segment(3) != "login")
		{
			check_rights(USER_ADMIN);		
		}
	}
	public function index()
	{
		$this->show_all();
	}
	
	
	public function show_all()
	{
		$this->show_categories("show_all");
	}
	
	public function show_categories($action)
	{
		$this->smarty_parser->assign("action", "show_all");
		
		$offset = $this->uri->segment(4,0);
		$limit = 100;
			
                $where = '';
		$where_or = array();
		$where_and = array();
		
		$keyword = $this->input->get("keyword");
		
		if($keyword)
		{
			$where_or[] = "name LIKE '%" . addslashes($keyword) . "%'";
			$where_or[] = "meta_title LIKE '%" . addslashes($keyword) . "%'";
			$where_or[] = "meta_keywords LIKE '%" . addslashes($keyword) . "%'";
                        $where_or[] = "meta_description LIKE '%" . addslashes($keyword) . "%'";
		}
                
		if (count($where_or) > 0)
                {
                    if($where != '')
                    {
                        $where .= " AND (" . implode(" OR ", $where_or) . ")";
                    }
                    else
                    {
                        $where = implode(" OR ", $where_or);
                    }
                }
                        
		$this->load->model("categories_model");
		$categories = $this->categories_model->get_all_categories($where, $offset, $limit, "published desc");
		
		$this->smarty_parser->assign('categories',$categories['rows']);
		$this->smarty_parser->assign('num_categories',$categories['count']);
		
		$this->smarty_parser->assign('offset', $offset);
		$this->smarty_parser->assign('keyword', $keyword);
		
		// navigator
		$this->load->library('pagination');
		
		$config['base_url'] 	= base_url() . "/admin/categories/" . $action . "/";
		$config['suffix'] 		= "/?keyword=" . $this->input->get("keyword");
		$config['first_url'] 	= $config['base_url'] . "0" . $config['suffix'];
		$config['total_rows'] 	= $categories['count'];
		$config['per_page'] 	= $limit;
		$config['uri_segment'] 	= 4;
		$config['num_links'] 	= 5;
		
		$this->pagination->initialize($config);
		$this->smarty_parser->assign('navigator', $this->pagination->create_links());
		// end navigator
		
		$this->smarty_parser->assign('action', $action);
		
		$output = $this->smarty_parser->fetch("admin/categories.htm");
		$this->output->set_output($output);
	}
	
	public function edit($category_id)
	{
		
		$errors = array();
		
		if ($category_id > 0 && $this->categories_model->category_exist($category_id, "id"))
		{
			$result = false;
			if($this->input->post("save"))
			{
				$data = array();
				$data["name"] 			= $this->input->post("name");
                                $data["meta_title"]                     = $this->input->post("meta_title");
                                $data["meta_description"]       	= $this->input->post("meta_description");
                                $data["meta_keywords"] 			= $this->input->post("meta_keywords");
                                $data["modified"] 			= date( "Y-m-d H:i:s" );
                                //$data["status"] 			= $this->input->post("status");	
				
				$this->load->library( "form_validation" );
				
				$this->form_validation->set_error_delimiters( "#", "" );
		
				$validated = $this->form_validation->run("update_category");		
				
				if ( $validated )
				{
                                    $result = $this->categories_model->update($category_id, $data);

                                    if ($result)
                                    {
                                            //set_flash_message( "Information Box", "The user profile has been updated successfully!", array( "show_title" => true, "modal" => true, "width" => 400, "height" => 200, "auto_hide" => 0, "show_button" => true ) );
                                            set_alert_message("The form was submitted successfully!", "The category has been updated successfully!", "", "success");
                                            redirect("/admin/categories/edit/" . $category_id);
                                    }
				}
				
				if ($validated == false || $result == false || count($errors) > 0)
				{
					if ($validated == false)
					{
						$errors = explode( "#", validation_errors() );
						array_shift( $errors );
					}
					
					if (count($errors) > 0)
						set_alert_message("Wrong data! Please check the list below.", "", $errors, "error");
					elseif ($result == false)
						set_alert_message("Error processing the form!", "There was an error processing the form. Please try again later.", "", "error");
					
					
					$this->smarty_parser->assign('data', $data);					
				}							
			}
			
			$category = $this->categories_model->get_category_info($category_id);
			$this->smarty_parser->assign('category', $category);
			
		}
		else
		{
			redirect("/admin/categories/");
		}	
		
		if (has_alert_message())
		{
			$this->smarty_parser->assign('alert_message', get_alert_message());
		}
		
		$this->smarty_parser->assign('action', "edit");
		$output = $this->smarty_parser->fetch("admin/categories.htm");
		$this->output->set_output($output);
	}
	
	public function add()
	{
		$errors = array();
		
		$category_id = 0;
		
		if($this->input->post("add"))
		{
			$data = array();
			$data["name"] 				= $this->input->post("name");
			$data["meta_title"]                     = $this->input->post("meta_title");
			$data["meta_description"]       	= $this->input->post("meta_description");
			$data["meta_keywords"] 			= $this->input->post("meta_keywords");
			$data["published"] 			= date( "Y-m-d H:i:s" );
			$data["status"] 			= $this->input->post("status");
			
			$this->load->library( "form_validation" );
			
			$this->form_validation->set_error_delimiters( "#", "" );
	
			$validated = $this->form_validation->run("add_category");		
			
			if ($validated)
			{
				$category_id = $this->categories_model->add($data);
				if($category_id > 0)
                                {
                                    set_alert_message("Congratulations!", "The category was created successfully!", "", "success");
                                    redirect("/admin/categories/edit/" . $category_id);
                                }
			}
			
			if ($validated == false || $category_id == 0)
			{
				if ($validated == false)
				{
					$errors = explode( "#", validation_errors() );
					array_shift( $errors );
				}
				
				if (count($errors) > 0)
					set_alert_message("Wrong data! Please check the list below.", "", $errors, "error");
				elseif ($category_id == 0)
					set_alert_message("Error processing the form!", "There was an error processing the form. Please try again later.", "", "error");
				
				$this->smarty_parser->assign('data', $data);					
			}	
		}
		
		if (has_alert_message())
		{
			$this->smarty_parser->assign('alert_message', get_alert_message());
		}
		
		
		$this->smarty_parser->assign('action', "add");
		$output = $this->smarty_parser->fetch("admin/categories.htm");
		$this->output->set_output($output);
	}
}