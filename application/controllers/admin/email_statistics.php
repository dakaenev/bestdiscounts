<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email_statistics extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->administration = true;
		
		check_rights(USER_ADMIN);
		
		$action = $this->uri->segment(2);
		$this->smarty_parser->assign("action", $action);
	}
	
	public function index()
	{
		$this->show_all();
	}
	
	public function show_all()
	{
		if (has_alert_message())
		{
			$this->smarty_parser->assign('alert_message', get_alert_message());
		}

		$events = $this->emailsstatistics_model->getEmailStatisticsEvents();
		$this->smarty_parser->assign( "events", $events );

		$email_statistics = $this->emailsstatistics_model->getAllEmailsStatistics();
		$this->smarty_parser->assign( "email_statistics", $email_statistics );				
		
		$output = $this->smarty_parser->fetch("admin/email_statistics.htm");
		$this->output->set_output($output);
	}
	
	/*public function add()
	{
		if ($this->input->post("add"))
		{
			$this->load->library("form_validation");
			$this->form_validation->set_error_delimiters("#", "");

			if ($this->form_validation->run("add_email_campaign"))
			{
				$data = array();
				$data["name"] 		 = $this->input->post("name");
				$data["description"] = $this->input->post("description");
				$data["published"] 	 = now();
				
				if ($this->emailsstatistics_model->add($data))
				{
					set_alert_message("Success", "The campaign has been added successfully!", "", "success");
					redirect("/admin/email_statistics/show_all");						
				}
				else
				{
					set_alert_message("Error", "An error has occurred during DB insert. Please try again later.", "", "error");
				}
			}
			else
			{
				$errors = explode("#", validation_errors());
				array_shift( $errors );

				set_alert_message("Wrong data! Please check the list below.", "", $errors, "error");
				
				foreach ($this->input->post() as $key => $value)
					$this->smarty_parser->assign($key, $value);
			}
		}

		if (has_alert_message())
		{
			$this->smarty_parser->assign('alert_message', get_alert_message());
		}

		$output = $this->smarty_parser->fetch("admin/email_statistics.htm");
		$this->output->set_output($output);
	}	*/
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */