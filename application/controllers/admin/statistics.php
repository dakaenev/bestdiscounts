<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statistics extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->administration = true;
		
		check_rights(USER_ADMIN);
	}
	
	public function index()
	{
		$this->load->model("products_model");
		$otea_contracts = $this->products_model->get_product_contracts(1);
		$this->smarty_parser->assign("otea_contracts", $otea_contracts);
		$this->smarty_parser->assign("otea_contracts_count", count($otea_contracts));
		
		$this->load->model("statistics_model");
		$start_date = $this->statistics_model->get_stats_start_date();

		if ($start_date)
		{
			$end_time = time();
			$start_time = strtotime($start_date);
			
			$start_year = date("Y", $start_time);
			$start_month = date("m", $start_time);
			$start_day = date("d", $start_time);
			
			$end_year = date("Y", $end_time);
			$end_month = date("m", $end_time);
			$end_day = date("d", $end_time);
			
			$where_start = $start_date;
			$where_start_time = strtotime($where_start);
			$month_days = cal_days_in_month(CAL_GREGORIAN, $start_month, $start_year);
			$where_end = mktime(23, 59, 59, $start_month, $month_days, $start_year);
			
			$where_arr = array();
			$where_arr[] = array("start" => $where_start, "end" => strftime("%Y-%m-%d %H:%M:%S", $where_end));

			while ($where_end < $end_time)
			{
				$where_start_time = strtotime("+1 second", $where_end);
				$where_start = strftime("%Y-%m-%d %H:%M:%S", $where_start_time);
				
				$year = date("Y", $where_start_time);
				$month = date("m", $where_start_time);
				$month_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				
				$where_end = mktime(23, 59, 59, $month, $month_days, $year);
				
				$where_arr[] = array("start" => $where_start, "end" => strftime("%Y-%m-%d %H:%M:%S", $where_end));
			}
			$where_arr = array_reverse($where_arr);
			
			$results = array();
			foreach ($where_arr as $period)
			{
				$new_customers = $this->statistics_model->get_new_customers($period["start"], $period["end"]);
				
				$contracts_results = array();
				foreach ($otea_contracts as $contract)
				{
					$contracts_results[$contract["id"]]['sales'] = $this->statistics_model->get_contract_stats($contract["id"], $period["start"], $period["end"], false);
					$contracts_results[$contract["id"]]['refunds'] = $this->statistics_model->get_contract_stats($contract["id"], $period["start"], $period["end"], true);
				}
				
				$total_sales = $this->statistics_model->get_total_stats($period["start"], $period["end"], false);
				$total_refunds = $this->statistics_model->get_total_stats($period["start"], $period["end"], true);
				$regnow_sales = $this->statistics_model->get_payment_processor_stats('regnow', $period["start"], $period["end"], false);
				$regnow_refunds = $this->statistics_model->get_payment_processor_stats('regnow', $period["start"], $period["end"], true);
				
				$period_month = date("F", strtotime($period["start"]));
				$period_year = date("Y", strtotime($period["start"]));
				
				$results[$period_year][$period_month] = compact("new_customers", "contracts_results", "total_sales", "total_refunds", "regnow_sales", "regnow_refunds");
			}
			
			$this->smarty_parser->assign("results", $results);
		//	var_dump($results);
		}
				
		$output = $this->smarty_parser->fetch("admin/statistics.htm");
		$this->output->set_output($output);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */