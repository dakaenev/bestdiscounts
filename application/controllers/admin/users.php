<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->administration = true;
		
		if ($this->uri->segment(3) != "login")
		{
			check_rights(USER_ADMIN);		
		}
	}
	public function index()
	{
		$this->customers();
	}
	
	public function login()
	{
		$this->layout = "simple";
		
		$error = "";
		
		if ($this->input->post("login"))
		{
			$this->load->library("form_validation");
			$this->form_validation->set_error_delimiters("#", "");
			
			if ($this->form_validation->run("login_admin"))
			{
				$email = $this->input->post("email");
				$password = $this->input->post("password");
				
				$this->load->model("users_model");
				$user_info = $this->users_model->get_user_info_by_email($email);
				if ($user_info)
				{
					if ($user_info["user_type"] == USER_ADMIN && md5($password) == $user_info["password"])
					{
						$this->users_model->log_user($user_info);
						
						if ($this->input->post("remember_me"))
						{
							$this->users_model->set_user_cookie($user_info);
						}
						
						set_flash_message("Information Box", "Welcome ".$_SESSION["user"]["name"]."!", array("auto_hide" => 2000));
						redirect("/admin/");
					}
					else
					{
						$error = "Wrong login details! Please try again.";
						//set_flash_message( "Information Box", "Wrong login details! Please try again.", array( "show_title" => true, "modal" => true, "width" => 400, "auto_hide" => 0, "show_button" => true ) );
					}
				}
				else
				{
					$error = "Wrong login details! Please try again.";
					//set_flash_message( "Information Box", "Wrong login details! Please try again.", array( "show_title" => true, "modal" => true, "width" => 400, "auto_hide" => 0, "show_button" => true ) );
				}
			}
			else
			{
				$errors = explode( "#", validation_errors() );
				array_shift( $errors );
				$error = "An error has occured! Please fill the form correctly.";
				//set_flash_message( "Information Box", "An error has occured! Please fill the form correctly.", array( "show_title" => true, "modal" => true, "width" => 400, "auto_hide" => 0, "show_button" => true ), $errors );
			}
			
		}
		
		$this->smarty_parser->assign("error", $error);
		
		$this->smarty_parser->assign("action", "login");
		$output = $this->smarty_parser->fetch("admin/users.htm");
		$this->output->set_output($output);
	}
	
	public function logout()
	{
		$this->load->model("users_model");
		$this->users_model->logout();
				
		set_flash_message( "Information Box", "You logged out successfully!", array( "show_title" => true, "modal" => true, "width" => 400, "auto_hide" => 0, "show_button" => true ) );	
		redirect("/");
	}
	
	public function customers()
	{
		$this->show_users("customers");
	}
	
	public function inactive_customers()
	{
		$this->show_users("inactive_customers");
	}
	
	public function administrators()
	{
		$this->show_users("administrators");
	}
	
	public function show_users($action)
	{
		$this->smarty_parser->assign("action", "customers");
		
		$offset = $this->uri->segment(4,0);
		$limit = 100;
				
		$where_or = array();
		$where_and = array();
		
		$keyword = $this->input->get("keyword");
		
		if($keyword)
		{
			$where_or[] = "email LIKE '%" . addslashes($keyword) . "%'";
			$where_or[] = "first_name LIKE '%" . addslashes($keyword) . "%'";
			$where_or[] = "last_name LIKE '%" . addslashes($keyword) . "%'";
		}
		
		switch ($action)
		{
			case "customers" : 
			{ 
				$where_and[] = "user_type = '" . USER_NORMAL . "'";
				$where_and[] = "status = '1'";
			} break;
			
			case "inactive_customers" : 
			{
				$where_and[] = "user_type = '" . USER_NORMAL . "'";
				$where_and[] = "status = '0'";
			} break;
			
			case "administrators" : 
			{
				$where_and[] = "user_type = '" . USER_ADMIN . "'";
			} break;
		}
		
		
		$where = implode(" AND ", $where_and);
		
		if (count($where_or) > 0)
			$where .= " AND (" . implode(" OR ", $where_or) . ")";
					
		$this->load->model("users_model");
		$users = $this->users_model->get_all_users($where, $offset, $limit, "published desc");
		
		$this->smarty_parser->assign('users',$users['rows']);
		$this->smarty_parser->assign('num_users',$users['count']);
		
		$this->smarty_parser->assign('offset', $offset);
		$this->smarty_parser->assign('keyword', $keyword);
		
		// navigator
		$this->load->library('pagination');
		
		$config['base_url'] 	= base_url() . "/admin/users/" . $action . "/";
		$config['suffix'] 		= "/?keyword=" . $this->input->get("keyword");
		$config['first_url'] 	= $config['base_url'] . "0" . $config['suffix'];
		$config['total_rows'] 	= $users['count'];
		$config['per_page'] 	= $limit;
		$config['uri_segment'] 	= 4;
		$config['num_links'] 	= 5;
		
		$this->pagination->initialize($config);
		$this->smarty_parser->assign('navigator', $this->pagination->create_links());
		// end navigator
		
		$this->smarty_parser->assign('action', $action);
		
		$output = $this->smarty_parser->fetch("admin/users.htm");
		$this->output->set_output($output);
	}
	
	public function edit($user_id)
	{
		$this->load->model("users_model");
		$errors = array();
		
		if ($user_id > 0 && $this->users_model->user_exist($user_id, "id"))
		{
			$result = false;
			if($this->input->post("save"))
			{
				$data = array();
				$data["email"] 				= $this->input->post("email");
				$data["additional_email"]	= $this->input->post("additional_email");
				$data["first_name"] 		= $this->input->post("first_name");
				$data["last_name"] 			= $this->input->post("last_name");
				$data["country"] 			= $this->input->post("country");
				$data["city"] 				= $this->input->post("city");
				$data["post_code"] 			= $this->input->post("post_code");
				$data["address"] 			= $this->input->post("address");
				$data["phone"] 				= $this->input->post("phone");
				$data["referrer"] 			= $this->input->post("referrer");
				$data["company"] 			= $this->input->post("company");
				$data["comment"] 			= $this->input->post("comment");
				$data["is_affiliate"] 		= $this->input->post("is_affiliate");
				$data["newsletter"] 		= $this->input->post("newsletter");
				$data["password"] 			= $this->input->post("password");
				$data["modified"] 			= date( "Y-m-d H:i:s" );		
				
				$this->load->library( "form_validation" );
				
				$this->form_validation->set_error_delimiters( "#", "" );
		
				$validated = $this->form_validation->run("update_user");		
				
				if ( $validated )
				{
					$email_available = $this->users_model->is_email_available($data["email"], $user_id);
					if ( !$email_available )	
					{
						$errors[] = "The email address " . $data["email"] . " is already in use.";
					}
					else
					{
						if($data["password"] !== false && $data["password"] != "")
							$data["password"] = md5($data["password"]);
						else
							unset($data["password"]);
							
						$result = $this->users_model->update($user_id, $data);
						
						if ($result)
						{
							//set_flash_message( "Information Box", "The user profile has been updated successfully!", array( "show_title" => true, "modal" => true, "width" => 400, "height" => 200, "auto_hide" => 0, "show_button" => true ) );
							set_alert_message("The form was submitted successfully!", "The user profile has been updated successfully!", "", "success");
							redirect("/admin/users/edit/" . $user_id);
						}
					}
				}
				
				if ($validated == false || $result == false || count($errors) > 0)
				{
					if ($validated == false)
					{
						$errors = explode( "#", validation_errors() );
						array_shift( $errors );
					}
					
					if (count($errors) > 0)
						set_alert_message("Wrong data! Please check the list below.", "", $errors, "error");
					elseif ($result == false)
						set_alert_message("Error processing the form!", "There was an error processing the form. Please try again later.", "", "error");
					
					
					$this->smarty_parser->assign('data', $data);					
				}							
			}
			
			$user = $this->users_model->get_user_info($user_id);
			$this->smarty_parser->assign('user', $user);
			
		}
		else
		{
			redirect("/admin/users/");
		}	
		
		if (has_alert_message())
		{
			$this->smarty_parser->assign('alert_message', get_alert_message());
		}
		
		$this->smarty_parser->assign('action', "edit");
		$output = $this->smarty_parser->fetch("admin/users.htm");
		$this->output->set_output($output);
	}
	
	public function add()
	{
		$errors = array();
		
		$user_id = 0;
		
		if($this->input->post("add"))
		{
			$data = array();
			$data["email"] 				= $this->input->post("email");
			$data["additional_email"]	= $this->input->post("additional_email");
			$data["first_name"] 		= $this->input->post("first_name");
			$data["last_name"] 			= $this->input->post("last_name");
			$data["country"] 			= $this->input->post("country");
			$data["city"] 				= $this->input->post("city");
			$data["post_code"] 			= $this->input->post("post_code");
			$data["address"] 			= $this->input->post("address");
			$data["phone"] 				= $this->input->post("phone");
			$data["referrer"] 			= $this->input->post("referrer");
			$data["company"] 			= $this->input->post("company");
			$data["comment"] 			= $this->input->post("comment");
			$data["is_affiliate"] 		= $this->input->post("is_affiliate");
			$data["newsletter"] 		= $this->input->post("newsletter");
			$data["password"] 			= $this->input->post("password");
			$data["published"] 			= date( "Y-m-d H:i:s" );
			$data["status"] 			= $this->input->post("status");
			$password		 			= $this->input->post("password");
			$contracts 					= $this->input->post("contracts");
			
			$data["user_type"] 			= $this->input->post("user_type");
			
			if ($data["password"] != "")
				$data["password"] = md5($password);
			else
			{
				$password = generate_random_password();
				$data["password"] = md5($password);
			}
			
			$this->load->library( "form_validation" );
			
			$this->form_validation->set_error_delimiters( "#", "" );
	
			$validated = $this->form_validation->run("add_user");		
			
			if ($validated)
			{
				$user_id = $this->users_model->add($data);
				if ($user_id > 0)
				{								
					//begin email
					$this->smarty_parser->assign("name", $data["first_name"] . " " . $data["last_name"]);
					$this->smarty_parser->assign("email", $data["email"]);
					$this->smarty_parser->assign("password", $password);
					$this->smarty_parser->assign("base_url", base_url());
					$email_content = $this->smarty_parser->fetch("admin/emails/add_user.htm");
					$this->smarty_parser->clearAllAssign();
					
					$this->load->model("emails_model");
					$data_email["from_name"] = SUPPORT_EMAIL_FROM_NAME;
					$data_email["from_email"] = SUPPORT_EMAIL;
					$data_email["subject"] = "Welcome to " . WEBSITE_NAME;
					$data_email["mail_content"]	= nl2br($email_content);
					$data_email["recipients"] = array();
					$data_email["recipients"][]	= array("recipient_name" => $data["first_name"] . " " . $data["last_name"], "recipient_email" => $data["email"], "type" => 1);
					$this->emails_model->add($data_email);
					//end email
					
					set_alert_message("Congratulations!", "The user was added successfully!", "", "success");
					redirect("/admin/users/edit/" . $user_id);
				}
			}
			
			if ($validated == false || $user_id == 0)
			{
				if ($validated == false)
				{
					$errors = explode( "#", validation_errors() );
					array_shift( $errors );
				}
				
				if (count($errors) > 0)
					set_alert_message("Wrong data! Please check the list below.", "", $errors, "error");
				elseif ($user_id == 0)
					set_alert_message("Error processing the form!", "There was an error processing the form. Please try again later.", "", "error");
				
				$this->smarty_parser->assign('data', $data);					
			}	
		}
		
		if (has_alert_message())
		{
			$this->smarty_parser->assign('alert_message', get_alert_message());
		}
		
		
		$this->smarty_parser->assign('action', "add");
		$output = $this->smarty_parser->fetch("admin/users.htm");
		$this->output->set_output($output);
	}
}