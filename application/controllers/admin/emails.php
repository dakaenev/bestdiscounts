<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emails extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->administration = true;
		
		check_rights(USER_ADMIN);
		
		$action = $this->uri->segment(3);
		$this->smarty_parser->assign("action", $action);
		
		$this->load->model("emails_model");
	}
	
	public function index()
	{
		$this->view_sent();
	}
	
	public function view_sent()
	{
		if (has_alert_message())
		{
			$this->smarty_parser->assign('alert_message', get_alert_message());
		}
		
		$offset = $this->uri->segment(4,0);
		$limit = 50;

		$keyword = $this->input->get("keyword");
		$this->smarty_parser->assign("keyword", $keyword);
		$where_or = array();
		
		if ($keyword)
		{
			$where_or[] = "from_name LIKE '%" . addslashes($keyword) . "%'";
			$where_or[] = "from_email LIKE '%" . addslashes($keyword) . "%'";
			$where_or[] = "subject LIKE '%" . addslashes($keyword) . "%'";
			$where_or[] = "mail_content LIKE '%" . addslashes($keyword) . "%'";
			$where_or[] = "emails_recipients.recipient_name LIKE '%" . addslashes($keyword) . "%'";
			$where_or[] = "emails_recipients.recipient_email LIKE '%" . addslashes($keyword) . "%'";
		}

		$where = "";
		if (count($where_or))
			$where = implode(" OR ", $where_or);
			
		$all_emails = $this->emails_model->get_all_emails($where, $limit, $offset);
		$this->smarty_parser->assign("all_emails", $all_emails);
		
		// navigator
		$this->load->library('pagination');
		
		$config['base_url'] 	= base_url() . "/admin/emails/view_sent/";
		$config['suffix'] 		= "/?keyword=" . $this->input->get("keyword");
		$config['first_url'] 	= $config['base_url'] . "0" . $config['suffix'];
		$config['total_rows'] 	= $all_emails['count'];
		$config['per_page'] 	= $limit;
		$config['uri_segment'] 	= 4;
		$config['num_links'] 	= 5;
		
		$this->pagination->initialize($config);
		$this->smarty_parser->assign('navigator', $this->pagination->create_links());
		// end navigator		
		
		$output = $this->smarty_parser->fetch("admin/emails.htm");
		$this->output->set_output($output);
	}	
	
	public function send()
	{
		if ($this->input->post("send_email"))
		{
			$email_to 	= $this->input->post("email_to");
			$email_from = $this->input->post("email_from");
			$subject 	= $this->input->post("subject");
			$message 	= $this->input->post("message");
			$name 		= $this->input->post("name");

			$active_clients    = $this->input->post("active_clients");
			
			$use_template = $this->input->post("use_template") ? 1 : 0;
			
			$this->load->library("form_validation");
			$this->form_validation->set_error_delimiters("#", "");
			
			$unsubscribe_template = '<br /><br /><br /><br /><br /><br /><br /><br /><br /><br />Omega Trend LTD, E Indian School Rd 1254, Phoenix, Arizona, U.S.A.<br />
			<a href="' . base_url() . 'unsubscribe/?email=[email]&type=[type]&hash=[hash]">Unsubscribe</a>
			';
			
			$make_statistics = $this->input->post("make_statistics") ? 1 : 0;
			$email_campaign_code = '<img src="' . base_url() . 'photo?e_id=[email_id]&email=[email]" alt="" />';
			
			if ($this->form_validation->run("send_email"))
			{
				$this->load->model("users_model");
				$recipients_arr = array();
				$unsubscribe_link_arr = $this->input->post("unsubscribe_link");
				
				if (is_array($active_clients) && count($active_clients))
				{					
					foreach ($active_clients as $client)
					{
						if ($client == "all")
							continue;
							
						$client_info = $this->users_model->get_user_info($client);
						if ($client_info)
						{
							$unsubscribe = "";
							if ($unsubscribe_link_arr && is_array($unsubscribe_link_arr) && isset($unsubscribe_link_arr["u"]))
							{
								$unsubscribe = $unsubscribe_template;
								$unsubscribe = str_replace( "[email]", $client_info["email"], $unsubscribe );
								$unsubscribe = str_replace( "[type]", "u", $unsubscribe );
								$unsubscribe = str_replace( "[hash]", md5( $client_info["email"] . "user" ), $unsubscribe );
							}
							$recipients_arr[] = array("recipient_name" => $client_info["first_name"] . " " . $client_info["last_name"], "recipient_email" => $client_info["email"], "type" => "1", "unsubscribe" => $unsubscribe);							
						}
					}
				}
				
				if (isset($_FILES["csv_emails"]) && $_FILES["csv_emails"]["size"] > 0 )
				{
					if (($handle = fopen($_FILES["csv_emails"]["tmp_name"], "r")) !== FALSE)
					{
						while (($data_csv = fgetcsv( $handle, 10000, ";" ) ) !== FALSE)
						{
							if( isset( $data_csv[0] ) && isset( $data_csv[1] ) )
								$recipients_arr[] = array("recipient_name" => $data_csv[0], "recipient_email" => $data_csv[1], "type" => "1", "unsubscribe" => "");
						}
					}
					
					fclose($handle);
					@unlink($_FILES["csv_emails"]["tmp_name"]);
				}
				
				if ($email_to != "")
				{
					$email_to_arr = explode(",", $email_to);
					foreach ($email_to_arr as $value)
					{
						$user_info = $this->users_model->get_user_info_by_email($value);
						
						if ($user_info && is_array( $user_info ) && count( $user_info) > 0)
							$recipients_arr[] = array("recipient_name" => $user_info["first_name"] . " " . $user_info["last_name"], "recipient_email" => $user_info["email"], "type" => "1", "unsubscribe" => "");
						else 
							$recipients_arr[] = array("recipient_name" => $value, "recipient_email" => $value, "type" => "1", "unsubscribe" => "");
					}
				}
				
				$attachments = array();
				if( count( $recipients_arr ) > 0 )
				{
					$this->load->library('upload', array("allowed_types" => "*"));
					for ($i = 1; $i <= 5; $i++)
					{
						if (isset( $_FILES["file_" . $i] ) && $_FILES["file_" . $i]["error"] == 0)
						{
							/*$path = $this->upload->create_upload_path();
							$this->upload->set_upload_path($path);
							$this->upload->do_upload("file_" . $i);
							$file_data = $this->upload->data();*/
							
							$path = $this->upload->create_upload_path();
							$this->upload->set_upload_path($path);
							$this->upload->do_upload("file_" . $i);
							$file = $this->upload->insert_into_db();
							
							if ($file)
							{
							//	$attachments[] = array( "file_path" => $path . $file_data["file_name"], "file_name" => $file_data["file_name"], "delete_after" => 0 );
								$attachments[] = array("file_id" => $file["id"], "delete_after" => 0);
							}
						}
					}
				}
				
				if ($use_template == 1)
				{
					$mail_body = new Smarty_parser();
					/*if( $recipient["name"] == "" )
						$mail_body->assign( "name", "Customer" );
					else
						$mail_body->assign( "name", $recipient["name"] );*/
					$mail_body->assign( "email_content", $message );
					$text_body = $mail_body->fetch( "admin/emails/send_to_all_template.htm" );
				}
				else
					$text_body = $message;
					
				if (isset( $recipient["unsubscribe"] ) && $recipient["unsubscribe"])
					$text_body = $text_body . $recipient["unsubscribe"];
				

				if ($make_statistics)
				{
					$text_body = $text_body . $email_campaign_code;
				}

				$emails_parameters = array();
				$emails_parameters["from_name"] = $name;
				$emails_parameters["from_email"] = $email_from;
				$emails_parameters["subject"] = $subject;
				$emails_parameters["mail_content"] = $text_body;
				$emails_parameters["status"] = 1;
				$emails_parameters["priority"] = 1;
				$emails_parameters["published"] = now();
				$emails_parameters["recipients"] = $recipients_arr;
			//	$emails_parameters["recipients"][] = array("recipient_name" => ( $recipient["name"] != $recipient["email"] ? $recipient["name"] : "" ), "recipient_email" => $recipient["email"], "type" => "1");
				if (count( $attachments ) > 0)
				{
					$emails_parameters["attachments"] = $attachments;
				}
				$send_email = $this->emails_model->add($emails_parameters);
				
				if (!($send_email > 0))
				{
					$sending_errors[] = "Message to " . $recipient["email"] . ": FAILED";
				}
				
				if( isset($sending_errors) && count( $sending_errors ) > 0 )
				{
					set_alert_message("Attention!", "Some messages were not sent.", $sending_errors, "error");
				}
				else 
				{
					set_alert_message("Success!", "Messages were sent successfully.", "", "success");

					$this->emails_model->update_predefined_data($subject, "subject");
					$this->emails_model->update_predefined_data($email_from, "from");
					$this->emails_model->update_predefined_data($name, "name");
					$this->emails_model->update_predefined_data($message, "message");
					
					redirect("/admin/emails/send");
				}
			}
			else
			{
				$errors = explode("#", validation_errors());
				array_shift( $errors );

				set_alert_message("Wrong data! Please check the list below.", "", $errors, "error");
				
				foreach ($this->input->post() as $key => $value)
					$this->smarty_parser->assign($key, $value);
			}
		}
		
		if (has_alert_message())
		{
			$this->smarty_parser->assign('alert_message', get_alert_message());
		}
				
		$this->load->model("users_model");
		$active_clients = $this->users_model->get_all_users("status = 1 AND newsletter = 1");
		$this->smarty_parser->assign("active_clients", $active_clients);
				
		//$this->load->model("emails_model");
		$predefined_from = $this->emails_model->get_predefined_data("from");
		$this->smarty_parser->assign("predefined_from", $predefined_from);

		$predefined_name = $this->emails_model->get_predefined_data("name");
		$this->smarty_parser->assign("predefined_name", $predefined_name);

		$predefined_subject = $this->emails_model->get_predefined_data("subject");
		$this->smarty_parser->assign("predefined_subject", $predefined_subject);

		$predefined_message = $this->emails_model->get_predefined_data("message");
		$this->smarty_parser->assign("predefined_message", $predefined_message);
		
		$output = $this->smarty_parser->fetch("admin/emails.htm");
		$this->output->set_output($output);
	}
	
	public function view($email_id)
	{
		$email_info = $this->emails_model->get_email_info_by_id($email_id);
		$this->smarty_parser->assign("email_info", $email_info);
		
		$output = $this->smarty_parser->fetch("admin/emails.htm");
		$this->output->set_output($output);		
	}
	
	public function send_email_to($field = null)
	{
		$email_to 		   = $this->input->post("email_to");
		$active_clients    = $this->input->post("active_clients");

		if ($email_to == "" && !$active_clients && !(isset($_FILES["csv_emails"]) && $_FILES["csv_emails"]["size"] > 0))
		{
			$this->form_validation->set_message("send_email_to", "Please select to who you want to send the message.");
			return false;
		}
			
		return true;
	}
	
	public function is_valid_csv($field = null)
	{
		if( isset( $_FILES["csv_emails"] ) && $_FILES["csv_emails"]["size"] > 0 )
		{
			$ext = end( explode( ".", $_FILES["csv_emails"]["name"] ) );
			if( $ext != "csv" )
			{
				$this->form_validation->set_message("is_valid_csv", "Please upload a valid CSV file.");
				return false;
			}
		}	

		return true;
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */