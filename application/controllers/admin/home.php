<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->administration = true;
		
		check_rights(USER_ADMIN);
	}
	
	public function index()
	{
		//set_flash_message( "Information Box", "This is just an example of the information box which we think to use in our web site.", array( "show_title" => true, "modal" => true, "width" => 400, "height" => 200, "auto_hide" => 0, "show_button" => true ) );

		
		$this->load->library('smarty_parser');
		
		$output = $this->smarty_parser->fetch("admin/home.htm");
		
		$this->output->set_output($output);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */