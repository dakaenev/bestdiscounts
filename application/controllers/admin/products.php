<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->administration = true;
		$this->load->model("products_model");
		if ($this->uri->segment(3) != "login")
		{
			check_rights(USER_ADMIN);		
		}
	}
        
	public function index()
	{
		$this->show_all();
	}
	
	public function show_all()
	{
		$this->show_products("show_all");
	}
	
	public function show_products($action)
	{
		$this->smarty_parser->assign("action", "show_all");
		
		$offset = $this->uri->segment(4,0);
		$limit = 100;
			
                $where = '';
		$where_or = array();
		$where_and = array();
		
		$keyword = $this->input->get("keyword");
	
		if($keyword)
		{
			$where_or[] = "title LIKE '%" . addslashes($keyword) . "%'";
			$where_or[] = "meta_title LIKE '%" . addslashes($keyword) . "%'";
			$where_or[] = "meta_keywords LIKE '%" . addslashes($keyword) . "%'";
                        $where_or[] = "meta_description LIKE '%" . addslashes($keyword) . "%'";
		}
                
		if (count($where_or) > 0)
                {
                    if($where != '')
                    {
                        $where .= " AND (" . implode(" OR ", $where_or) . ")";
                    }
                    else
                    {
                        $where = implode(" OR ", $where_or);
                    }
                }
                        
		$products = $this->products_model->get_all_products($where, $offset, $limit, "published desc");
		
		$this->smarty_parser->assign('products',$products['rows']);
		$this->smarty_parser->assign('num_products',$products['count']);
		
		$this->smarty_parser->assign('offset', $offset);
		$this->smarty_parser->assign('keyword', $keyword);
		
		// navigator
		$this->load->library('pagination');
		
		$config['base_url'] 	= base_url() . "/admin/products/" . $action . "/";
		$config['suffix'] 		= "/?keyword=" . $this->input->get("keyword");
		$config['first_url'] 	= $config['base_url'] . "0" . $config['suffix'];
		$config['total_rows'] 	= $products['count'];
		$config['per_page'] 	= $limit;
		$config['uri_segment'] 	= 4;
		$config['num_links'] 	= 5;
		
		$this->pagination->initialize($config);
		$this->smarty_parser->assign('navigator', $this->pagination->create_links());
		// end navigator
		
		$this->smarty_parser->assign('action', $action);
		
		$output = $this->smarty_parser->fetch("admin/products.htm");
		$this->output->set_output($output);
	}
	
	public function edit($product_id)
	{
	
		$errors = array();
		
		if ($product_id > 0 && $this->products_model->product_exist($product_id, "id"))
		{
			$result = false;
                        
			if($this->input->post("save"))
			{
				
				$data["title"]                          = $this->input->post("title");
                                $data["url"]                            = prepare_for_url($this->input->post("title"));
                                $data["short_description"]       	= $this->input->post("s_description");
                                $data["description"]                    = $this->input->post("f_description");
                                $data["meta_title"]                     = $this->input->post("meta_title");
                                $data["meta_description"]       	= $this->input->post("meta_description");
                                $data["meta_keywords"] 			= $this->input->post("meta_keywords");
                                $data["modified"] 			= date( "Y-m-d H:i:s" );
                                $data["categories"]                     = $this->input->post("categories");
                                //$data["status"] 			= $this->input->post("status");	
                                
                                $this->load->library( "form_validation" );
				
				$this->form_validation->set_error_delimiters( "#", "" );
		
				$validated = $this->form_validation->run("update_product");		
				
				if ( $validated )
				{
                                    $result_update = $this->products_model->update($product_id, $data);
                                    $this->products_model->add_image($product_id, 'file_id');
                                    if ($result_update)
                                    {
                                            //set_flash_message( "Information Box", "The user profile has been updated successfully!", array( "show_title" => true, "modal" => true, "width" => 400, "height" => 200, "auto_hide" => 0, "show_button" => true ) );
                                            set_alert_message("The form was submitted successfully!", 'The <a href="/product/'.$product_id.'">product</a> has been updated successfully!', "", "success");
                                            redirect("/admin/products/edit/" . $product_id);
                                    }
				}
				
				if ($validated == false || $result == false || count($errors) > 0)
				{
					if ($validated == false)
					{
						$errors = explode( "#", validation_errors() );
						array_shift( $errors );
					}
					
					if (count($errors) > 0)
						set_alert_message("Wrong data! Please check the list below.", "", $errors, "error");
					elseif ($result == false)
						set_alert_message("Error processing the form!", "There was an error processing the form. Please try again later.", "", "error");
					
					
					$this->smarty_parser->assign('data', $data);					
				}							
			}
                        
			$product = $this->products_model->get_product_info($product_id);
                        $product_categories = $this->products_model->get_product_categories($product_id);
                        
                        $prod_image = array();
                        $prod_image = $this->products_model->get_product_image($product['file_id']);

                        if(count($prod_image) > 0)
                        {
                            if(mb_strlen($prod_image['file_name'], 'UTF-8') > 30)
                            {
                                $prod_image['file_name'] = mb_substr($prod_image['file_name'],0,27, 'UTF-8').'...';
                            }
                            $this->smarty_parser->assign('product_image', $prod_image);
                        }

			$this->smarty_parser->assign('product', $product);
			
		}
		else
		{
			redirect("/admin/products/");
		}	
		
		if (has_alert_message())
		{
			$this->smarty_parser->assign('alert_message', get_alert_message());
		}
		
                                
                // Load categories. We will use them, as options for Drop Down Menu
		$this->load->model("categories_model");
                
                if(isset($data['categories']) && is_array($data['categories']))
                {
                    // Get posted data
                    $categories_array = array_flip($data['categories']);
                }
                elseif(isset($product_categories) && is_array($product_categories))
                    {
                        // Get DataBase data
                        $categories_array = array_flip($product_categories);
                    }
                    else
                    {
                        $categories_array = array();
                    }
                    
                $categories = $this->categories_model->get_all_categories('status = 1', 0, 0, 'name ASC');
		$categories_list = '';
                foreach($categories['rows'] as $category)
                {
                    $categories_list .= PHP_EOL.'<option value="'.$category['id'].'"'.(isset($categories_array[$category['id']]) ? ' selected="selected"' : '').'>'.$category['name'].'</option>';
                }
                    
                $this->smarty_parser->assign('categories_list', $categories_list);
                
		$this->smarty_parser->assign('action', "edit");
		$output = $this->smarty_parser->fetch("admin/products.htm");
		$this->output->set_output($output);
	}
	
	public function add()
	{
		$errors = array();
		
		$product_id = 0;
		
		if($this->input->post("add"))
		{
			$data = array();
			$data["title"] 				= $this->input->post("title");
			$data["url"]                            = prepare_for_url($this->input->post("title"));
                        $data["description"]			= $this->input->post("f_description");
			$data["short_description"]		= $this->input->post("s_description");
			$data["meta_title"]                     = $this->input->post("meta_title");
			$data["meta_description"]       	= $this->input->post("meta_description");
			$data["meta_keywords"] 			= $this->input->post("meta_keywords");
			$data["published"] 			= date( "Y-m-d H:i:s" );
			$data["status"] 			= $this->input->post("status");
			$data["categories"]                     = $this->input->post("categories");
                        
			$this->load->library( "form_validation" );
			
			$this->form_validation->set_error_delimiters( "#", "" );
	
			$validated = $this->form_validation->run("add_product");		
			
			if ($validated)
			{
				$product_id = $this->products_model->add($data);
				if($product_id > 0)
                                {
                                    set_alert_message("Congratulations!", 'The <a href="/product/'.$product_id.'">product</a> was created successfully!', "", "success");
                                    redirect("/admin/products/edit/" . $product_id);
                                }
			}
			
			if ($validated == false || $product_id == 0)
			{
				if ($validated == false)
				{
					$errors = explode( "#", validation_errors() );
					array_shift( $errors );
				}
				
				if (count($errors) > 0)
					set_alert_message("Wrong data! Please check the list below.", "", $errors, "error");
				elseif ($product_id == 0)
					set_alert_message("Error processing the form!", "There was an error processing the form. Please try again later.", "", "error");
				
				$this->smarty_parser->assign('data', $data);					
			}	
		}
		
		if (has_alert_message())
		{
			$this->smarty_parser->assign('alert_message', get_alert_message());
		}
		
                // Load categories. We will use them, as options for Drop Down Menu
		$this->load->model("categories_model");
                
                if(isset($data['categories']) && is_array($data['categories']))
                {
                    // Get posted data
                    $categories_array = array_flip($data['categories']);
                }
                else
                {
                    $categories_array = array();
                }
                    
                $categories = $this->categories_model->get_all_categories('status = 1', 0, 0, 'name ASC');
		$categories_list = '';
                foreach($categories['rows'] as $category)
                {
                    $categories_list .= PHP_EOL.'<option value="'.$category['id'].'"'.(isset($categories_array[$category['id']]) ? ' selected="selected"' : '').'>'.$category['name'].'</option>';
                }
                    
                $this->smarty_parser->assign('categories_list', $categories_list);
                
		$this->smarty_parser->assign('action', "add");
		$output = $this->smarty_parser->fetch("admin/products.htm");
		$this->output->set_output($output);
	}
}