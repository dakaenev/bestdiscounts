<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap extends CI_Controller {

	public function index()
	{
		$this->page_title = "Sitemap";
		$this->page_keywords = "";
		$this->page_description = "";
		
		$output = $this->smarty_parser->fetch("sitemap.htm");
		$this->output->set_output($output);
	}
}