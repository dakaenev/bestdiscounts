<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_not_found extends CI_Controller {

	public function index()
	{
		$_SESSION["page_description"] = "Page not found!";
		$_SESSION["page_keywords"] = "";
		$_SESSION["page_title"] = "Page not found!";		
		
		$output = $this->smarty_parser->fetch("page_not_found.htm");
		
		$this->output->set_header("HTTP/1.1 404 Not Found");
		$this->output->set_status_header('404');
			
		$this->output->set_output($output);
	}
}