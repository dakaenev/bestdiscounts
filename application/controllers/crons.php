<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crons extends CI_Controller {

	/*
	*     *     *   *    *        command to be executed
	-     -     -   -    -
	|     |     |   |    |
	|     |     |   |    +----- day of week (0 - 6) (Sunday=0)
	|     |     |   +------- month (1 - 12)
	|     |     +--------- day of        month (1 - 31)
	|     +----------- hour (0 - 23)
	+------------- min (0 - 59)
	*/
	
	
	
	public function index()
	{
	//	if (!$this->input->is_cli_request())
	//		die("Error!");
		
		ini_set("display_errors","On");
		
		$query = $this->db->get_where('crons', array('status' => 1));	
		foreach ($query->result() as $row)
		{
			$last_run_time = strtotime( $row->last_run_time );
			$cron_time = explode( " ", $row->cron_time );
			
			$day_of_week = false;
			$month = false;
			$day_of_month = false;
			$hour = false;
			$minute = false;
			
			if( $cron_time[4] == "*" )
				$day_of_week = true;
			else 
			{
				if( strpos( $cron_time[4], "," ) > 0 )
				{
					$helper = explode( ",", $cron_time[4] );
					foreach( $helper as $value )
					{
						if( $value == date( "w" ) )
						{
							$day_of_week = true;
							break;
						}
					}
				}
				elseif( strpos( $cron_time[4], "-" ) > 0 )
				{
					$helper = explode( "-", $cron_time[4] );
					for( $i = $helper[0]; $i <= $helper[1]; $i ++ )
					{
						if( $i == date( "w" ) )
						{
							$day_of_week = true;
							break;
						}
					}
				}
				else 
				{
					if( $cron_time[4] == date( "w" ) )
						$day_of_week = true;
				}
			}
				
			if( $cron_time[3] == "*" )
				$month = true;
			else 
			{
				if( strpos( $cron_time[3], "/" ) > 0 )
				{
					$helper = explode( "/", $cron_time[3] );
					$cron_month = $helper[1];
					$month_check = $cron_month + date( "m", $last_run_time );
					if( $month_check > 12 )
						$month_check = $month_check - 12;
					//if( $month_check == date( "m" ) )
					if( ( mktime( date( "H", $last_run_time ), date( "i", $last_run_time ), date( "s", $last_run_time ), ( date( "m", $last_run_time ) + $cron_month ), date( "d", $last_run_time ), date( "Y", $last_run_time ) ) <= time() ) )
						$month = true;
				}
				elseif( strpos( $cron_time[3], "," ) > 0 )
				{
					$helper = explode( ",", $cron_time[3] );
					foreach( $helper as $value )
					{
						if( $value == date( "m" ) )
						{
							$month = true;
							break;
						}
					}
				}
				elseif( strpos( $cron_time[3], "-" ) > 0 )
				{
					$helper = explode( "-", $cron_time[3] );
					for( $i = $helper[0]; $i <= $helper[1]; $i ++ )
					{
						if( $i == date( "m" ) )
						{
							$month = true;
							break;
						}
					}
				}
				else 
				{
					if( $cron_time[3] == date( "m" ) )
						$month = true;
				}
			}
				
			if( $cron_time[2] == "*" )
				$day_of_month = true;
			else 
			{
				if( strpos( $cron_time[2], "/" ) > 0 )
				{
					$helper = explode( "/", $cron_time[2] );
					$cron_day_of_month = $helper[1];
					$day_of_month_check = $cron_day_of_month + date( "d", $last_run_time );
					if( !checkdate( date( "m" ), $day_of_month_check, date( "Y" ) ) )
					{
						$new_time = mktime( 0, 0, 0, date( "m", $last_run_time ), $day_of_month_check, date( "Y", $last_run_time ) );
						$day_of_month_check = date( "d", $new_time );
					}
					//if( $day_of_month_check == date( "d" ) )
					if( ( mktime( date( "H", $last_run_time ), date( "i", $last_run_time ), date( "s", $last_run_time ), date( "m", $last_run_time ), ( date( "d", $last_run_time ) + $cron_day_of_month ), date( "Y", $last_run_time ) ) <= time() ) )
						$day_of_month = true;
				}
				elseif( strpos( $cron_time[2], "," ) > 0 )
				{
					$helper = explode( ",", $cron_time[2] );
					foreach( $helper as $value )
					{
						if( $value == date( "d" ) )
						{
							$day_of_month = true;
							break;
						}
					}
				}
				elseif( strpos( $cron_time[2], "-" ) > 0 )
				{
					$helper = explode( "-", $cron_time[2] );
					for( $i = $helper[0]; $i <= $helper[1]; $i ++ )
					{
						if( $i == date( "d" ) )
						{
							$day_of_month = true;
							break;
						}
					}
				}
				else 
				{
					if( $cron_time[2] == date( "d" ) )
						$day_of_month = true;
				}
			}
				
			if( $cron_time[1] == "*" )
				$hour = true;
			else 
			{
				if( strpos( $cron_time[1], "/" ) > 0 )
				{
					$helper = explode( "/", $cron_time[1] );
					$cron_hour = $helper[1];
					$hour_check = $cron_hour + date( "H", $last_run_time );
					if( $hour_check > 23 )
						$hour_check = $hour_check - 23;
					//if( $hour_check == date( "H" ) )
					if( ( mktime( ( date( "H", $last_run_time ) + $cron_hour ), date( "i", $last_run_time ), date( "s", $last_run_time ), date( "m", $last_run_time ), date( "d", $last_run_time ), date( "Y", $last_run_time ) ) <= time() ) )
						$hour = true;
				}
				elseif( strpos( $cron_time[1], "," ) > 0 )
				{
					$helper = explode( ",", $cron_time[1] );
					foreach( $helper as $value )
					{
						if( $value == date( "H" ) )
						{
							$hour = true;
							break;
						}
					}
				}
				elseif( strpos( $cron_time[1], "-" ) > 0 )
				{
					$helper = explode( "-", $cron_time[1] );
					for( $i = $helper[0]; $i <= $helper[1]; $i ++ )
					{
						if( $i == date( "H" ) )
						{
							$hour = true;
							break;
						}
					}
				}
				else 
				{
					if( $cron_time[1] == date( "H" ) )
						$hour = true;
				}
			}
				
			if( $cron_time[0] == "*" )
				$minute = true;
			else 
			{
				if( strpos( $cron_time[0], "/" ) > 0 )
				{
					$helper = explode( "/", $cron_time[0] );
					$cron_minute = $helper[1];
					$minute_check = $cron_minute + date( "i", $last_run_time );
					if( $minute_check > 59 )
						$minute_check = $minute_check - 59;
					if( $minute_check == date( "i" ) || ( mktime( date( "H", $last_run_time ), ( date( "i", $last_run_time ) + $cron_minute ), date( "s", $last_run_time ), date( "m", $last_run_time ), date( "d", $last_run_time ), date( "Y", $last_run_time ) ) < time() ) )
						$minute = true;
				}
				elseif( strpos( $cron_time[0], "," ) > 0 )
				{
					$helper = explode( ",", $cron_time[0] );
					foreach( $helper as $value )
					{
						if( $value == date( "i" ) )
						{
							$minute = true;
							break;
						}
					}
				}
				elseif( strpos( $cron_time[0], "-" ) > 0 )
				{
					$helper = explode( "-", $cron_time[0] );
					for( $i = $helper[0]; $i <= $helper[1]; $i ++ )
					{
						if( $i == date( "i" ) )
						{
							$minute = true;
							break;
						}
					}
				}
				else 
				{
					if( $cron_time[0] == date( "i" ) )
						$minute = true;
				}
			}
			/*
			var_dump($day_of_week);
			var_dump($month);
			var_dump($day_of_month);
			var_dump($hour);
			var_dump($minute);
			echo $row->cron_method . '<br />';*/
			
			if ($day_of_week && $month && $day_of_month && $hour && $minute)
			{
				$cron_method = $row->cron_method;
				$this->$cron_method();
				
				$data = array(
	               'last_run_time' => date("Y-m-d H:i:s")
	            );
	
				$this->db->where('id', $row->id);
				$this->db->update('crons', $data);
			}
		}
	}
	
	public function send_emails()
	{
		$this->load->library('email');
		$this->load->model('emails_model');
		$emails = $this->emails_model->get_all_emails_from_db("emails.status = 1 AND emails_recipients.status = 0 AND emails_recipients.tried < 3", "emails.priority DESC, emails.published ASC", 5, 0);
		
		foreach ($emails as $email)
		{
			
			$this->email->clear();
			
			$this->email->from($email->from_email, $email->from_name);
			$this->email->reply_to($email->from_email, $email->from_name);
								
			$content = stripslashes($email->mail_content);
			$content = str_replace("[name]", $email->recipient_name, $content);
			$content = str_replace("[email_id]", $email->id, $content);
			$content = str_replace("[email]", $this->emailsstatistics_model->encrypt($email->recipient_email), $content);
		//	$content = str_replace("[email_hash]", $this->emailsstatistics_model->encrypt( $email->recipient_email ), $content);
			$content .= $email->unsubscribe;
			
			$this->email->message($content);
			$this->email->subject($email->subject);
			
			if (!$this->email->valid_email($email->recipient_email))
			{
				$this->emails_model->update_recipient_status(1, $email->recipient_id);
				continue;
			}
			
			if ( $email->type == "1" )
				$this->email->to($email->recipient_email); 
			else 
				$this->email->bcc($email->recipient_email); 
			
			if (is_array($email->attachments) && count($email->attachments) > 0) 
			{
				foreach ($email->attachments as $attachment)
				{
					$this->email->attach(FCPATH . ltrim($attachment->full_path, "/"));
				}
			}

			if ($this->email->send())
			{
				$this->emails_model->update_recipient_status(1, $email->recipient_id);

				if (is_array($email->attachments) && count($email->attachments) > 0) 
				{
					foreach ($email->attachments as $attachment)
					{
						if ($attachment->delete_after == 1)
						{
							if (is_file(FCPATH . ltrim($attachment->full_path, "/")))
								unlink(FCPATH . ltrim($attachment->full_path, "/"));
						}
					}
				}
			}
			else 
			{
				$this->emails_model->update_recipient_status(0, $email->recipient_id);
			}
			
			/*
			require_once(FCPATH . "plugins/phpmailer/class.phpmailer.php");
			
			$mail = new phpmailer();
		
			$mail->From = $email->from_email;
			$mail->FromName = $email->from_name;
			$mail->ContentType = MAIL_TYPE;
			$mail->Mailer = MAIL_MAILER;
			$mail->Host = MAIL_HOST;
			
			$mail->Body = strip_slashes($email->mail_content);
			$mail->Subject = $email->subject;
			
			if (isset($email->reply_to) && $email->reply_to && $mail->ValidateAddress($email->reply_to))
				$mail->AddReplyTo($email->reply_to, "");
			
			if ( MAIL_AUTHENTICATION === true )
			{
				$mail->Username = MAIL_USERNAME;
				$mail->Password = MAIL_PASSWORD;
				$mail->SMTPAuth = MAIL_AUTHENTICATION;
				$mail->SMTPSecure = MAIL_SECURE;
			}
			
			foreach ($email->recipients as $recipient)
			{
				if (!$mail->ValidateAddress($recipient->recipient_email))
					continue;
				if ( $recipient->type == "1" )
					$mail->AddAddress($recipient->recipient_email, $recipient->recipient_name);
				else 
					$mail->AddBCC($recipient->recipient_email, $recipient->recipient_name);
			}
			
			if (is_array($email->attachments) && count($email->attachments) > 0) 
			{
				foreach ($email->attachments as $attachment)
				{
					$mail->AddAttachment(FCPATH . ltrim($attachment->file_path, "/"));
				}
			}
			
			$mail->SMTPDebug = true;
			$status = $mail->Send();
			if ( $status )
			{
				$this->emails_model->update_email_status(1, $email->id);
				
				if (is_array($email->attachments) && count($email->attachments) > 0) 
				{
					foreach ($email->attachments as $attachment)
					{
						if ($attachment->delete_after == 1)
						{
							if (is_file(FCPATH . ltrim($attachment->file_path, "/")))
								unlink(FCPATH . ltrim($attachment->file_path, "/"));
						}
					}
				}
			}
			else 
			{ echo $mail->ErrorInfo;
				$this->emails_model->update_email_status(0, $email->id);
			}*/
						
		}
		
		print("this is send_emails cron!");
	}
		
	public function _output()
	{
	//	echo 'dddddddddddd';
		exit;
	}
}