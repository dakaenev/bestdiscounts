<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Terms extends CI_Controller {

	public function index()
	{
		$_SESSION["page_description"] = "Terms of Use. Best Discounts Here";
		$_SESSION["page_keywords"] = "Best Discounts Here";
		$_SESSION["page_title"] = "Terms of Use. Best Discounts Here";		
		
		$output = $this->smarty_parser->fetch("terms.htm");
		$this->output->set_output($output);
	}
}