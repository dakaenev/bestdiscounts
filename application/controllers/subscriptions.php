<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subscriptions extends CI_Controller {

	public function __construct()
	{
		parent::__construct();		
		//$this->load->model('subscribers_model');
	}
	
	public function index()
	{
		redirect("/");
	}
		
	public function success($type = "")
	{
		if ($type == "affiliate")
			$this->smarty_parser->assign("is_affiliate", true);
		
		$this->smarty_parser->assign("action", "success");
			
		$output = $this->smarty_parser->fetch("subscriptions.htm");
		$this->output->set_output($output);
	}
	
	public function confirmed($type = "")
	{		
		if ($type == "affiliate")
			$this->smarty_parser->assign("is_affiliate", true);
		
		$this->smarty_parser->assign("action", "confirm");
			
		$output = $this->smarty_parser->fetch("subscriptions.htm");
		$this->output->set_output($output);
	}
	
	public function alreadysubscribed($type = "")
	{
		if ($type == "affiliate")
			$this->smarty_parser->assign("is_affiliate", true);
		
		$this->smarty_parser->assign("action", "alreadysubscribed");
		
		
		$output = $this->smarty_parser->fetch("subscriptions.htm");
		$this->output->set_output($output);		
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */