<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
		
	public function index()
	{
		$item = $this->uri->segment(1);
		$item_id = $this->uri->segment(2);
                $item_id = 2;
                if($item == '')
                {
                    $item = 'home';
                }
                
                //$item sadarja imeto-url na kategoria ili na produkt. po dolu trqbva da se napravi produkta dali go ima v bazata ili ne.
		//ako e kategoria se zarejdat produktite, ako e produkt se zarejda produkta
		//suotvetno ako e kategoria se zarejdat meta dannite na kategoriata
		//ako e produkt meta dannite na produkt
		//ako ne go namerim v bazata danni vrushtame 404.
                
                // Load models
		$this->load->model("categories_model");
                $this->load->model("products_model");
                
                // Load default meta data
                $_SESSION["page_description"] = "Best Discounts Here";
                $_SESSION["page_keywords"] = "Best Discounts Here";
                $_SESSION["page_title"] = "Best Discounts Here";
                
                // Load all categories and Last 10 discounts
		$categories = $this->categories_model->get_all_categories("", 0, 0, "name asc");
		$last_products = $this->products_model->get_last_products();
		
                $categories_list = '';
                $products_list = '';
                
                // Prepare categories
                foreach ($categories['rows'] as $category)
                {
                    $products_count = $this->categories_model->get_products_count_for_category($category['id']);
                    $categories_list .= '<li><a href="/category/'.$category['id'].'/">'.$category['name'].' ('.$products_count.')</a></li>'.PHP_EOL;
                }
                
                // Prepare products
                foreach ($last_products['rows'] as $product)
                {
                    $min_price = $this->products_model->get_minimal_price($product['id']);
                    $products_list .= '<div class="latest-shop-items">
					<a href="/product/'.$product['id'].'/'.$product['url'].'"><img src="'.$product['full_path'].'" width="55" height="55" alt="" /></a>
					<p><a href="/product/'.$product['id'].'/'.$product['url'].'">'.$product['title'].'</a> <span>'.((int) $min_price['price'] > 0 ? '$'.$min_price['price'] : '').'</span></p>
                                       </div>';
                }
                
                $where = '';
		$where_or = array();
		$where_and = array();
		
		$keyword = $this->input->get("keyword");
	
		if($keyword)
		{
			$where_or[] = "title LIKE '%" . addslashes($keyword) . "%'";
			$where_or[] = "meta_title LIKE '%" . addslashes($keyword) . "%'";
			$where_or[] = "meta_keywords LIKE '%" . addslashes($keyword) . "%'";
                        $where_or[] = "meta_description LIKE '%" . addslashes($keyword) . "%'";
		}
                
		if (count($where_or) > 0)
                {
                    if($where != '')
                    {
                        $where .= " AND (" . implode(" OR ", $where_or) . ")";
                    }
                    else
                    {
                        $where = implode(" OR ", $where_or);
                    }
                }
                        
                //Check what we looking for
                
                switch(strtolower($item))
                {
                    case 'category': $item_type = 'category'; $item_name = 'Категория'; break;
                    case 'product': $item_type = 'product'; $item_name = 'Продукт'; break;
                    case 'home': $item_type = 'home'; $item_name = 'Начало'; break;
                    
                    //case '': $item_type = ''; $item_title = ''; $item_text = ''; break;
                    
                    default: $item_type = 'Not Found'; $item_title = 'Page Not Found'; $item_text = ''; $item_name = 'Проблем!'; $output_page = 'page_not_found.htm'; break;
                }
                
                if($item_type == 'product')
                {
                    if(!$this->products_model->product_exist($item_id, 'id'))
                    {
                        $output_page = 'page_not_found.htm';
                    }
                    else
                    {
                        $product_info = $this->products_model->get_product_info($item_id);
                        $product_options = $this->products_model->get_product_options($item_id);
                        $product_categories = $this->products_model->get_product_categories($item_id);
                        
                        $product_related = $this->categories_model->get_related_products($product_categories,$item_id);
                        $product_categories = $this->categories_model->get_all_categories('id IN ('.implode(',',$product_categories).')');
                        
                        foreach($product_related as $k => $v)
                        {
                            $prod_image = array();
                            $prod_image = $this->products_model->get_product_image($v['file_id']);

                            if(count($prod_image) > 0)
                            {
                                $product_related[$k]['image'] = $prod_image['full_path'];
                            }
                        }
                        
                        $_SESSION["page_description"] = $product_info['meta_description'];
                        $_SESSION["page_keywords"] = $product_info['meta_keywords'];
                        $_SESSION["page_title"] = $product_info['meta_title'];
                        
                        $item_img_array = $this->products_model->get_product_image($product_info['file_id']);
                        if(isset($item_img_array['full_path']))
                        {
                            $item_img = $item_img_array['full_path'];
                        } 
                        $item_title = $product_info['title'];
                        $item_text = $product_info['description'].'<br />';
                        
                        $this->smarty_parser->assign('product_related', $product_related);
                        $this->smarty_parser->assign('product_options', $product_options);
                        $this->smarty_parser->assign('product_categories', $product_categories['rows']);
                        $output_page = 'product.htm';
                    }
                }
                
                if($item_type == 'category')
                {
                    if(!$this->categories_model->category_exist($item_id, 'id'))
                    {
                        $output_page = 'page_not_found.htm';
                    }
                    else
                    {
                        $category_info = $this->categories_model->get_category_info($item_id);
                        
                        $_SESSION["page_description"] = $category_info['meta_description'];
                        $_SESSION["page_keywords"] = $category_info['meta_keywords'];
                        $_SESSION["page_title"] = $category_info['meta_title'];
                        
                        $item_title = $category_info['name'];
                        $products = $this->categories_model->get_products_into_category($item_id);
                        
                        if(is_array($products) && count($products) > 0)
                        {
                            $item_text = '';
                            $new_products = array();
                            
                            foreach($products as $key => $value)
                            {
                                $prod_image = array();
                                $prod_image = $this->products_model->get_product_image($value['file_id']);
                                
                                if(count($prod_image) > 0)
                                {
                                    $products[$key]['image'] = $prod_image['full_path'];
                                }
                                
                                $product_categories = $this->products_model->get_product_categories($value['product_id']);
                                
                                $product_categories = $this->categories_model->get_all_categories('id IN ('.implode(',',$product_categories).')');
                                $products[$key]['categories'] = $product_categories['rows'];
                                $products[$key]['options'] = $this->products_model->get_product_options($value['product_id']);
                            }
                        }
                        else
                        {
                            $item_text = '<div class="alert alert-info">There are no products into this category!</div>';
                        }
                        
                        $this->smarty_parser->assign('products', $products);
                        $output_page = 'category.htm';
                    }
                }
                
                if($item_type == 'home' || $where != '')
                {
                    //$category_info = $this->categories_model->get_category_info($item_id);

                    //$_SESSION["page_description"] = $category_info['meta_description'];
                    //$_SESSION["page_keywords"] = $category_info['meta_keywords'];
                    //$_SESSION["page_title"] = $category_info['meta_title'];

                    $item_title = 'All Categories';
                    $products = $this->categories_model->get_products_into_category(false, $where);

                    if(is_array($products) && count($products) > 0)
                    {
                        $item_text = '';
                        $new_products = array();

                        foreach($products as $key => $value)
                        {
                            $product_categories = $this->products_model->get_product_categories($value['product_id']);

                            $product_categories = $this->categories_model->get_all_categories('id IN ('.implode(',',$product_categories).')');
                            //print_r($product_categories); exit;
                            $products[$key]['categories'] = $product_categories['rows'];
                            $products[$key]['options'] = $this->products_model->get_product_options($value['product_id']);
                        }
                    }
                    else
                    {
                        $item_text = '<div class="alert alert-info">There are no products into this category!</div>';
                    }

                    $this->smarty_parser->assign('products', $products);
                    $output_page = 'category.htm';
                    
                }
                
                
                // Assign view variables
                $this->smarty_parser->assign('categories_array', $categories['rows']);
                $this->smarty_parser->assign('categories_list', $categories_list);
                $this->smarty_parser->assign('products_list', $products_list);
                
                
                if(isset($item_type)) $this->smarty_parser->assign('item_type', ucfirst($item_name));
                if(isset($item_title)) $this->smarty_parser->assign('item_title', $item_title);
                if(isset($item_text)) $this->smarty_parser->assign('item_text', $item_text);
                if(isset($item_img)) $this->smarty_parser->assign('item_img', $item_img);
                
                // Set Default page
                if(!isset($output_page))
                {
                    $output_page = 'home.htm'; 
                }
                

		$output = $this->smarty_parser->fetch($output_page);
		$this->output->set_output($output);		
	}
}