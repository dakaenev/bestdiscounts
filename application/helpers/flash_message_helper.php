<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('valid_email'))
{
	function valid_email($address)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $address)) ? FALSE : TRUE;
	}
}

if ( ! function_exists('get_flash_message'))
{
	function get_flash_message()
	{
		if ( isset( $_SESSION["flash_message"] ) )
		{
			$flash_message = "<script type=\"text/javascript\" language=\"javascript\">
				$( document ).ready( function() {
					showFlashMessage( '" . addslashes( $_SESSION["flash_message"]["title"] ) . "', '" . addslashes( $_SESSION["flash_message"]["message"] ) . "', " . json_encode( $_SESSION["flash_message"]["options"] ) . " );
				} );
			</script>";
			
			unset( $_SESSION["flash_message"] );
			
			return $flash_message;
		}
		else 
			return "";
	}
}

if ( ! function_exists('set_flash_message'))
{
	function set_flash_message( $title, $message, $options = array(), $list = null ) 
	{
		if ( !isset( $_SESSION["flashmsg"] ) )
		{
			$_SESSION["flash_message"] = array();
		}
		$extraMessage = "";
		if ( $list != null )
		{
			$extraMessage = create_list_message( $list );
		}
		else $list = "";
		
		$_SESSION["flash_message"]["title"] = $title; 
		$_SESSION["flash_message"]["message"] = str_replace( array( "\n", "\t" ), array( "" ), $message . $extraMessage );
		$_SESSION["flash_message"]["options"] = $options; 
	}
}

if ( ! function_exists('create_list_message'))
{
	function create_list_message( $list = null )
	{
		$extraMessage = "";
		if ( $list != null )
		{
			$extraMessage = "<br /><ul><li>";
			if ( is_array( $list ) )
			{
				$extraMessage .= implode( "</li><li>", $list );		
			}
			else 
			{
				$extraMessage .= $list;	
			}
			$extraMessage .= "</li></ul>";
			
			$extraMessage = str_replace( array( "\n", "\t" ), array( "" ), $extraMessage );
		}

		return $extraMessage;
	}
}

/* End of file email_helper.php */
/* Location: ./application/helpers/flash_message */