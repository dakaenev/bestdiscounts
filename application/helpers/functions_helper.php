<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('now'))
{
	function now()
	{
		return date("Y-m-d H:i:s");
	}
}

if ( ! function_exists('check_rights'))
{
	function check_rights($user_type = "", $allow_redirect = true)
	{
		if (isset($_SESSION["user"]))
			$logged_user = true;
		else
			$logged_user = false;
		
		if ($logged_user == true && $user_type == "")
			return true;
		
		if ($logged_user == true && $_SESSION["user"]["type"] == $user_type)
			return true;
		
		if ($logged_user == true && $user_type == USER_ADMIN && $_SESSION["user"]["type"] == USER_NORMAL)
		{
                    if($allow_redirect)
                    {
                        redirect("/");
                    }
                    else
                    {
                        return true;
                    }
                }
			
		if ($logged_user == false && ($user_type == "" || $user_type == USER_NORMAL))
                {
                    if($allow_redirect)
                    {
                        redirect("/users/login");
                    }
                    else
                    {
                        return false;
                    }
                }
                
		if ($logged_user == false && $user_type == USER_ADMIN)
                {
                    if($allow_redirect)
                    {
                        redirect("/admin/users/login");
                    }
                    else
                    {
                        return false;
                    }
                }
	}
}

if ( ! function_exists('errors_array2html'))
{
	function errors_array2html($errors = array())
	{
		$html = '';
		if (is_array($errors) && $errors)
		{
			$html = '<ul>';
			foreach ($errors as $error)
			{
				$html .= '<li>' . $error . '</li>';
			}
			$html .= '</ul>';
		}		
		
		return $html;
	}
}

if ( ! function_exists('remove_file'))
{
	function remove_file($path = "")
	{
		if (file_exists($path))
		{
			@unlink($path);
			return true;
		}
		
		return false;
	}
}

if ( ! function_exists('alert_message'))
{
	function alert_message($title, $message, $list = array(), $type = "info")
	{
		// type: success, info, error
		$html = '';
		$html .= '<div class="alert alert-' . $type . '">';
		$html .= '<button type="button" class="close" data-dismiss="alert">&times;</button>';
		
		if ($title != "")
			$html .= '<h4>' . $title . '</h4>';
		if ($message != "")
			$html .= '<p>' . $message . '</p>';
			
		if (is_array($list) && count($list) > 0)
		{
			$html .= '<p><ul>';
			foreach ($list as $item)
			{
				$html .= '<li>' . $item . '</li>';
			}
			$html .= '</ul></p>';
		}
		else if ($list != "")
		{
			$html .= '<p>' . $list . '</p>';
		}
		
		$html .= '</div>';
		
		return $html;
	}
}

if ( ! function_exists('set_alert_message'))
{
	function set_alert_message($title, $message, $list = array(), $type = "info") 
	{
		$alert_message = alert_message($title, $message, $list, $type);
		
		$_SESSION["alert_message"] = $alert_message;
		
	}
}

if ( ! function_exists('get_alert_message'))
{
	function get_alert_message() 
	{
		if (isset($_SESSION["alert_message"]) && $_SESSION["alert_message"])
		{
			$alert_message = $_SESSION["alert_message"];
			unset( $_SESSION["alert_message"] );
			return $alert_message;
		}
		else
			return false;
	}
}

if ( ! function_exists('has_alert_message'))
{
	function has_alert_message() 
	{
		if (isset($_SESSION["alert_message"]) && $_SESSION["alert_message"])
			return true;
		else
			return false;
	}
}

if ( ! function_exists('generate_random_password'))
{
	function generate_random_password() 
	{
		$chars = "abcdefghijkmnopqrstuvwxyz023456789";
		srand((double)microtime() * 1000000);
		$i = 0;
		$pass = '' ;

		while ($i <= 7) 
		{
			$num = rand() % 33;
			$tmp = substr( $chars, $num, 1 );
			$pass = $pass . $tmp;
			$i++;
		}
		
		return $pass;
	}
}

if ( ! function_exists('load_launch_data'))
{
	function load_launch_data()
	{
		$start_time = time() + ( 9 * 60 * 60 );
		//$end_time = mktime( 18, 05, 0, 3, 23, 2011 );
		$end_time = mktime( 8, 0, 0, 12, 10, 2012 );
		
		$time_left = $end_time - $start_time;
		
		if( $time_left <= 0 )
		{
			$launched = true;
			$time_left = 0;
		}
		else
		{
			$launched = false;
		}
		
		if( !isset( $_SESSION["launch"] ) )
		{
			$_SESSION["launch"] = array();
		}
		
		$_SESSION["launch"]["time_left"] = $time_left;
		$_SESSION["launch"]["launched"] = $launched;
	}
}

if(!function_exists('cyr_to_latin')){
	function cyr_to_latin( $str ) 
	{
		$cyr2lat = array(
			"а" => "a", "б" => "b", "в" => "v", "г" => "g",
			"д" => "d", "е" => "e", "ж" => "j", "з" => "z",
			"и" => "i", "й" => "y", "к" => "k", "л" => "l",
			"м" => "m", "н" => "n", "о" => "o", "п" => "p",
			"р" => "r", "с" => "s", "т" => "t", "у" => "u",
			"ф" => "f", "х" => "h", "ч" => "ch", "ш" => "sh",
			"щ" => "sht", "ъ" => "a", "ь" => "y", "ю" => "yu",
			"я" => "ya", "ц" => "ts", "А" => "A", "Б" => "B", "В"=> "V", "Г"=> "G",
			"Д" => "D", "Е" => "E", "Ж" => "J", "З" => "Z",
			"И" => "I", "Й" => "Y", "К" => "K", "Л" => "L",
			"М" => "M", "Н" => "N", "О" => "O", "П" => "P",
			"Р" => "R", "С" => "S", "Т" => "T", "У" => "U",
			"Ф" => "F", "Х" => "H", "Ч" => "CH", "Ш" => "SH",
			"Щ" => "SHT", "Ъ" => "A", "Ь" => "Y", "Ю" => "YU",
			"Я" => "YA", "Ц" => "TS",
			//Russian
			"ы" => "y", "э" => "e", "ё" => "yo"
		);
			
		$keys = array_keys( $cyr2lat );
		$val_keys = array_values( $cyr2lat );
		$str = str_replace( $keys, $val_keys, $str );
		
		return $str;				
	}
}

if(!function_exists('str_to_latin')){
	function str_to_latin( $str ) 
	{	
		//for Bulgarian, Russian
		$str = cyr_to_latin( $str );
	
		//for French, German, Italian, Spanish, Turkish
		$a = array( '/(à|á|â|ã|ä|å|æ)/i', '/(è|é|ê|ë)/i', '/(ì|í|î|ï|ı|İ)/i', '/(ð|ò|ó|ô|õ|ö|ø)/i', '/(ù|ú|û|ü)/i', '/ç/i', '/þ/i', '/ñ/i', '/(ß|ş)/i', '/(ý|ÿ)/i', '/ğ/i', '/(¡|¿|=|\+|\/|\\\|\.|\'|\_|\\n| |\(|\))/', '/[^a-z0-9_ -]/si', '/-{2,}/si' );
		$b = array( 'a', 'e', 'i', 'o', 'u', 'c', 'd', 'n', 's', 'y', 'g', '-', '', '-' );
	
		return trim( preg_replace( $a, $b, strtolower( $str ) ), '-' );
	}
}

if(!function_exists('prepare_for_url')){
	function prepare_for_url($str)
	{
		$arr = array( 
			"-" => "-",
			"+" => "-",
			" " => "-",
			"." => "-",
			"," => "-",
			":" => "-",
			";" => "-",
			"'" => "-",
			"\"" => "-",
			"#" => "-",
			"$" => "-",
			"%" => "-",
			"^" => "-",
			"=" => "-",
			"|" => "-",
			"/" => "-",
			"\\" => "-",
			"[" => "-",
			"]" => "-",
			"{" => "-",
			"}" => "-",
			"(" => "-",
			")" => "-",
			"@" => "-",
			"!" => "-",
			"?" => "-",
			"~" => "-",
			"*" => "-",
			"<" => "-",
			">" => "-",
			"&" => "-and-"
		);
		
		$keys = array_keys( $arr );
		$val_keys = array_values( $arr );
		
		$str = mb_convert_case( $str, MB_CASE_LOWER, "UTF-8" );
		
		$str = str_replace( $keys, $val_keys, $str );
		$str = str_to_latin( $str );
	
		$str = str_replace( "----", "-", $str );
		$str = str_replace( "---", "-", $str );
		$str = str_replace( "--", "-", $str );
		
		return $str;
	}
}

/* End of file email_helper.php */
/* Location: ./application/helpers/flash_message */