<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
	
	"login" => array(
		array(
			"field" => "email",
			"label" => "E-mail",
			"rules" => "required|valid_email",
		),
		array(
			"field" => "password",
			"label" => "Password",
			"rules" => "required|callback_check_login_data",
		),
	),

	"login_admin" => array(
		array(
			"field" => "email",
			"label" => "E-mail",
			"rules" => "required|valid_email",
		),
		array(
			"field" => "password",
			"label" => "Password",
			"rules" => "required",
		),
	),

	"affiliate_signup" => array(
		array(
			"field" => "name",
			"label" => "Name",
			"rules" => "required",
		),	
		array(
			"field" => "email",
			"label" => "E-mail",
			"rules" => "required|valid_email",
		),
		array(
			"field" => "regnow_id",
			"label" => "Regnow ID",
			"rules" => "required",
		),
	),
	
	"contact_us" => array(
		array(
			"field" => "name",
			"label" => "Name",
			"rules" => "required",
		),	
		array(
			"field" => "email",
			"label" => "E-mail",
			"rules" => "valid_email|required",
		),
		array(
			"field" => "subject",
			"label" => "Subject",
			"rules" => "required",
		),
		array(
			"field" => "message",
			"label" => "Message",
			"rules" => "required",
		),		
	),

	"add_robot_version" => array(
		array(
			"field" => "version",
			"label" => "Version",
			"rules" => "required|callback_is_version_unique",
		),	
		/*array(
			"field" => "file_ex4",
			"label" => "EX4 file",
			"rules" => "callback_is_uploaded_robot_file[ex4]",
		),
		array(
			"field" => "file_dll",
			"label" => "DLL file",
			"rules" => "callback_is_uploaded_robot_file[dll]",
		),
		array(
			"field" => "file_exe",
			"label" => "EXE file",
			"rules" => "callback_is_uploaded_robot_file[exe]",
		),		
		array(
			"field" => "file_pdf",
			"label" => "PDF file",
			"rules" => "callback_is_uploaded_robot_file[pdf]",
		),*/		
	),

	"edit_robot_version" => array(
		array(
			"field" => "version",
			"label" => "Version",
			"rules" => "required|callback_is_version_unique[edit]",
		),	
		/*array(
			"field" => "file_ex4",
			"label" => "EX4 file",
			"rules" => "callback_is_uploaded_robot_file_edit[ex4]",
		),
		array(
			"field" => "file_dll",
			"label" => "DLL file",
			"rules" => "callback_is_uploaded_robot_file_edit[dll]",
		),
		array(
			"field" => "file_exe",
			"label" => "EXE file",
			"rules" => "callback_is_uploaded_robot_file_edit[exe]",
		),		
		array(
			"field" => "file_pdf",
			"label" => "PDF file",
			"rules" => "callback_is_uploaded_robot_file_edit[pdf]",
		),*/			
	),

	"add_product" => array(
		array(
			"field" => "title",
			"label" => "Title",
			"rules" => "trim|required|is_unique[products.title]",
		)
	),

	"update_product" => array(
		array(
			"field" => "title",
			"label" => "Title",
			"rules" => "trim|required",
		)
	),

	"add_email_campaign" => array(
		array(
			"field" => "name",
			"label" => "Name",
			"rules" => "required",
		),	
	),

	"generate_statement" => array(
		array(
			"field" => "statement",
			"label" => "Statement",
			"rules" => "callback_check_statement_file",
		),	
		array(
			"field" => "acc_num",
			"label" => "Account number",
			"rules" => "required|numeric",
		),
		array(
			"field" => "acc_name",
			"label" => "Account name",
			"rules" => "required",
		),
		array(
			"field" => "symbol",
			"label" => "Currency",
			"rules" => "required|alpha|exact_length[6]",
		),		
	),

	"add_edit_faq" => array(
		array(
			"field" => "product_id",
			"label" => "Product",
			"rules" => "required",
		),
		array(
			"field" => "question",
			"label" => "Question",
			"rules" => "required",
		),	
		array(
			"field" => "answer",
			"label" => "Answer",
			"rules" => "required",
		),
	),

	"send_email" => array(
		array(
			"field" => "email_from",
			"label" => "From",
			"rules" => "required|valid_email",
		),	
		array(
			"field" => "subject",
			"label" => "Subject",
			"rules" => "required",
		),
		array(
			"field" => "message",
			"label" => "Message",
			"rules" => "required",
		),
		array(
			"field" => "name",
			"label" => "Name",
			"rules" => "required",
		),
		array(
			"field" => "email_to",
			"label" => "Email to",
			"rules" => "callback_send_email_to",
		),
		array(
			"field" => "csv_emails",
			"label" => "CSV file",
			"rules" => "callback_is_valid_csv",
		),
	),

	"update_profile" => array(
		array(
			"field" => "first_name",
			"label" => "First name",
			"rules" => "required",
		),
		array(
			"field" => "last_name",
			"label" => "Last name",
			"rules" => "required",
		),		
		array(
			"field" => "email",
			"label" => "E-mail",
			"rules" => "valid_email|required",
		),
	),

	"change_password" => array(
		array(
			"field" => "old_password",
			"label" => "Old Password",
			"rules" => "trim|required|callback_check_old_password",
		),
		array(
			"field" => "new_password",
			"label" => "News password",
			"rules" => "required",
		),		
		array(
			"field" => "confirm_new_password",
			"label" => "Confrim new password",
			"rules" => "required|matches[new_password]",
		),
	),
	
	"update_user" => array(
		array(
			"field" => "first_name",
			"label" => "First name",
			"rules" => "trim|required",
		),		
		array(
			"field" => "last_name",
			"label" => "Last name",
			"rules" => "trim|required",
		),			
		array(
			"field" => "email",
			"label" => "E-mail",
			"rules" => "trim|required|valid_email",
		),
	),
	
	"add_user" => array(
		array(
			"field" => "first_name",
			"label" => "First name",
			"rules" => "trim|required",
		),		
		array(
			"field" => "last_name",
			"label" => "Last name",
			"rules" => "trim|required",
		),			
		array(
			"field" => "email",
			"label" => "E-mail",
			"rules" => "trim|required|valid_email",
		),
	),
	
	"add_category" => array(
		array(
			"field" => "name",
			"label" => "Name",
			"rules" => "trim|required|is_unique[categories.name]",
		),		
	),
	
	"update_category" => array(
		array(
			"field" => "name",
			"label" => "Name",
			"rules" => "trim|required",
		),		
	)

);

?>